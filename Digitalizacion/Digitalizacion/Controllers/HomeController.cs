﻿using Digitalizacion.Models;
using Microsoft.AspNetCore.Mvc;
using ReflectionIT.Mvc.Paging;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Digitalizacion.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HomeController(ApplicationDbContext dBContext)
        {
            _context = dBContext;
        }
        public IActionResult Index()
        {
            var model = _context.BatchIndexado.OrderBy(p => p.FecRegistro).ToList();
            return View(model);
        }

        [HttpPost]
        public IActionResult Index(string search)
        {
            return View(_context.BatchIndexado.Where(x => x.Cliente.Contains(search) ||
            x.Batch.Contains(search) || x.Poliza.Contains(search) || search == null).ToList());
        }

        [HttpGet]
        public FileStreamResult GetPDF(int id)
        {
            string ruta;
            string doc = _context.BatchImagenes.Where(r => r.IdImagen == id).Select(r => r.PathDoc).Single();
            ruta = doc;

            FileStream fs = new FileStream(path: ruta, FileMode.Open, FileAccess.Read);
            if (fs != null)
            {
                return File(fs, "application/pdf");
            }
            else
            {
                return ViewBag.Error = "No hay ningun documento En la Ruta";
            }
        }

        [HttpGet]
        public JsonResult Details(int id)
        {
            var batchImagenes = (from img in _context.BatchImagenes
                                 join imgInx in _context.BatchIndexado on img.IdImagen equals imgInx.IdIndex
                                 where img.IdImagen == id
                                 select new
                                 {
                                     img.IdImagen,
                                     img.Batch,
                                     imgInx.Poliza,
                                     imgInx.Cliente,
                                     imgInx.Dependientes,
                                     imgInx.Parentesco,
                                     img.TipoDoc,
                                     imgInx.Aseguradora,
                                     imgInx.FecRegistro,
                                     imgInx.Titular,
                                     img.PathDoc,
                                     imgInx.NroReclamo,
                                     imgInx.UsuarioReg
                                 }).SingleOrDefault();
            return Json(batchImagenes);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}
