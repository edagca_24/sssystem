﻿

$(document).ready(function () {
    $("#search").keyup(function () {
        _this = this;
        $.each($("#mytable tbody tr"), function () {
            if ($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
                $(this).hide();
            else
                $(this).show();
        });
    });
});

function getInfo(id, action) {
    $.ajax({
        type: "GET",
        url: action,
        data: { id },
        success: function (response) {
            mostrarDoc(response);
            //console.log(response);
        }
    });
};

function getTipoDoc(id, action) {
    $.ajax({
        type: "GET",
        url: action,
        data: { id },
        success: function (response) {
            $('#mytable').find('tr').each(function () {
                $(this).find('td').eq(n).after('<td>' + response.tipoDoc + '</td>');
            });
        }
    });
};

function mostrarDoc(response) {
    val = response;
    $('#dBatch').text(val.batch);
    $('#dPoliza').text(val.poliza);
    $('#dCliente').text(val.cliente);
    $('#dTipoDoc').text(val.tipoDoc);
    $('#dPathDoc').text(val.pathDoc);
    $('#dFecRegistro').text(formatDate(val.fecRegistro));
    // getPDF(val.idImagen,'Home/GetPDF') Url.Action("GetPDF","Home", new { id =  })"
    $(".pdfview").attr("src", "../../Home/GetPDF/" + val.idImagen);
};

function formatDate(fecha) {
    // format from M/D/YYYY to YYYYMMDD
    return (new Date(fecha).toJSON().slice(0, 10).split('-').reverse().join('-'));
};
