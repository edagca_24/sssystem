﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Digitalizacion.Models
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<BatchImagenes> BatchImagenes { get; set; }
        public DbSet<BatchIndexado> BatchIndexado { get; set; }
        public DbSet<DocumentosBatch> DocumentosBatch { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=10.0.1.25;Initial Catalog=GestionSeguros_TEST;User ID=ros;Password=ros;Connect Timeout=30;");
            }
        }
    }
}
