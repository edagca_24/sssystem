﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Digitalizacion.Models
{
    public class BatchIndexado
    {
        [Key]
        public int IdIndex { get; set; }
        public string Poliza { get; set; }
        public string Cliente { get; set; }
        public string Aseguradora { get; set; }
        public string Titular { get; set; }
        public string Dependientes { get; set; }
        public string Parentesco { get; set; }
        public string Batch { get; set; }
        public string TipoDoc { get; set; }
        public string NroReclamo { get; set; }
        public DateTime FecRegistro { get; set; }
        public string UsuarioReg { get; set; }
    }
}
