﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Digitalizacion.Models
{
    public class DocumentosBatch
    {
        [Key]
        public int IdBatch { get; set; }
        public string CodPoliza { get; set; }
        public string Batch { get; set; }
        public string PathDoc { get; set; }
        public string Fuente { get; set; }
        public DateTime FecRegistro { get; set; }
        public string UsuarioReg { get; set; }
    }
}
