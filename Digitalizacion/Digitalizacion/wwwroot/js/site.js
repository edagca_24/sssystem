﻿$(document).ready(function () {
    $('#mytable').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        }
    });
});

function getInfo(id, action) {
    $.ajax({
        type: "GET",
        url: action,
        data: { id },
        success: function (response) {
            mostrarDoc(response);
            console.log(response);
        }
    });
};

function getTipoDoc(id, action) {
    $.ajax({
        type: "GET",
        url: action,
        data: { id },
        success: function (response) {
            $('#mytable').find('tr').each(function () {
                $(this).find('td').eq(n).after('<td>' + response.tipoDoc + '</td>');
            });
        }
    });
};

function mostrarDoc(response) {
    val = response;
    $('#dBatch').text(val.batch);
    $('#dPoliza').text(val.poliza);
    $('#dCliente').text(val.cliente);
    $('#dTipoDoc').text(val.tipoDoc);
    $('#dPathDoc').text(val.pathDoc);
    $('#dAseguradora').text(val.aseguradora);
    $('#dTitular').text(val.titular);
    $('#dDependientes').text(val.dependientes != "" ? val.dependientes : "-" );
    $('#dParentesco').text(val.parentesco != null ? val.parentesco : "-" );
    $('#dNroReclamo').text(val.nroReclamo != null ? val.parentesco : "-");
    $('#dFecRegistro').text(formatDate(val.fecRegistro));
    // getPDF(val.idImagen,'Home/GetPDF') Url.Action("GetPDF","Home", new { id =  })"
    $(".pdfview").attr("src", "../../Home/GetPDF/" + val.idImagen);
};

function formatDate(fecha) {
    // format from M/D/YYYY to YYYYMMDD
    return (new Date(fecha).toJSON().slice(0, 10).split('-').reverse().join('-'));
};
