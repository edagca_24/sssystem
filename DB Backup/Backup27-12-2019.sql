USE SS_DB
GO
CREATE TABLE [dbo].[TBL_Usuario](
	[ID_Usuario] [int] IDENTITY(1,1) NOT NULL,
	[Usuario] [varchar](100) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Email] [varchar](100) NULL,
	[Fecha_Modificacion] [datetime] NULL,
	[Fecha_Registro] [datetime] default(getdate())  NULL,
	[ID_Estado] [char](1) NOT NULL,
	[Clave] [varchar](max) NULL,
	[Token] [varbinary](max) NULL,
	PRIMARY KEY (ID_Usuario)
	)
GO
CREATE TABLE [dbo].[TBL_Ente](
	[ID_Ente] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](200) NOT NULL,
	[Apellido] [varchar](200) NOT NULL,
	[ID_Estado] [char](1) default(1) NULL,
	[Fecha_Registro] [datetime] default(getdate())  NULL,
	[Tipo_Ente] [varchar](50) NULL,
	  PRIMARY KEY (ID_Ente))
GO
CREATE TABLE [dbo].[TBL_Motivo_Visita](
	[ID_Motivo] [int] IDENTITY(1,1) NOT NULL,
	[Motivo] [varchar](200) NOT NULL,
	[Usuario_Registro] [int] NOT NULL,
	[Fecha_Modificacion] [datetime] NULL,
	[Fecha_Registro] [datetime] default(getdate())  NULL,
	PRIMARY KEY (ID_Motivo),
	FOREIGN KEY (Usuario_Registro) REFERENCES TBL_Usuario(ID_Usuario)
)
GO
CREATE TABLE [dbo].[TBL_Estado_Visita](
	[ID_Estado_Visita] [int] IDENTITY(1,1) NOT NULL,
	[Estado_Visita] [varchar](200) NOT NULL,
	[Usuario_Registro] [int] NOT NULL,
	[Fecha_Registro] [datetime] default(getdate())  NULL,
PRIMARY KEY (ID_Estado_Visita),
FOREIGN KEY (Usuario_Registro) REFERENCES TBL_Usuario(ID_Usuario)
)
GO
CREATE TABLE [dbo].[TBL_Visita](
	[ID_Visita] [int] IDENTITY(1,1) NOT NULL,
	[Contacto] [varchar](150) NOT NULL,
	[ID_Motivo_Visita] [int] NOT NULL,
	[ID_Estado_Visita] [int] NOT NULL,
	[ID_Usuario] [int] NOT NULL,
	[Direccion] [varchar](250) NOT NULL,
	[Motivo_Cambio] [varchar](250) NOT NULL,
	[Fecha_Inicio_Visita] [datetime] NOT NULL,
	[Fecha_Fin_Visita] [datetime] NULL,
	[Fecha_Realizada_Visita] [datetime] NULL,
	[Fecha_Registro] [datetime] default(getdate())  NULL,
	[Fecha_Modificacion] [datetime] NULL,
	[Asunto] [nvarchar](250) NOT NULL,
	[Comentario] [nvarchar](250) NULL,
	[Comentario_Cierre] [varchar](250) NULL,
	[Hora_Inicio] [varchar](10) NOT NULL,
	[Hora_Fin] [varchar](10) NOT NULL,
	[Hora_Realizada] [varchar](10) NULL,
	[Comentario_Pospuesta] [varchar](250) NULL,
	[ID_Ente] [int] NOT NULL,
	PRIMARY KEY (ID_Visita),
    FOREIGN KEY (ID_Motivo_Visita) REFERENCES TBL_Motivo_Visita(ID_Motivo),
	FOREIGN KEY (ID_Estado_Visita) REFERENCES TBL_Estado_Visita(ID_Estado_Visita),
	FOREIGN KEY (ID_Usuario) REFERENCES TBL_Usuario(ID_Usuario),
	FOREIGN KEY (ID_Ente) REFERENCES TBL_Ente(ID_Ente)
	)
GO
CREATE TABLE [dbo].[TBL_Minuta](
	[ID_Minuta] [int] IDENTITY(1,1) NOT NULL,
	[ID_Ente] [int] NOT NULL,
	[Contacto] [varchar](150) NOT NULL,
	[Fecha_Reunion] [datetime] NULL,
	[Motivo_Reunion] [varchar](100) NULL,
	[Estado_Prioridad] [varchar](50) NOT NULL,
	[Lugar] [varchar](max) NOT NULL,
	[Usuario_Registro] [int] NOT NULL,
	[Fecha_Registro] [datetime] default(getdate())  NULL,
	PRIMARY KEY (ID_Minuta),
    FOREIGN KEY (Usuario_Registro) REFERENCES TBL_Usuario(ID_Usuario),
	FOREIGN KEY (ID_Ente) REFERENCES TBL_Ente(ID_Ente)
 )
GO
CREATE TABLE [dbo].[TBL_Asistentes](
	[ID_Asistentes] [int] IDENTITY(1,1) NOT NULL,
	[ID_Minuta] [int] NOT NULL,
	[Nombre_Asistente] [varchar](50) NOT NULL,
	[Fecha_Registro] [datetime] default  NULL,
	PRIMARY KEY (ID_Asistentes),
    FOREIGN KEY (ID_Minuta) REFERENCES TBL_Minuta(ID_Minuta))
GO
CREATE TABLE [dbo].[TBL_BatchImagenes](
	[IdImagen] [float] NULL,
	[Batch] [nvarchar](255) NULL,
	[TipoDoc] [nvarchar](255) NULL,
	[PathDoc] [nvarchar](255) NULL
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[TBL_BatchIndexado](
	[ID_Index] [float] NULL,
	[Poliza] [nvarchar](255) NULL,
	[Cliente] [nvarchar](255) NULL,
	[Aseguradora] [nvarchar](255) NULL,
	[Titular] [nvarchar](255) NULL,
	[Dependientes] [float] NULL,
	[Parentesco] [nvarchar](255) NULL,
	[Batch] [nvarchar](255) NULL,
	[Tipo_Doc] [nvarchar](255) NULL,
	[Nro_Reclamo] [nvarchar](255) NULL,
	[Fecha_Registro] [datetime] default(getdate())  NULL,
	[Usuario_Registro] [int] NULL,
	FOREIGN KEY (Usuario_Registro) REFERENCES TBL_Usuario(ID_Usuario)
)
GO
CREATE TABLE [dbo].[TBL_Detalle_Minuta](
	[ID_Detalle_Minuta] [int] IDENTITY(1,1) NOT NULL,
	[ID_Minuta] [int] NOT NULL,
	[ID_Asignado] [int] NOT NULL,
	[Tarea] [varchar](max) NULL,
	[Acuerdo] [varchar](max) NULL,
	[Recomendacion] [varchar](max) NULL,
	[Fecha_Registro] [datetime] default(getdate()) NULL,
	PRIMARY KEY (ID_Detalle_Minuta),
    FOREIGN KEY (ID_Minuta) REFERENCES TBL_Minuta(ID_Minuta),
	FOREIGN KEY (ID_Asignado) REFERENCES TBL_Ente(ID_Ente))
GO
CREATE TABLE [dbo].[TBL_DocumentosBatch](
	[ID_Batch] [float] NULL,
	[Cod_Poliza] [nvarchar](255) NULL,
	[Batch] [nvarchar](255) NULL,
	[PathDoc] [nvarchar](255) NULL,
	[Fuente] [nvarchar](255) NULL,
	[Fecha_Registro] [datetime] default(getdate())  NULL,
	[Usuario_Registro] [int] not NULL,
	[Sn_Indexado] [nvarchar](255) NULL,
	FOREIGN KEY (Usuario_Registro) REFERENCES TBL_Usuario(ID_Usuario)
)
GO
CREATE TABLE [dbo].[TBL_Integrantes_Visitas](
	[ID_Integrantes_Visitas] [int] IDENTITY(1,1) NOT NULL,
	[ID_Visita] [int] NOT NULL,
	[ID_Ente] [int] NOT NULL,
	[Nombre_Integrante] [varchar](100) NOT NULL,
	[Fecha_Registro] [datetime] default(getdate())  NULL,
	[Log_No] [int] NOT NULL,
	[Fecha_Modificacion] [datetime] NULL,
	PRIMARY KEY (ID_Integrantes_Visitas),
    FOREIGN KEY (ID_Visita) REFERENCES TBL_Visita(ID_Visita),
	FOREIGN KEY (ID_Ente) REFERENCES TBL_Ente(ID_Ente)
 )
GO
CREATE TABLE [dbo].[TBL_Poliza](
	[NumeroPoliza] [float] NULL,
	[Poliza] [nvarchar](255) NULL,
	[ID_Estado] [char](1) default(1) NULL
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[Vw_ConsultaPoliza](
	[Poliza] [varchar](30) NOT NULL,
	[Nombre Cliente] [varchar](403) NULL,
	[Aseguradora] [varchar](100) NULL,
	[Supervisor] [varchar](50) NOT NULL,
	[Ejecutivo] [varchar](50) NULL,
	[Unidad] [varchar](50) NULL,
	[Ramo] [varchar](50) NULL,
	[PersonaNumero] [bigint] NOT NULL
) ON [PRIMARY]
GO
CREATE procedure [dbo].[SP_CompletarVisita]
@IdVisita int,
@HoraRealizada varchar(50),
@FechaRealizada datetime,
@ComentarioCierre varchar(150)
		
as 

update [dbo].[TBL_Visita] set Hora_Realizada = @HoraRealizada, Fecha_realizada_visita = @FechaRealizada, 
Comentario_cierre = @ComentarioCierre, ID_Estado_Visita = 3 
where ID_Visita = @IdVisita

GO
CREATE procedure [dbo].[SP_EditarVisita]
@IdVisita int,
@Motivo int,
@Contacto varchar(50),
@Lugar varchar(100),
@Asunto varchar(100),
@Comentario varchar(150)
		
as 

update [dbo].[TBL_Visita] set ID_Motivo_visita = @Motivo,Contacto = @Contacto,Direccion = @Lugar, Asunto = @Asunto, 
Comentario = @Comentario
where ID_Visita = @IdVisita

GO
CREATE procedure [dbo].[SP_GetDataVisitas]
		@id int
as 

select *,(select (select P.Nombre from TBL_Ente P where P.ID_Ente = IV.ID_Ente) [NombrePersona] from TBL_Integrantes_Visitas IV where IV.ID_Visita = V.ID_Visita) 

from [dbo].[TBL_Visita] V
where V.ID_Visita = @id
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_GetDataVisitasEvent]
	@User int 	
as 

select 
(select cast(V.ID_Visita as varchar) +'-'+MV.Motivo from TBL_Motivo_visita MV where V.ID_Motivo_visita = MV.ID_Motivo) Titulo,
CONVERT(varchar,V.Fecha_Inicio_visita,23) +'T'+ V.Hora_Inicio FechaInicio,
CONVERT(varchar,V.Fecha_Inicio_visita,23) +'T'+ V.Hora_Fin FechaFin, 
case when V.ID_Estado_Visita = 1 then '#007bff' when V.ID_Estado_Visita = 2 then '#ffc107'  when V.ID_Estado_Visita = 3 then '#28a745' when V.ID_Estado_Visita = 4 then '#dc3545' end Color
from [dbo].[TBL_Visita] V
where  V.ID_Usuario = @User
GO
CREATE PROCEDURE [dbo].[SP_GetPersona]
@criterio VARCHAR (100)
AS
SELECT ID_Ente,Nombre+' '+ Apellido as NombreCompleto
FROM [dbo].[TBL_Ente]
WHERE (convert(varchar(50),ID_Ente)+' '+Nombre+' '+ Apellido) like '%'+@criterio+'%'
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_ListPersona]

as 

select ID_Ente,Nombre + '' + Apellido [Nombre] from TBL_Ente
GO
CREATE procedure [dbo].[SP_PosponerVisita]
@IdVisita int,
@HoraInicioNueva varchar(50),
@HoraFinNueva varchar(50),
@FechaInicioNueva datetime,
@Comentario_Pospuesta varchar(max)
		
as 

update [dbo].[TBL_Visita] set Hora_Inicio = @HoraInicioNueva, Hora_Fin = @HoraFinNueva, Fecha_Inicio_visita = @FechaInicioNueva, 
ID_Estado_Visita = 2, Comentario_Pospuesta = @Comentario_Pospuesta
where ID_Visita = @IdVisita
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_ReporteDeVisita]
(@fechaDesde datetime,
@fechaHasta datetime)
AS
begin
SELECT [ID_Visita],[Contacto],[ID_Motivo_Visita],[ID_Estado_Visita],[ID_Usuario]
,[Direccion],[Motivo_Cambio],[Fecha_Inicio_Visita],[Fecha_Fin_Visita],[Fecha_Realizada_Visita]
,[Fecha_Registro] ,[Fecha_Modificacion],[Asunto],[Comentario],[Comentario_Cierre] 
,[Hora_Inicio],[Hora_Fin],[Hora_Realizada],[Comentario_Pospuesta]
FROM [dbo].[TBL_Visita]
where [Fecha_Registro] >= @fechaDesde and  [Fecha_Registro] <= @fechaHasta
end
GO
create procedure [dbo].[SP_Save_Minuta]
(@IdPersona int 
,@Contacto varchar(50)
,@FechaReunion datetime
,@Motivo INT
,@Prioridad VARCHAR (20)
,@Lugar varchar(max)
,@Usuario_Registro int)
AS
begin 
set nocount on

INSERT INTO  [dbo].[TBL_Minuta] (ID_Ente,Contacto,Fecha_Reunion,Estado_Prioridad,Lugar,
Usuario_Registro,Fecha_Registro)

values(@IdPersona ,@Contacto,@FechaReunion,@Prioridad 
,@Lugar  ,@Usuario_Registro ,GETDATE())
      
end 
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_SAVE_VISITA]
(@ID_Contacto varchar(100) 
,@ID_Motivo_visita INT
,@ID_Estado_Visita INT
,@ID_Usuario INT
,@Direccion VARCHAR (200)
,@Fecha_Inicio_visita DATETIME
,@Fecha_Registro   DATETIME   
,@Asunto varchar (200)
,@Comentario  varchar (max)   
,@HoraInicio varchar (10)
,@HoraFin varchar (10)
,@Responsable int)
AS
begin 
set nocount on

INSERT INTO  [dbo].[TBL_Visita] (Contacto ,ID_Motivo_visita,ID_Estado_Visita
,ID_Usuario ,Direccion ,Fecha_Inicio_visita 
,Fecha_Registro ,Asunto ,Comentario ,Hora_Inicio ,Hora_Fin,ID_Ente)

values(@ID_Contacto ,@ID_Motivo_visita,@ID_Estado_Visita,@ID_Usuario 
,@Direccion  ,@Fecha_Inicio_visita ,@Fecha_Registro   
,@Asunto ,@Comentario ,@HoraInicio ,@HoraFin,@Responsable)
      
end 
GO
CREATE procedure [dbo].[SP_SaveItegrantes]
@IdVisita int,
@IdPersona int,
@Log int
,@Opcion int
as 
if @Opcion = 1 
begin
insert into [dbo].[TBL_Integrantes_Visitas](ID_Visita,ID_Ente,Nombre_Integrante,Fecha_Registro,Log_No,Fecha_Modificacion)
values(
@IdVisita,@IdPersona,(select Nombre +' '+ Apellido from TBL_Ente where ID_Ente = @IdPersona),GETDATE(),@Log,GETDATE()
)
end
else
begin
insert into [dbo].[TBL_Integrantes_Visitas](ID_Visita,ID_Ente,Nombre_Integrante,Fecha_Registro,Log_No,Fecha_Modificacion)
values(
@IdVisita,@IdPersona,(select Nombre +' '+ Apellido from TBL_Ente where ID_Ente = @IdPersona),GETDATE(),@Log,null
)
end

