USE [master]
GO
/****** Object:  Database [DB_A502E1_SmartSys]    Script Date: 18/12/2019 04:45 ******/
CREATE DATABASE [DB_A502E1_SmartSys]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DB_A502E1_SmartSys_Data', FILENAME = N'H:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\DB_A502E1_SmartSys_DATA.mdf' , SIZE = 13248KB , MAXSIZE = 1024000KB , FILEGROWTH = 10%)
 LOG ON 
( NAME = N'DB_A502E1_SmartSys_Log', FILENAME = N'H:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\DB_A502E1_SmartSys_Log.LDF' , SIZE = 3072KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DB_A502E1_SmartSys].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET ARITHABORT OFF 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET  ENABLE_BROKER 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET  MULTI_USER 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET QUERY_STORE = OFF
GO
USE [DB_A502E1_SmartSys]
GO
/****** Object:  Table [dbo].[TBL_Asistentes]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_Asistentes](
	[ID_Asistentes] [int] IDENTITY(1,1) NOT NULL,
	[ID_Minuta] [int] NOT NULL,
	[Nombre_Asistente] [varchar](50) NOT NULL,
	[Fecha_Registro] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_Asistentes] PRIMARY KEY CLUSTERED 
(
	[ID_Asistentes] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBL_BatchImagenes]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_BatchImagenes](
	[IdImagen] [float] NULL,
	[Batch] [nvarchar](255) NULL,
	[TipoDoc] [nvarchar](255) NULL,
	[PathDoc] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBL_BatchIndexado]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_BatchIndexado](
	[IdIndex] [float] NULL,
	[Poliza] [nvarchar](255) NULL,
	[Cliente] [nvarchar](255) NULL,
	[Aseguradora] [nvarchar](255) NULL,
	[Titular] [nvarchar](255) NULL,
	[Dependientes] [float] NULL,
	[Parentesco] [nvarchar](255) NULL,
	[Batch] [nvarchar](255) NULL,
	[TipoDoc] [nvarchar](255) NULL,
	[NroReclamo] [nvarchar](255) NULL,
	[FecRegistro] [datetime] NULL,
	[UsuarioReg] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBL_Detalle_Minuta]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_Detalle_Minuta](
	[ID_Detalle_Minuta] [int] IDENTITY(1,1) NOT NULL,
	[ID_Minuta] [int] NOT NULL,
	[Asignado] [int] NOT NULL,
	[Tarea] [varchar](max) NULL,
	[Acuerdo] [varchar](max) NULL,
	[Recomendacion] [varchar](max) NULL,
	[Fecha_Registro] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_Detalle_Minuta] PRIMARY KEY CLUSTERED 
(
	[ID_Detalle_Minuta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBL_DocumentosBatch]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_DocumentosBatch](
	[IdBatch] [float] NULL,
	[CodPoliza] [nvarchar](255) NULL,
	[Batch] [nvarchar](255) NULL,
	[PathDoc] [nvarchar](255) NULL,
	[Fuente] [nvarchar](255) NULL,
	[FecRegistro] [datetime] NULL,
	[UsuarioReg] [nvarchar](255) NULL,
	[SnIndexado] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBL_Ejecutivo]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_Ejecutivo](
	[ID_Ejecutivo] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](200) NOT NULL,
	[Posicion] [varchar](200) NOT NULL,
	[ID_Persona] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Ejecutivo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBL_Estado]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_Estado](
	[ID_Estado] [int] IDENTITY(1,1) NOT NULL,
	[Estado] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Estado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBL_Estado_Visita]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_Estado_Visita](
	[ID_Estado_Visita] [int] IDENTITY(1,1) NOT NULL,
	[Estado_Visita] [varchar](200) NOT NULL,
	[Usuario_Registro] [varchar](15) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Estado_Visita] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBL_Integrantes_Visitas]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_Integrantes_Visitas](
	[ID_Integrantes_Visitas] [int] IDENTITY(1,1) NOT NULL,
	[ID_Visita] [int] NOT NULL,
	[ID_Persona] [int] NOT NULL,
	[Nombre_Integrante] [varchar](50) NOT NULL,
	[Fecha_Registro] [datetime] NOT NULL,
	[Log_No] [int] NOT NULL,
	[Fecha_Modificacion] [datetime] NULL,
 CONSTRAINT [PK__TBL_Inte__A3AB2D57919CCAA8] PRIMARY KEY CLUSTERED 
(
	[ID_Integrantes_Visitas] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBL_Minuta]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_Minuta](
	[ID_Minuta] [int] IDENTITY(1,1) NOT NULL,
	[ID_Persona] [int] NOT NULL,
	[Contacto] [varchar](50) NOT NULL,
	[Fecha_Reunion] [datetime] NULL,
	[Motivo_Reunion] [varchar](100) NULL,
	[Estado_Prioridad] [varchar](50) NOT NULL,
	[Lugar] [varchar](max) NOT NULL,
	[Usuario_Registro] [int] NOT NULL,
	[Fecha_Registro] [datetime] NOT NULL,
	[ID_Estado] [int] NOT NULL,
 CONSTRAINT [PK_TBL_Minuta] PRIMARY KEY CLUSTERED 
(
	[ID_Minuta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBL_Motivo_Visita]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_Motivo_Visita](
	[ID_Motivo] [int] IDENTITY(1,1) NOT NULL,
	[Motivo] [varchar](200) NOT NULL,
	[Usuario_Registro] [varchar](15) NOT NULL,
	[Fecha_Modificacion] [datetime] NULL,
	[Fecha_Registro] [datetime] NOT NULL,
	[ID_Estado] [char](1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Motivo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBL_Persona]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_Persona](
	[ID_Persona] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](200) NOT NULL,
	[Apellido] [varchar](200) NOT NULL,
	[ID_Estado] [int] NOT NULL,
	[Fecha_Registro] [datetime] NOT NULL,
	[Tipo_Persona] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Persona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBL_Poliza]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_Poliza](
	[NumeroPoliza] [float] NULL,
	[Poliza] [nvarchar](255) NULL,
	[IDEstado] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBL_Usuario]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_Usuario](
	[ID_Usuario] [int] IDENTITY(1,1) NOT NULL,
	[Usuario] [varchar](50) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Email] [varchar](50) NULL,
	[Fecha_Modificacion] [datetime] NULL,
	[Fecha_Registro] [datetime] NOT NULL,
	[ID_Estado] [char](1) NOT NULL,
	[Clave] [varchar](max) NULL,
	[Token] [varbinary](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBL_Visita]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_Visita](
	[ID_Visita] [int] IDENTITY(1,1) NOT NULL,
	[ID_Contacto] [varchar](100) NOT NULL,
	[ID_Motivo_Visita] [int] NOT NULL,
	[ID_Estado_Visita] [int] NOT NULL,
	[ID_Usuario] [int] NOT NULL,
	[Direccion] [varchar](max) NOT NULL,
	[Motivo_Cambio] [varchar](200) NULL,
	[Fecha_Inicio_Visita] [datetime] NOT NULL,
	[Fecha_Fin_Visita] [datetime] NULL,
	[Fecha_Realizada_Visita] [datetime] NULL,
	[Fecha_Registro] [datetime] NOT NULL,
	[Fecha_Modificacion] [datetime] NULL,
	[Asunto] [varchar](max) NULL,
	[Comentario] [varchar](max) NULL,
	[Comentario_Cierre] [varchar](max) NULL,
	[HoraInicio] [varchar](10) NULL,
	[HoraFin] [varchar](10) NULL,
	[HoraRealizada] [varchar](10) NULL,
	[Comentario_Pospuesta] [varchar](max) NULL,
	[Responsable] [int] NOT NULL,
 CONSTRAINT [PK__TBL_Visi__015899C8F9C598C9] PRIMARY KEY CLUSTERED 
(
	[ID_Visita] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Vw_ConsultaPoliza]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vw_ConsultaPoliza](
	[Poliza] [varchar](30) NOT NULL,
	[Nombre Cliente] [varchar](403) NULL,
	[Aseguradora] [varchar](100) NULL,
	[Supervisor] [varchar](50) NOT NULL,
	[Ejecutivo] [varchar](50) NULL,
	[Unidad] [varchar](50) NULL,
	[Ramo] [varchar](50) NULL,
	[PersonaNumero] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TBL_Integrantes_Visitas] ADD  CONSTRAINT [DF__TBL_Integ__Fecha__5CD6CB2B]  DEFAULT (getdate()) FOR [Fecha_Registro]
GO
ALTER TABLE [dbo].[TBL_Motivo_Visita] ADD  DEFAULT (NULL) FOR [Fecha_Registro]
GO
ALTER TABLE [dbo].[TBL_Persona] ADD  DEFAULT (getdate()) FOR [Fecha_Registro]
GO
ALTER TABLE [dbo].[TBL_Visita] ADD  CONSTRAINT [DF__TBL_Visit__Fecha__37A5467C]  DEFAULT (NULL) FOR [Fecha_Registro]
GO
ALTER TABLE [dbo].[TBL_Asistentes]  WITH CHECK ADD  CONSTRAINT [FK_TBL_Asistentes_TBL_Minuta] FOREIGN KEY([ID_Minuta])
REFERENCES [dbo].[TBL_Minuta] ([ID_Minuta])
GO
ALTER TABLE [dbo].[TBL_Asistentes] CHECK CONSTRAINT [FK_TBL_Asistentes_TBL_Minuta]
GO
ALTER TABLE [dbo].[TBL_Detalle_Minuta]  WITH CHECK ADD  CONSTRAINT [FK_TBL_Detalle_Minuta_TBL_Minuta] FOREIGN KEY([ID_Minuta])
REFERENCES [dbo].[TBL_Minuta] ([ID_Minuta])
GO
ALTER TABLE [dbo].[TBL_Detalle_Minuta] CHECK CONSTRAINT [FK_TBL_Detalle_Minuta_TBL_Minuta]
GO
ALTER TABLE [dbo].[TBL_Ejecutivo]  WITH CHECK ADD FOREIGN KEY([ID_Persona])
REFERENCES [dbo].[TBL_Persona] ([ID_Persona])
GO
ALTER TABLE [dbo].[TBL_Minuta]  WITH CHECK ADD  CONSTRAINT [FK_TBL_Minuta_TBL_Estado] FOREIGN KEY([ID_Estado])
REFERENCES [dbo].[TBL_Estado] ([ID_Estado])
GO
ALTER TABLE [dbo].[TBL_Minuta] CHECK CONSTRAINT [FK_TBL_Minuta_TBL_Estado]
GO
ALTER TABLE [dbo].[TBL_Visita]  WITH CHECK ADD  CONSTRAINT [FK_TBL_Visita_TBL_Estado_Visita] FOREIGN KEY([ID_Estado_Visita])
REFERENCES [dbo].[TBL_Estado_Visita] ([ID_Estado_Visita])
GO
ALTER TABLE [dbo].[TBL_Visita] CHECK CONSTRAINT [FK_TBL_Visita_TBL_Estado_Visita]
GO
ALTER TABLE [dbo].[TBL_Visita]  WITH CHECK ADD  CONSTRAINT [FK_TBL_Visita_TBL_Motivo_visita] FOREIGN KEY([ID_Motivo_Visita])
REFERENCES [dbo].[TBL_Motivo_Visita] ([ID_Motivo])
GO
ALTER TABLE [dbo].[TBL_Visita] CHECK CONSTRAINT [FK_TBL_Visita_TBL_Motivo_visita]
GO
ALTER TABLE [dbo].[TBL_Visita]  WITH CHECK ADD  CONSTRAINT [FK_TBL_Visita_TBL_Persona] FOREIGN KEY([Responsable])
REFERENCES [dbo].[TBL_Persona] ([ID_Persona])
GO
ALTER TABLE [dbo].[TBL_Visita] CHECK CONSTRAINT [FK_TBL_Visita_TBL_Persona]
GO
ALTER TABLE [dbo].[TBL_Visita]  WITH CHECK ADD  CONSTRAINT [FK_TBL_Visita_TBL_Usuario] FOREIGN KEY([ID_Usuario])
REFERENCES [dbo].[TBL_Usuario] ([ID_Usuario])
GO
ALTER TABLE [dbo].[TBL_Visita] CHECK CONSTRAINT [FK_TBL_Visita_TBL_Usuario]
GO
/****** Object:  StoredProcedure [dbo].[SP_CompletarVisita]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_CompletarVisita]
@IdVisita int,
@HoraRealizada varchar(50),
@FechaRealizada datetime,
@ComentarioCierre varchar(150)
		
as 

update [dbo].[TBL_Visita] set HoraRealizada = @HoraRealizada, Fecha_realizada_visita = @FechaRealizada, 
Comentario_cierre = @ComentarioCierre, ID_Estado_Visita = 3 
where ID_Visita = @IdVisita

GO
/****** Object:  StoredProcedure [dbo].[SP_EditarVisita]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_EditarVisita]
@IdVisita int,
@Motivo int,
@Contacto varchar(50),
@Lugar varchar(100),
@Asunto varchar(100),
@Comentario varchar(150)
		
as 

update [dbo].[TBL_Visita] set ID_Motivo_visita = @Motivo,ID_Contacto = @Contacto,Direccion = @Lugar, Asunto = @Asunto, 
Comentario = @Comentario
where ID_Visita = @IdVisita

GO
/****** Object:  StoredProcedure [dbo].[SP_GetDataVisitas]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_GetDataVisitas]
		@id int
as 

select *,(select (select P.Nombre from TBL_Persona P where P.ID_Persona = IV.ID_Persona) [NombrePersona] from TBL_Integrantes_Visitas IV where IV.ID_Visita = V.ID_Visita) 

from [dbo].[TBL_Visita] V
where V.ID_Visita = @id
GO
/****** Object:  StoredProcedure [dbo].[SP_GetDataVisitasEvent]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_GetDataVisitasEvent]
		
as 

select 
(select cast(V.ID_Visita as varchar) +'-'+MV.Motivo from TBL_Motivo_visita MV where V.ID_Motivo_visita = MV.ID_Motivo) Titulo,
CONVERT(varchar,V.Fecha_Inicio_visita,23) +'T'+ V.HoraInicio FechaInicio,
CONVERT(varchar,V.Fecha_Inicio_visita,23) +'T'+ V.HoraFin FechaFin, 
case when V.ID_Estado_Visita = 1 then '#007bff' when V.ID_Estado_Visita = 2 then '#ffc107'  when V.ID_Estado_Visita = 3 then '#28a745' when V.ID_Estado_Visita = 4 then '#dc3545' end Color
from [dbo].[TBL_Visita] V
GO
/****** Object:  StoredProcedure [dbo].[SP_GetPersona]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GetPersona]
@criterio VARCHAR (100)
AS
SELECT ID_Persona,Nombre+' '+ apellido as NombreCompleto
FROM [DB_A502E1_SmartSys].[dbo].[TBL_Persona]
WHERE (convert(varchar(50),ID_Persona)+' '+Nombre+' '+ apellido) like '%'+@criterio+'%'
GO
/****** Object:  StoredProcedure [dbo].[SP_ListPersona]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_ListPersona]

as 

select ID_Persona,Nombre + '' + Apellido [Nombre] from TBL_Persona

GO
/****** Object:  StoredProcedure [dbo].[SP_PosponerVisita]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_PosponerVisita]
@IdVisita int,
@HoraInicioNueva varchar(50),
@HoraFinNueva varchar(50),
@FechaInicioNueva datetime,
@Comentario_Pospuesta varchar(max)
		
as 

update [dbo].[TBL_Visita] set HoraInicio = @HoraInicioNueva, HoraFin = @HoraFinNueva, Fecha_Inicio_visita = @FechaInicioNueva, 
ID_Estado_Visita = 2, Comentario_Pospuesta = @Comentario_Pospuesta
where ID_Visita = @IdVisita

GO
/****** Object:  StoredProcedure [dbo].[SP_ReporteDeVisita]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_ReporteDeVisita]
(@fechaDesde datetime,
@fechaHasta datetime)
AS
begin
SELECT [ID_Visita],[ID_Contacto],[ID_Motivo_Visita],[ID_Estado_Visita],[ID_Usuario]
,[Direccion],[Motivo_Cambio],[Fecha_Inicio_Visita],[Fecha_Fin_Visita],[Fecha_Realizada_Visita]
,[Fecha_Registro] ,[Fecha_Modificacion],[Asunto],[Comentario],[Comentario_Cierre] 
,[HoraInicio],[HoraFin],[HoraRealizada],[Comentario_Pospuesta]
FROM [DB_A502E1_SmartSys].[dbo].[TBL_Visita]
where [Fecha_Registro] between @fechaDesde and @fechaHasta
end
GO
/****** Object:  StoredProcedure [dbo].[SP_Save_Minuta]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_Save_Minuta]
(@IdPersona int 
,@Contacto varchar(50)
,@FechaReunion datetime
,@Motivo INT
,@Prioridad VARCHAR (20)
,@Lugar varchar(max)
,@Usuario_Registro int   
,@Estado int)
AS
begin 
set nocount on

INSERT INTO  DB_A502E1_SmartSys.[dbo].[TBL_Minuta] (ID_Persona,Contacto,Fecha_Reunion,Estado_Prioridad,Lugar,
Usuario_Registro,Fecha_Registro,ID_Estado)

values(@IdPersona ,@Contacto,@FechaReunion,@Prioridad 
,@Lugar  ,@Usuario_Registro ,GETDATE(),1)
      
end 
GO
/****** Object:  StoredProcedure [dbo].[SP_SAVE_VISITA]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_SAVE_VISITA]
(@ID_Contacto varchar(100) 
,@ID_Motivo_visita INT
,@ID_Estado_Visita INT
,@ID_Usuario INT
,@Direccion VARCHAR (200)
,@Fecha_Inicio_visita DATETIME
,@Fecha_Registro   DATETIME   
,@Asunto varchar (200)
,@Comentario  varchar (max)   
,@HoraInicio varchar (10)
,@HoraFin varchar (10)
,@Responsable int)
AS
begin 
set nocount on

INSERT INTO  DB_A502E1_SmartSys.[dbo].[TBL_Visita] (ID_Contacto ,ID_Motivo_visita,ID_Estado_Visita
,ID_Usuario ,Direccion ,Fecha_Inicio_visita 
,Fecha_Registro ,Asunto ,Comentario ,HoraInicio ,HoraFin,Responsable)

values(@ID_Contacto ,@ID_Motivo_visita,@ID_Estado_Visita,@ID_Usuario 
,@Direccion  ,@Fecha_Inicio_visita ,@Fecha_Registro   
,@Asunto ,@Comentario ,@HoraInicio ,@HoraFin,@Responsable)
      
end 
GO
/****** Object:  StoredProcedure [dbo].[SP_SaveItegrantes]    Script Date: 18/12/2019 04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_SaveItegrantes]
@IdVisita int,
@IdPersona int,
@Log int
,@Opcion int
as 
if @Opcion = 1 
begin
insert into [dbo].[TBL_Integrantes_Visitas](ID_Visita,ID_Persona,Nombre_Integrante,Fecha_Registro,Log_No,Fecha_Modificacion)
values(
@IdVisita,@IdPersona,(select Nombre +' '+ Apellido from TBL_Persona where ID_Persona = @IdPersona),GETDATE(),@Log,GETDATE()
)
end
else
begin
insert into [dbo].[TBL_Integrantes_Visitas](ID_Visita,ID_Persona,Nombre_Integrante,Fecha_Registro,Log_No,Fecha_Modificacion)
values(
@IdVisita,@IdPersona,(select Nombre +' '+ Apellido from TBL_Persona where ID_Persona = @IdPersona),GETDATE(),@Log,null
)
end
GO
USE [master]
GO
ALTER DATABASE [DB_A502E1_SmartSys] SET  READ_WRITE 
GO
