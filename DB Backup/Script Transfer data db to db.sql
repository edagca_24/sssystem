/****** Script for SelectTopNRows command from SSMS  ******/
insert into [DB_Smartsys3].[dbo].[TBL_Estado_Visita](
[Estado_Visita]
      ,[Usuario_Registro]
      ,[Fecha_Registro]

)
SELECT TOP (1000) [Estado_Visita]
      ,[Usuario_Registro]
      ,[Fecha_Registro]
  FROM [DB_Smartsys2].[dbo].[TBL_Estado_Visita]

  /****** Script for SelectTopNRows command from SSMS  ******/
insert into [DB_Smartsys3].[dbo].[TBL_Usuario](
 [Usuario]
      ,[Nombre]
      ,[Email]
      ,[Fecha_Modificacion]
      ,[Fecha_Registro]
      ,[ID_Estado]
      ,[Clave]
      ,[Token])

SELECT TOP (1000)
      [Usuario]
      ,[Nombre]
      ,[Email]
      ,[Fecha_Modificacion]
      ,[Fecha_Registro]
      ,[ID_Estado]
      ,[Clave]
      ,[Token]
  FROM [DB_Smartsys2].[dbo].[TBL_Usuario]

  /****** Script for SelectTopNRows command from SSMS  ******/
insert into [DB_Smartsys3].[dbo].[TBL_Ente](
 [Nombre]
      ,[ID_Estado]
      ,[Fecha_Registro]
      ,[Tipo_Ente]
)
SELECT 
      [Nombre]
      ,[ID_Estado]
      ,[Fecha_Registro]
      ,[Tipo_Ente]
  FROM DB_A502E1_Smartsys.[dbo].[TBL_Ente]
  where tipo_ente = 'Cliente'


  /****** Script for SelectTopNRows command from SSMS  ******/


  insert into [DB_Smartsys3].[dbo].[TBL_Motivo_Visita] (
  
      [Motivo]
      ,[Usuario_Registro]
      ,[Fecha_Modificacion]
      ,[Fecha_Registro]
  )

  SELECT TOP (1000)
      [Motivo]
      ,[Usuario_Registro]
      ,[Fecha_Modificacion]
      ,[Fecha_Registro]
  FROM [DB_Smartsys2].[dbo].[TBL_Motivo_Visita]