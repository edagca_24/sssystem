﻿using SmartSys.Models.Clases;
using System;
using System.Collections.Generic;
using System.IdentityModel.Metadata;
using System.Linq;
using System.Web;

namespace SmartSys.Models.ViewModels
{
    public class VMMinuta
    {
  

        public int IdPersona { get; set; }
        public int IdMinuta { get; set; }
        public string Contacto { get; set; }
        public DateTime Fecha { get; set; }
        public string MotivoReunion { get; set; }
        public string Prioridad { get; set; }
        public string Lugar { get; set; }
        public List<string> Asistente { get; set; }
        public List<AsignacionesMinuta> Asignaciones { get; set; }
        public string Resumen { get; set; }
        public int? IdMinutaTemporal { get; set; }
    }
}