﻿using SmartSys.Models.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartSys.Models.ViewModels
{
    public class VMMinutaDetails
    {
        public int IdMinutaDetalle { get; set; }
        public int ID_Minuta { get; set; }
        public string Cliente { get; set; }
        public string Contacto { get; set; }
        public string FechaReunion { get; set; }
        public string MotivoReunion { get; set; }
        public string Prioridad { get; set; }
        public string Lugar { get; set; }
        public string UsuarioRegistro { get; set; }
        public string FechaRegistro { get; set; }
        public List<AsistentesDeMinuta> Asistentes { get; set; }
        public List<AsignacionesMinuta> Asignaciones { get; set; }
        public string Resumen { get; set; }
        public string Condicion { get; set; }
        public string Fecha_Entrega { get; set; }
    }
}