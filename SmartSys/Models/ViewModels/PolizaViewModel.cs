﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartSys.Models.ViewModels
{
    public class PolizaViewModel
    {
        public string Poliza { get; set; }
        public string NombreCliente { get; set; }
        public string Aseguradora { get; set; }
        public string PersonaNumero { get; set; }
    }
}