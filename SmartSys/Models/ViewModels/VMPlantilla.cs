﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartSys.Models.ViewModels
{
    public class VMPlantilla
    {
        public List<string> NombreList { get; set; }
        public string NombrePlantilla { get; set; }

    }
}