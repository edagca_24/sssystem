﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartSys.Models.ViewModels
{
    public class VMVisita_Integrantes
    {
        public int ID_Visita { get; set; }
        public string ID_Contacto { get; set; }
        public int ID_Motivo_visita { get; set; }
        public int ID_Estado_Visita { get; set; }
        public int ID_Usuario { get; set; }
        public string Direccion { get; set; }
        public string Motivo_Cambio { get; set; }
        public System.DateTime Fecha_Inicio_visita { get; set; }
        public System.DateTime Fecha_Fin_visita { get; set; }
        public Nullable<System.DateTime> Fecha_realizada_visita { get; set; }
        public System.DateTime Fecha_Registro { get; set; }
        public Nullable<System.DateTime> Fecha_Modificacion { get; set; }
        public string Asunto { get; set; }
        public string Comentario { get; set; }
        public string Comentario_cierre { get; set; }
        public string HoraInicio { get; set; }
        public string HoraFin { get; set; }
        public string HoraRealizada { get; set; }

        public virtual TBL_Estado_Visita TBL_Estado_Visita { get; set; }
        public virtual TBL_Motivo_Visita TBL_Motivo_visita { get; set; }
        public virtual TBL_Usuario TBL_Usuario { get; set; }
        public int ID_Integrantes_Visitas { get; set; }       
        public int ID_Persona { get; set; }
        public string Nombre_Integrante { get; set; }
        public System.DateTime Fecha_RegistroIntegrante { get; set; }
        public string Responsable { get; set; }
        public int Log_No { get; set; }
    }
}