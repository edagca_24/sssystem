﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartSys.Models.ViewModels
{
    public class VMEventVisita
    {
        public string title { get; set; }        
        public string start { get; set; }
        public string end { get; set; }
        public string backgroundColor { get; set; }
    }
}