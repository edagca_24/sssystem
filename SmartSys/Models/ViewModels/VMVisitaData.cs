﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartSys.Models.ViewModels
{
    public class VMVisitaData
    {
        public int IDEntidad { get; set; }
        public string Entidad { get; set; }
        public string Contacto { get; set; }
        public int ID_Motivo { get; set; }
        public string Motivo { get; set; }     
        public string Direccion { get; set; }        
        public string Asunto { get; set; }      
        public string HoraInicio { get; set; }
        public string HoraFin { get; set; }
        public System.DateTime FechaReunion { get; set; }
        public int ID_Responsable { get; set; }
        public string Responsable { get; set; }
        public string Mes { get; set; }
    }
}