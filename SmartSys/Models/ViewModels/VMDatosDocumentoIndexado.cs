﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartSys.Models.ViewModels
{
    public class VMDatosDocumentoIndexado
    {
        public int IdImagen { get; set; }
        public string Batch { get; set; }
        public string Poliza { get; set; }
        public string Cliente { get; set; }
        public string Dependientes { get; set; }
        public string Parentesco { get; set; }
        public string TipoDoc { get; set; }
        public string Aseguradora { get; set; }
        public string FecRegistro { get; set; }
        public string Titular { get; set; }
        public string PathDoc { get; set; }
        public string NroDocumento { get; set; }
        public string UsuarioReg { get; set; }

    }
}