﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartSys.Models.ViewModels
{
    public class VMMinutaDatos
    {
     
        public int ID_Minuta { get; set; }    
        public string Contacto { get; set; }
        public DateTime? FechaReunion { get; set; }
        public string MotivoReunion { get; set; }
        public string Prioridad { get; set; }
        public string Lugar { get; set; }
        public string Condicion { get; set; }
        public string Cliente { get; set; }
        public int? IdVisita { get; set; }
        
    }
}