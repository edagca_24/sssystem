﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Threading.Tasks;
using Aspose.Email;
using Aspose.Email.Calendar;
using System.Net.Mail;

namespace SmartSys.Models.Clases
{
    public class SendEmailAndAppoiment
    {

        public bool AppoimentCreator(string mailbody, string mailsubjet,string mailOrganizer, List<string> attemptes, DateTime Datefrom, DateTime Dateto, string AppAddress)
        {
            bool result = false;
            int count = attemptes.Count;
            string[] correosto = new string[count];
            try
            {
                Aspose.Email.MailAddressCollection localattemptes = new Aspose.Email.MailAddressCollection();
                int c = 0;
                foreach (string item in attemptes)
                {
                    localattemptes.Add(new Aspose.Email.MailAddress(item));
                    correosto[c] = item;
                    c++;
                }
                Appointment app = new Appointment(AppAddress,mailsubjet, mailbody, Datefrom, Dateto, new Aspose.Email.MailAddress("noreply_smartsys@ros.com.do"), AppAddress);
                Aspose.Email.MailMessage msg = new Aspose.Email.MailMessage();
                msg.From = new Aspose.Email.MailAddress("noreply_smartsys@ros.com.do");

                for (int v = 0; v < c; v++)
                {
                    msg.To.Add(new Aspose.Email.MailAddress(correosto[v]));
                }
                
                msg.Subject = mailsubjet;
                msg.Body = mailbody;
                msg.AddAlternateView(app.RequestApointment());
                Aspose.Email.Clients.Smtp.SmtpClient Mailclient = new Aspose.Email.Clients.Smtp.SmtpClient("accesoros.saas24x7.com", 587, "noreply_smartsys@ros.com.do", "Smartsys2020$");
                Mailclient.UseDefaultCredentials = false;

                Mailclient.Send(msg);
                result = true;

            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;


        }

        public bool SendEmail(string mailbody,string mailsubjet, string attemptes)
        {
            bool result = false;
            //int count = attemptes.Count;
            //string[] correosto = new string[count];
            int c = 0;
            System.Net.Mail.MailAddressCollection mailAddresses = new System.Net.Mail.MailAddressCollection();

            //foreach (string item in attemptes)
            //{
            //    mailAddresses.Add(new System.Net.Mail.MailAddress(item));
            //    correosto[c] = item;
            //    c++;
            //}
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
            message.From = new System.Net.Mail.MailAddress("noreply_smartsys@ros.com.do");
            //for (int v = 0; v < c; v++)
            //{
            //    message.To.Add(new System.Net.Mail.MailAddress(correosto[v]));
            //}
            
            message.To.Add(attemptes);
            message.Subject = mailsubjet;
            message.Body = mailbody;
            message.BodyEncoding = Encoding.UTF8;
            message.IsBodyHtml = true;
            SmtpClient client = new SmtpClient(); //Gmail smtp    
            client.Host = "accesoros.saas24x7.com";//"pod51012.outlook.com";
            client.Port = 587;
            System.Net.NetworkCredential basicCredential1 = new System.Net.NetworkCredential("noreply_smartsys@ros.com.do", "Smartsys2020$");
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            client.Credentials = basicCredential1;
            //client.TargetName = "STARTTLS/smtp.gmail.com";
            try
            {
                client.Send(message);
            }

            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }
    }
}