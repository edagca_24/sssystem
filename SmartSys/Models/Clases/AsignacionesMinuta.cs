﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartSys.Models.Clases
{
    public class AsignacionesMinuta
    {
        public int IdAsignacion { get; set; }
        public int IdAsignado { get; set; }
        public string Asignado { get; set; }
        public string Tarea { get; set; }  
        public string Acuerdo { get; set; }  
        public string Recomendacion { get; set; }  
        public string Condicion { get; set; }  
        public string Fecha_Entrega { get; set; }  

    }
}