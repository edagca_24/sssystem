﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartSys.Models.Clases
{
    public class AsistentesDeMinuta
    {
        public int? IdAsistente { get; set; }
        public string Asistente { get; set; }       

    }
}