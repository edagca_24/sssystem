﻿using System.ComponentModel.DataAnnotations;

namespace Digitalizacion.Models
{
    public class BatchImagenes
    {
        [Key]
        public int IdImagen { get; set; }
        public string Batch { get; set; }
        public string TipoDoc { get; set; }
        public string PathDoc { get; set; }
    }
}
