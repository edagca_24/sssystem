﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmartSys.Controllers;
using SmartSys.Models;

namespace SmartSys.Filter
{
    public class Verificar : ActionFilterAttribute
    {
        private TBL_Usuario oUser;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
            base.OnActionExecuting(filterContext);

                oUser = (TBL_Usuario)HttpContext.Current.Session["Nombre"];
                if (oUser == null && filterContext.ActionDescriptor.ActionName != "RecuperarContraseña")
                {
                    if (filterContext.Controller is AccesoController == false)
                    {
                        filterContext.HttpContext.Response.Redirect("/Acceso/Login");
                    }
                }
                
               
            }
            catch(Exception)
            {
                //filterContext.HttpContext.Response.Redirect("/Acceso/Login");
            }

        }
    }
}