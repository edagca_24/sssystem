﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmartSys.Models;


namespace SmartSys.Controllers
{

    public class AccesoController : Controller
    {

        DB_Smartsys3Entities db = new DB_Smartsys3Entities();
        // GET: Acceso
        public ActionResult Login()
        {
            if (Session["Nombre"] is null)
            {

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        public ActionResult Login(TBL_Usuario usuario)
        {
            var user = Request.Form["Usuario"];
            var contraseña = Request.Form["contraseña"];

            if (user != "" && contraseña != "")
            {
                var data = db.TBL_Usuario.Where(a => a.Usuario.Equals(user) && a.Clave.Equals(contraseña.ToString())).FirstOrDefault();
                if (data != null)
                {
                    Session["IdUsuario"] = data.ID_Usuario.ToString();
                    Session["Usuario"] = data.Usuario.ToString();
                    Session["Nombre"] = data.Nombre.ToString();

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                        ViewBag.mensaje = "Usuario o contraseña incorrecta.";
                        return View(); 
                }
            }
            return View(usuario);
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Login", "Acceso");
            //return View();
        }
    }
}