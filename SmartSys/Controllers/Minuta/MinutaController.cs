﻿using Newtonsoft.Json;
using SmartSys.Models;
using SmartSys.Models.Clases;
using SmartSys.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using SmartSys.CorreoRos;
using System.Xml.Linq;
using System.Xml;
using System.IO;


namespace SmartSys.Controllers.Minuta
{
    public class MinutaController : Controller
    {
        DB_Smartsys3Entities db = new DB_Smartsys3Entities();
        
        // GET: Minuta
        public ActionResult IndexMinuta()
        {

            int IdUsuario = Convert.ToInt32(Session["IdUsuario"]);
            List<VMMinutaDatos> data = new List<VMMinutaDatos>();

            var minutas = (from M in db.TBL_Minuta
                           join DM in db.TBL_Detalle_Minuta on M.ID_Minuta equals DM.ID_Minuta

                           where DM.ID_Usuario_Asignado == IdUsuario || M.Usuario_Registro == IdUsuario

                           select new {
                               M.ID_Minuta,
                               M.Lugar,
                               M.Motivo_Reunion,
                               M.Fecha_Reunion,
                               M.Contacto,
                               M.Estado_Prioridad,
                               M.ID_Visita,
                              Cliente = M.TBL_Ente.Nombre
                           }).Distinct().ToList();

            for (int i = 0; i < minutas.Count; i++)
            {
                var minuta = minutas[i].ID_Minuta;
                var log = db.TBL_Detalle_Minuta.Where(e => e.ID_Minuta == minuta).Max(e => e.Log_No);
                var detalle = db.SP_GetDataMinutaEstado(minutas[i].ID_Minuta,log).ToList();

                bool contador = true;
                foreach (var item in detalle)
                {
                    if (item == "En Proceso")
                    {
                        contador = false;
                    }
                }


                data.Add(new VMMinutaDatos { ID_Minuta = minutas[i].ID_Minuta,
                Contacto = minutas[i].Contacto,
                FechaReunion = minutas[i].Fecha_Reunion,
                MotivoReunion = minutas[i].Motivo_Reunion,
                Prioridad = minutas[i].Estado_Prioridad,
                Lugar = minutas[i].Lugar,
                Condicion = contador == false ? "En Proceso" : "Completada",
                Cliente = minutas[i].Cliente,
                IdVisita = minutas[i].ID_Visita
                });

        }
    

            return View(data.ToList());
        }

        // View: Minuta
        public ActionResult CreateMinuta()
        {
            ViewBag.MinutasTemporales = new SelectList(db.SP_GetMinutasTemporalesList(), "ID", "NAME");
            return View();
        }

        // POST: Minuta
        [HttpPost]
        public JsonResult SaveMinuta(VMMinuta minuta)
        {
            ClearSessionIdVisita();
            try
            {
                if (minuta.IdMinutaTemporal != 0)
                {
                    var updateMinutaTemp = db.TBL_Minuta_Temporal.Where(e => e.ID_Minuta_Temporal == minuta.IdMinutaTemporal).FirstOrDefault();
                    updateMinutaTemp.Condicion = 1;
                    db.SaveChanges();
                }
                

                db.SP_Save_Minuta(minuta.IdPersona, minuta.Contacto, minuta.Fecha, minuta.MotivoReunion, minuta.Prioridad, minuta.Lugar, Convert.ToInt32(Session["IdUsuario"]), minuta.Resumen);

                var IdMinuta = db.TBL_Minuta.Max(e => e.ID_Minuta).ToString();

                if (minuta.Asistente != null)
                {

                    var asistentes = new TBL_Asistentes();

                    for (int i = 0; i < minuta.Asistente.Count; i++)
                    {
                        asistentes.ID_Minuta = Convert.ToInt32(IdMinuta);
                        asistentes.Nombre_Asistente = minuta.Asistente[i];
                        asistentes.Fecha_Registro = DateTime.Now;

                        db.TBL_Asistentes.Add(asistentes);
                        db.SaveChanges();
                    }



                }

                if (minuta.Asignaciones != null)
                {

                    foreach (var item in minuta.Asignaciones)
                    {
                        var fechaentrega = Convert.ToDateTime(item.Fecha_Entrega);
                        var detalleminuta = new TBL_Detalle_Minuta();
                        detalleminuta.ID_Minuta = Convert.ToInt32(IdMinuta);
                        detalleminuta.ID_Usuario_Asignado = item.IdAsignado;
                        detalleminuta.Tarea = item.Tarea;
                        detalleminuta.Acuerdo = item.Acuerdo;
                        detalleminuta.Recomendacion = item.Recomendacion;
                        detalleminuta.Fecha_Registro = DateTime.Now;
                        detalleminuta.Condicion = "En Proceso";
                        detalleminuta.Fecha_Entrega = fechaentrega;
                        detalleminuta.Log_No = 1;
                        detalleminuta.Condicion_Registro = "0";


                        db.TBL_Detalle_Minuta.Add(detalleminuta);
                        db.SaveChanges();
                    }
                }
                sendEmailAsignadosMinuta(Convert.ToInt32(IdMinuta));
                sendEmailCreador(Convert.ToInt32(IdMinuta));

            }
            catch (Exception e)
            {
                return Json("0", JsonRequestBehavior.AllowGet);
                throw;
            }
            return Json("1", JsonRequestBehavior.AllowGet);
        }

        
        public JsonResult GetData(string searchTerm, string tipo)
       {
            tipo = tipo.ToString();
            var data = db.SP_GetPersona(searchTerm, tipo).ToList();
            var modifiedData = data.Select(x => new
            {
                id = x.ID,
                text = x.NombreCompleto
            });

            return Json(modifiedData, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAsistentes(string searchTerm, string tipo)
        {
            tipo = tipo.ToString();
            var data = db.SP_GetPersona(searchTerm, tipo).ToList();
            var modifiedData = data.Select(x => new
            {
                id = x.NombreCompleto,
                text = x.NombreCompleto
            });

            return Json(modifiedData, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetEnte(string criterio)
        {
            if (criterio == "")
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            var data = db.SP_GetPersona(criterio, "Cliente").ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

            //[HttpGet]
            //public JsonResult GetAsistentes()
            //{

            //    var asistentes = db.SP_ListPersona().ToList();
            //    //var data = db.TBL_Ente.Where(e => e.Tipo_Ente == tipoente).Select(e => new
            //    //{
            //    //    IdAsistente = e.ID_Ente,
            //    //    Asistente = e.Nombre
            //    //}).ToList();

            //    return Json(asistentes, JsonRequestBehavior.AllowGet);
            //}

        [HttpGet]
        public JsonResult GetUsuarios(string criterio)
       {
            if (criterio == "")
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            var data = (from u in db.TBL_Usuario
                          where u.Nombre.Contains(criterio)
                          select new {
                              IdUsuario = u.ID_Usuario,
                              Usuario = u.Nombre
                          }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetDetailsMinuta(int? Id)
        {
            int IdUsuario = Convert.ToInt32(Session["IdUsuario"]);


            try
            {

           
            var log = db.TBL_Detalle_Minuta.Where(e => e.ID_Minuta == Id).Max(e => e.Log_No);

            var data = db.SP_GetDetailMinuta(Id,log).Select(e => new
            {
                IdMinutaDetalle = e.ID_Detalle_Minuta,
                IdMinuta = e.ID_Minuta,
                UsuarioCreador = e.UsuarioCreador,
                IdUsuarioCreador = e.Usuario_Registro,
                Contacto = e.Contacto,
                Motivo = e.Motivo_Reunion,
                Prioridad = e.Estado_Prioridad,
                Lugar = e.Lugar,
                Fecha = e.Fecha_Reunion,
                FechaRegistro = e.Fecha_Registro,
                Cliente = e.Cliente,
                Asignado = e.Asignado,
                Tarea = e.Tarea,
                Acuerdo = e.Acuerdo,
                Recomendacion = e.Recomendacion,
                Resumen = e.Resumen_Minuta,
                Condicion = e.Condicion,
                FechaCompletada = e.Fecha_Completada,
                e.Fecha_Entrega

            }).ToList();
            //Fecha = SqlFunctions.DatePart("MM", e.Fecha_Reunion) + "/" + SqlFunctions.DateName("DD", e.Fecha_Reunion) + "/" + SqlFunctions.DateName("YYYY", e.Fecha_Reunion),
            //    FechaRegistro = SqlFunctions.DatePart("MM", e.Fecha_Registro) + "/" + SqlFunctions.DateName("DD", e.Fecha_Registro) + "/" + SqlFunctions.DateName("YYYY", e.Fecha_Registro),
            /////////////////////////////////////
            var asistentes = db.TBL_Asistentes.Where(e => e.ID_Minuta == Id).Select(e => new
            {
                IdAsistente = e.ID_Asistentes,
                NombreAsistente = e.Nombre_Asistente
            }).ToList();

            List<AsistentesDeMinuta> ListAsistentes = new List<AsistentesDeMinuta>();

            foreach (var item in asistentes)
            {
                ListAsistentes.Add(new AsistentesDeMinuta { IdAsistente = item.IdAsistente, Asistente = item.NombreAsistente });
            }
            //////////////////////////////////////

            List<AsignacionesMinuta> ListAsignaciones = new List<AsignacionesMinuta>();
            if (data[0].IdUsuarioCreador == IdUsuario)
            {
                var asignaciones = db.TBL_Detalle_Minuta.Where(e => e.ID_Minuta == Id && e.Log_No == log).Select(e => new
                {
                    IdAsignacion = e.ID_Detalle_Minuta,
                    IdAsignado = e.ID_Usuario_Asignado,
                    Asignado = e.TBL_Usuario.Nombre,
                    Tarea = e.Tarea,
                    Acuerdo = e.Acuerdo,
                    Recomendacion = e.Recomendacion,
                    Condicion = e.Condicion,
                    Fecha_Entrega = SqlFunctions.DatePart("MM", e.Fecha_Entrega) + "/" + SqlFunctions.DateName("DD", e.Fecha_Entrega) + "/" + SqlFunctions.DateName("YYYY", e.Fecha_Entrega)
                }).ToList();



                foreach (var item in asignaciones)
                {
                    ListAsignaciones.Add(new AsignacionesMinuta { IdAsignacion = item.IdAsignacion,IdAsignado = item.IdAsignado, Asignado = item.Asignado, Tarea = item.Tarea, Acuerdo = item.Acuerdo == null ? "" : item.Acuerdo, Recomendacion = item.Recomendacion == null ? "" : item.Recomendacion, Condicion = item.Condicion, Fecha_Entrega = Convert.ToString(item.Fecha_Entrega) });
                }
            }
            else
            {
                var asignaciones = db.TBL_Detalle_Minuta.Where(e => e.ID_Minuta == Id && e.Log_No == log && e.ID_Usuario_Asignado == IdUsuario).Select(e => new
                {
                    IdAsignacion = e.ID_Detalle_Minuta,
                    Asignado = e.TBL_Usuario.Nombre,
                    Tarea = e.Tarea,
                    Acuerdo = e.Acuerdo,
                    Recomendacion = e.Recomendacion,
                    Condicion = e.Condicion,
                    Fecha_Entrega = SqlFunctions.DatePart("MM", e.Fecha_Entrega) + "/" + SqlFunctions.DateName("DD", e.Fecha_Entrega) + "/" + SqlFunctions.DateName("YYYY", e.Fecha_Entrega)
                }).ToList();



                foreach (var item in asignaciones)
                {
                    ListAsignaciones.Add(new AsignacionesMinuta { IdAsignacion = item.IdAsignacion, Asignado = item.Asignado, Tarea = item.Tarea, Acuerdo = item.Acuerdo == null ? "" : item.Acuerdo, Recomendacion = item.Recomendacion == null ? "" : item.Recomendacion, Condicion = item.Condicion, Fecha_Entrega = Convert.ToString(item.Fecha_Entrega) });
                }
            }

   
            ///////////////////////////////////////
            var detalle = new VMMinutaDetails();
            detalle.IdMinutaDetalle = data[0].IdMinutaDetalle;
            detalle.ID_Minuta = data[0].IdMinuta;
            detalle.UsuarioRegistro = data[0].UsuarioCreador;
            detalle.Contacto = data[0].Contacto;
            detalle.MotivoReunion = data[0].Motivo;
            detalle.Prioridad = data[0].Prioridad;
            detalle.Lugar = data[0].Lugar;
            detalle.FechaReunion = Convert.ToString(data[0].Fecha);
            detalle.FechaRegistro = Convert.ToString(data[0].FechaRegistro);
            detalle.Cliente = data[0].Cliente;
            detalle.Asistentes = ListAsistentes;
            detalle.Asignaciones = ListAsignaciones;
            detalle.Resumen = data[0].Resumen;
            detalle.Condicion = data[0].Condicion;
                detalle.Fecha_Entrega = Convert.ToString(data[0].Fecha_Entrega);
          


            return Json(detalle, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e, JsonRequestBehavior.AllowGet);
                throw;
            }
        }


        public void sendEmailCreador(int Id)
        {
            StringBuilder body = new StringBuilder();
            SendEmailAndAppoiment mail = new SendEmailAndAppoiment();
            var contador = 1;
         
           
            ///////////////////////////////////////////////
            ///Envio a los que se le asigna una tarea
            try
            {

                ///Envio al creador de la minuta

                var dataminuta2 = (from M in db.TBL_Minuta
                                  join DM in db.TBL_Detalle_Minuta on M.ID_Minuta equals DM.ID_Minuta
                             
                                  join US in db.TBL_Usuario on DM.ID_Usuario_Asignado equals US.ID_Usuario
                                  where DM.ID_Minuta == Id
                                  select new
                                  {
                                      Correo = US.Email,
                                      Asunto = M.Motivo_Reunion,
                                      Resumen = M.Resumen_Minuta,
                                      Usuario = DM.ID_Usuario_Asignado
                                  }).ToList();

             
   
                    var to = dataminuta2[0].Correo;
                    var subject = "Minuta de: " + dataminuta2[0].Asunto;

                  
                        body.Append("<p style='font-weight: normal'>Buenas,</p>");
                  

                    body.Append(dataminuta2[0].Resumen);
                    var asignaciones = (from DM in db.TBL_Detalle_Minuta
                                        where DM.ID_Minuta == Id
                                        select new
                                        {
                                            DM.Tarea,
                                            DM.Acuerdo,
                                            DM.Recomendacion,
                                            DM.Fecha_Entrega,
                                            DM.TBL_Usuario.Nombre
                                        }).ToList();
                    foreach (var item in asignaciones)
                    {
                        body.Append("<ul>");
                        body.Append("<li><span style='font-weight: bold'>Asignado a: </span><p style='font-weight: normal'>" + item.Nombre + "</p></li>");
                        body.Append("<li><span style='font-weight: bold'>Tarea: </span><p style='font-weight: normal'>" + item.Tarea + "</p></li>");
                        body.Append("<li><span style='font-weight: bold'>Acuerdo: </span><p style='font-weight: normal'>" + item.Acuerdo + "</p></li>");
                        body.Append("<li><span style='font-weight: bold'>Recomendaciones: </span><p style='font-weight: normal'>" + item.Recomendacion + "</p></li>");
                        body.Append("<li><span style='font-weight: bold'>Fecha de Entrega: </span><p style='font-weight: normal'>" + item.Fecha_Entrega.ToString() + "</p></li><br>");
                        body.Append("</ul>");
                    }
              
                        body.Append("<p style='font-weight: normal'>Saludos</p><br>");
                    
                    mail.SendEmail(body.ToString(), subject, to);

                    contador++;
                

            }
            catch (Exception e) 
            {

                throw;
            }
            
            //mailTHMLRequestBody mail = new mailTHMLRequestBody();
           
       


         
        }

        public void sendEmailAsignadosMinuta(int Id)
        {
            StringBuilder body = new StringBuilder();
            SendEmailAndAppoiment mail = new SendEmailAndAppoiment();
            var contador = 1;

            var dataminuta = (from M in db.TBL_Minuta
                              join DM in db.TBL_Detalle_Minuta on M.ID_Minuta equals DM.ID_Minuta
                              join US in db.TBL_Usuario on DM.ID_Usuario_Asignado equals US.ID_Usuario
                              where DM.ID_Minuta == Id
                              select new
                              {
                                  Correo = US.Email,
                                  Asunto = M.Motivo_Reunion,
                                  Resumen = M.Resumen_Minuta,
                                  Usuario = DM.ID_Usuario_Asignado
                              }).ToList();

            foreach (var item in dataminuta)
            {
              
                var asignaciones = (from DM in db.TBL_Detalle_Minuta
                                    where DM.ID_Minuta == Id && DM.ID_Usuario_Asignado == item.Usuario
                                    select new
                                    {
                                        DM.Tarea,
                                        DM.Acuerdo,
                                        DM.Recomendacion,
                                        DM.Fecha_Entrega
                                    }).ToList();



                var to = item.Correo;
                var subject = dataminuta[0].Asunto;
                if (contador == 1)
                {
                    body.Append("<p>Buenas,</p>");
                }
                body.Append(item.Resumen);
                body.Append("<ul>");
                body.Append("<li><span style='font-weight: bold'>Tarea: </span><p>" + asignaciones[0].Tarea + "</p></li>");
                body.Append("<li><span style='font-weight: bold'>Acuerdo: </span><p>" + asignaciones[0].Acuerdo + "</p></li>");
                body.Append("<li><span style='font-weight: bold'>Recomendacion: </span><p>" + asignaciones[0].Recomendacion + "</p></li>");
                body.Append("<li><span style='font-weight: bold'>Fecha de Entrega: </span><p>" + asignaciones[0].Fecha_Entrega.ToString() + "</p></li><br>");
                body.Append("</ul>");
                
                if (contador == 1)
                {
                    body.Append("<p>Saludos</p><br>");
                }
                mail.SendEmail(body.ToString(), subject, to);
                contador++;
            }
        }

        [HttpPost]
        public JsonResult conpletarAsignacion(int? Id)
        {
            try
            {
                db.SP_CompletarAsignacion(Id);
            }
            catch (Exception e)
            {
                return Json("0", JsonRequestBehavior.AllowGet);
                throw;
            }
            return Json("1", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetDataMinutaFromVisita()
        {

            //var  IVisita = Convert.ToInt32(id);

            List<object> listdata = new List<object>();
            try
            {
                if (Session["IdVisita"].ToString() != "")
                {
                    var IVisita = Convert.ToInt32(Session["IdVisita"]);
                     var query = (from V in db.TBL_Visita
                                 where V.ID_Visita == IVisita
                                  select new
                                 {
                                     IdCliente = V.ID_Ente,
                                     Nombre = V.TBL_Ente.Nombre,
                                     Contacto = V.Contacto,
                                     Motivo = V.TBL_Motivo_Visita.Motivo,
                                     Direccion = V.Direccion,
                                     Fecha = SqlFunctions.DateName("YYYY", V.Fecha_Inicio_Visita) + "-" + SqlFunctions.DatePart("MM", V.Fecha_Inicio_Visita) + "-" + SqlFunctions.DateName("DD", V.Fecha_Inicio_Visita),
                                 }).ToList();

                    listdata.Add(query);       

                }

                return Json(listdata, JsonRequestBehavior.AllowGet);


                //VMVisitaData data  = new VMVisitaData();
                //data.IDEntidad = query[0].IdCliente;
                //data.Entidad = query[0].Nombre;
                //data.Contacto = query[0].Contacto;
                //data.Motivo = query[0].Motivo;
                //data.Direccion = query[0].Direccion;
                //data.FechaReunion = query[0].Fecha;



            }
            catch (Exception e)
            {
                return Json('0', JsonRequestBehavior.AllowGet);
                throw;
            }           
        }

        [HttpPost]
        public void ClearSessionIdVisita()
        {
            Session["IdVisita"] = "";
        }

        [HttpGet]
        public ActionResult GetDataMinutaTemporal(int Id)
        {
            var retorna = "";
            try
            {
                var query = (from V in db.TBL_Minuta_Temporal
                             where V.ID_Minuta_Temporal == Id
                             select new { 
                             NombreCliente = V.TBL_Ente.Nombre,
                             IdCliente = V.ID_Ente,
                             V.Contacto,
                             FechaReunion = SqlFunctions.DateName("YYYY", V.Fecha_Reunion) + "-" + SqlFunctions.DatePart("MM", V.Fecha_Reunion) + "-" + SqlFunctions.DateName("DD", V.Fecha_Reunion)  ,
                             Motivo = V.Motivo_Reunion,
                             V.Lugar
                             }).ToList();

                //foreach (var item in query)
                //{
                //    retorna = " var newOption = new Option('" + item.TBL_Ente.Nombre +"', '"+ item.ID_Ente +"', true, true);";
                //    retorna += "$('#ClienteM').append(newOption).trigger('change');";
                //    retorna += "$('#Contacto').val(" + item.Contacto + ");";
                //    retorna += "$('#Fecha').val(" + item.Fecha_Reunion + ");";
                //    retorna += "$('#MotivoReunion').val(" + item.Motivo_Reunion + ");";
                //    retorna += "$('#lugar').text(" + item.Lugar + ");";
      

                //}

                

                return Json(query, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json("0", JsonRequestBehavior.AllowGet);
                throw;
            }
            
        }

        [HttpPost]
        public ActionResult EditarMinuta(VMMinuta minuta)
        {

            try
            {
                if (minuta.Resumen != "")
                {
                    var updateminuta = db.TBL_Minuta.Find(minuta.IdMinuta);
                    updateminuta.Resumen_Minuta = minuta.Resumen;
                    db.SaveChanges();
                }
               

                var exist = (from c in db.TBL_Detalle_Minuta
                             where c.ID_Minuta == minuta.IdMinuta
                             select new { c.Log_No }).ToList();


                if (minuta.Asignaciones != null)
                {
                    if (exist.Count != 0)
                    {

                        var log = db.TBL_Detalle_Minuta.Where(e => e.ID_Minuta == minuta.IdMinuta).Max(e => e.Log_No) + 1;                  
          
                        foreach (var item in minuta.Asignaciones)
                        {
                    
                                var fechaentrega = Convert.ToDateTime(item.Fecha_Entrega);
                                var detalleminuta = new TBL_Detalle_Minuta();
                                detalleminuta.ID_Minuta = Convert.ToInt32(minuta.IdMinuta);
                                detalleminuta.ID_Usuario_Asignado = item.IdAsignado;
                                detalleminuta.Tarea = item.Tarea;
                                detalleminuta.Acuerdo = item.Acuerdo;
                                detalleminuta.Recomendacion = item.Recomendacion;
                                detalleminuta.Fecha_Registro = DateTime.Now;
                                detalleminuta.Condicion = "En Proceso";
                                detalleminuta.Fecha_Entrega = fechaentrega;
                                detalleminuta.Log_No = log;
                                detalleminuta.Condicion_Registro = "0";


                                db.TBL_Detalle_Minuta.Add(detalleminuta);
                                db.SaveChanges();
                            
                           
                        }

                    }
                    else
                    {                      

                        foreach (var item in minuta.Asignaciones)
                        {
                            var fechaentrega = Convert.ToDateTime(item.Fecha_Entrega);
                            var detalleminuta = new TBL_Detalle_Minuta();
                            detalleminuta.ID_Minuta = Convert.ToInt32(minuta.IdMinuta);
                            detalleminuta.ID_Usuario_Asignado = item.IdAsignado;
                            detalleminuta.Tarea = item.Tarea;
                            detalleminuta.Acuerdo = item.Acuerdo;
                            detalleminuta.Recomendacion = item.Recomendacion;
                            detalleminuta.Fecha_Registro = DateTime.Now;
                            detalleminuta.Condicion = "En Proceso";
                            detalleminuta.Fecha_Entrega = fechaentrega;
                            detalleminuta.Log_No = 1;
                            detalleminuta.Condicion_Registro = "0";


                            db.TBL_Detalle_Minuta.Add(detalleminuta);
                            db.SaveChanges();
                        }
                    }
                }
                else
                {
                    if (exist.Count != 0)
                    {
                        var log = db.TBL_Detalle_Minuta.Where(e => e.ID_Minuta == minuta.IdMinuta).Max(e => e.Log_No);
                        db.SP_DeleteAsignacion(minuta.IdMinuta, log);
                    }
                }

            }
            catch (Exception e)
            {
                return Json('0', JsonRequestBehavior.AllowGet);
                throw;
            }

            return Json("1", JsonRequestBehavior.AllowGet);
        }

    }
}