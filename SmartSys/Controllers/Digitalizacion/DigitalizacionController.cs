﻿using SmartSys.Models;
using SmartSys.Models.Clases;
using SmartSys.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace SmartSys.Controllers.Digitalizacion
{
    public class DigitalizacionController : Controller
    {
        AdoConnections.conneciones referencia = new AdoConnections.conneciones();
        DB_Smartsys3Entities db = new DB_Smartsys3Entities();
        string Localpoliza;


        #region Vistas Digitalizacion

        // GET: Digitalizacion

        // carga de la pagina de Digitalizacion
        public ActionResult Inicio()
        {
            //Limpiamos los archivos temporales_____________________//

            erasetemp();
            return View();


        }

        [HttpPost]
        public void erasetemp()
        {
            string ruta = Server.MapPath("/Temporal");
            var directorio = new DirectoryInfo(ruta);
            try
            {

                foreach (FileInfo fi in directorio.GetFiles())
                {

                    fi.Delete();
                }

                foreach (DirectoryInfo di in directorio.GetDirectories())
                {

                    Directory.Delete(di.FullName.ToString(), true);
                }
                //if (directorio.Exists == true)
                //{
                //    Directory.Delete(directorio.ToString(), true);
                //}


            }
            catch (Exception)
            {

            }
        }
        //Action 
        //consulta si hay archivos en la carpeta temporal......///
        [HttpPost]
        public JsonResult FileConsult(string polizza)
        {
            int cont = 0;
            bool response = false;
            string fecha = DateTime.Now.ToString("dd-MM-yyyy");
            try
            {
                string source = Server.MapPath("/App_Data/Temporal/RosTemp_Digitalizacion/" + fecha + "/" + polizza);
                var datasrouce = new DirectoryInfo(source);


                foreach (var item in datasrouce.GetFiles())
                {
                    cont++;
                }
                if (cont != 0)
                {
                    response = true;

                }
                else
                {
                    response = false;
                }
            }
            catch (Exception ex)
            {
                response = false;
            }
            finally
            {

            }
            return Json(response);


        }
        [HttpPost]
        public JsonResult uploadfile()
        {
            string[] respuesta = new string[2];
            /// Primero traemos el documentos adjunto y la poliza desde el front_____!
            string listFilesToDelete = "";


            HttpFileCollectionBase files = Request.Files;
            string paliza = Request.Form["Id"].ToString();
            //HttpPostedFileBase onfile = files["data"];
            int Num = Convert.ToInt32(Request.Form["datalengh"]);
            Localpoliza = paliza;


            List<HttpPostedFileBase> onfile2 = new List<HttpPostedFileBase>();

            for (int i = 0; i < Num; i++)
            {
                onfile2.Add(files["data" + i]);

            }


            /// Tomamos la fecha sin la hora_____!
            string Time = DateTime.Now.ToString("dd-MM-yyyy");
            string ruta = @"//172.16.32.35/Digitalizacion/" + Localpoliza + "/" + Time;
            //string ruta = Server.MapPath("/Temporal/" +  Localpoliza + "/" + Time);
            try
            {

                //verificamos si llegaron los archivos o no 
                if (Request.Files.Count > 0)
                {

                    if (files.Count > 0)
                    {

                        // String para el directorio donde se carara el archivo temporal...
                        foreach (var item in onfile2)
                        {
                            listFilesToDelete += item.FileName + ",";

                            var NombreArchivo = Path.GetFileName(item.FileName);
                            string filename = "";
                            var DirDestino = new DirectoryInfo(ruta);
                            var Archivo = new FileInfo(Path.Combine(DirDestino.FullName, NombreArchivo.ToString()));


                            //int sizeKB = Convert.ToInt32(sizebyte) / 1024;
                            //string size = sizeKB.ToString();
                            // long filesize = new System.IO.FileInfo(NombreArchivo).Length;

                            // Verificamos si el directorio existe para grabar el archivo...
                            if (DirDestino.Exists == false)
                            {
                                DirDestino.Create();
                            }

                            filename = string.Concat(Archivo);
                            FileInfo nuevo = new FileInfo(filename);
                            item.SaveAs(nuevo.ToString());




                            ViewBag.mensaje = "Documento  cargado exitosamente...";
                        }


                        //Envio de los archivos e informacion al frontend_________//

                        //FileInfo fi = new FileInfo(Archivo.ToString());
                        //long size = fi.Length;
                        //int size2 = Convert.ToInt32(size);
                        //int filesize = size2 / 1024; //---------
                        //string extension = Archivo.Extension.ToString();



                        respuesta[0] = "1";
                        //respuesta[1] = Time;
                        //respuesta[2] = paliza;
                        //respuesta[3] = Path.GetFileName(onfile.FileName);
                        //respuesta[4] = filesize + " KB";
                        //respuesta[5] = extension;


                    }
                    else
                    {
                        respuesta[0] = "0";
                    }
                }
                else
                {
                    respuesta[0] = "?";
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                respuesta[0] = "error";
            }

            respuesta[1] = listFilesToDelete;
            return Json(respuesta);

        }
        [HttpPost]
        public JsonResult ConsultaPoliza(string Referencia)
        {
            string Time = DateTime.Now.ToString("dd-MM-yyyy");
            string rutaTemp = GetPathServer(Referencia);
            //     @"//172.16.32.35/Digitalizacion/"+ poliza;
            string ruta = Server.MapPath("/Temporal");
            string[] Data = new string[5];
            Data[0] = "1";

            try
            {

                if (rutaTemp != "")
                {

                    var didestination = new DirectoryInfo(rutaTemp);



                    if (didestination.Exists == true)
                    {
                        string[] subdirectoryEntries = Directory.GetFiles(rutaTemp, "*.*", SearchOption.AllDirectories);
                        foreach (string subdirectory in subdirectoryEntries)
                        {
                            FileInfo info = new FileInfo(subdirectory);
                            //var rutatempfinal = Path.Combine(ruta, info.Name);
                            //FileInfo filetemppath = new FileInfo(rutatempfinal);
                            //if (filetemppath.Exists == false)
                            //{
                            //    System.IO.File.Copy(info.FullName, Path.Combine(ruta, info.Name));
                            //    // Do something with the Folder or just add them to a list via nameoflist.add();


                            //}
                            Data[4] += info.FullName + "*" + info.Name + "|";
                            Data[4].TrimEnd();
                        }
                    }
                }


               // string BatchString = "DG|" + "Ref." + Referencia;
                //Data[1] = BatchString;

                SqlConnection con = referencia.roscon();
                string stringcomando = " Select Poliza, [Nombre CLiente], PersonaNumero, Aseguradora From DB_Smartsys3.dbo.Vw_ConsultaPoliza "
                                      + " Where Upper(Poliza) = '" + Referencia + "' Group By Poliza, [Nombre CLiente], PersonaNumero, Aseguradora";


                con.Open();
                SqlCommand commando = new SqlCommand(stringcomando, con);
                SqlDataReader leer = commando.ExecuteReader();
                if (leer.Read())
                {


                    //Data[0] = leer[0].ToString();
                    //Data[1] = leer[1].ToString(); 
                    Data[0] = leer[2].ToString();
                    Data[2] = leer[1].ToString();
                    Data[1] = "DG | " + "Po." + Referencia.ToUpper() + "_" + "Id." + Data[0];                     
                    Data[3] = leer[3].ToString();
                }
            }
            catch (Exception ex)
            {
                Data[0] = "0";
                Data[1] = "";
            }
            finally
            {
                
            }

            Localpoliza = Referencia;

            return Json(Data.ToArray());
        }
        //Busca todos los arvhivos en temporal____________________________________//
        [HttpPost]
        public JsonResult erasefile(string filename, string feccha, string polizza)
        {
            string response = "";


            string ruta = Server.MapPath("/App_Data/Temporal/RosTemp_Digitalizacion/" + feccha + "/" + polizza + "/" + filename);
            var directorio = new DirectoryInfo(ruta);
            try
            {
                System.IO.File.Delete(directorio.ToString());
                response = "correct";
            }
            catch (Exception)
            {
                response = null;
            }

            return Json(response);

        }
        ///funcion para registrar archivos 
        [HttpPost]
        public JsonResult registerfiles(string polizza, string batchh, string listFileDelete)
        {
            DeleteFiles(listFileDelete);
            string Time = DateTime.Now.ToString("dd-MM-yyyy");
            string response = "";
            string source = @"//172.16.32.35/Digitalizacion/" + polizza + "/" + Time;
            //string destination = ("//172.16.32.35/Salud Internacional/DigTemp/" + feccha + "/" + polizza);

            AdoConnections.conneciones varcon = new AdoConnections.conneciones();

            SqlConnection con = varcon.con();
            //FileSystemAccessRule accRule = new FileSystemAccessRule(username, FileSystemRights.FullControl, AccessControlType.Allow);



            try
            {

                //var disourse = new DirectoryInfo(source);
                //var didestination = new DirectoryInfo(destination);

                //if (didestination.Exists == false)
                //{
                //    didestination.Create();
                //}
                //foreach (FileInfo fi in source.GetFiles())
                //{
                //    fi.CopyTo(Path.Combine(didestination.FullName, fi.Name), true);
                //}

                con.Open();
                SqlCommand cmd = new SqlCommand("Insert Into TBL_DocumentosBatch(Cod_Referencia,Batch,PathDoc,Fuente,Fecha_Registro,Sn_Indexado,Usuario_Registro)Values(@CodReferencia,@Batch,@PathDoc,@Fuente,getdate(),'N',@UsuarioReg)", con);
                cmd.Parameters.AddWithValue("@CodReferencia", polizza.ToUpper());
                cmd.Parameters.AddWithValue("@Batch", batchh);
                cmd.Parameters.AddWithValue("@PathDoc", source);
                cmd.Parameters.AddWithValue("@Fuente", "Disco");
                cmd.Parameters.AddWithValue("@UsuarioReg", Session["IdUsuario"].ToString());
                cmd.ExecuteNonQuery();
                response = "exito";
                //limpiar directorio temporal
                // System.IO.Directory.Delete(source);
            }
            catch (Exception ex)
            {
                response = "error";
            }
            con.Close();
            return Json(response);
        }
        // funcion para limpiar registro de de documentos al finalizar la indexacio.......///
        [HttpPost]
        public JsonResult finalindex(string poliza)
        {
            string response = "";
            string Time = DateTime.Now.ToString("dd-MM-yyyy");
            string ruta = ("//172.16.32.35/Digitalizacion/" + Time + "/" + poliza);
            var inforuta = new DirectoryInfo(ruta);

            try
            {
                if (inforuta.Exists == true)
                {
                    System.IO.Directory.Delete(inforuta.ToString(), true);
                    response = "correct";
                    RedirectToAction("Index", "Home");
                }
                else
                {
                    response = "404";
                }

            }
            catch (Exception ex)
            {
                response = null;
            }

            return Json(response);
        }
        // funcinon para revisar si existe un documento registrado......// 
        [HttpPost]
        public JsonResult Indexa(string poliza, string cliente, string aseguradora, string titutar, string Dependientes, string Batch, string TipoDoc, string Parentesco, string Noreclamo, string pathfile, string pathfileserver)
        {
            string respuesta;
            AdoConnections.conneciones conn = new AdoConnections.conneciones();
            SqlConnection con = conn.con();
            try
            {
                FileInfo fileInfo = new FileInfo(pathfileserver);

                string verificaIndexado = "NO";
                string query = "select * from TBL_BatchImagenes where Batch = '" + Batch + "' and PathDoc = '" + fileInfo + "'";

                con.Open();
                SqlCommand commando = new SqlCommand(query, con);
                SqlDataReader leer = commando.ExecuteReader();
                if (leer.Read())
                {
                    verificaIndexado =
                            "SI";
                }
                con.Close();

                string comando = "Insert Into TBL_BatchIndexado(Poliza,Cliente,Aseguradora,Titular,Dependientes,Batch,Tipo_Doc,Usuario_Registro,Parentesco,Nro_Documento)Values(upper(@Poliza),@Cliente,@Aseguradora,@Titular,@Dependientes,@Batch,@TipoDoc,@UsuarioReg,@Parentesco,@Nro_Documento)";
                string comando2 = "Insert TBL_BatchImagenes(Batch,ID_Index, TipoDoc, PathDoc) Values(@Batch,(select max(ID_Index) from TBL_BatchIndexado), @TipoDoc, @PathDoc)";

                string Time = DateTime.Now.ToString("dd-MM-yyyy");



                if (verificaIndexado != "SI")
                {




                    con.Open();
                    SqlCommand comm = new SqlCommand(comando, con);
                    if (poliza != null)
                    {
                        comm.Parameters.AddWithValue("@Poliza", poliza.ToUpper());
                    }
                    else
                    {
                        comm.Parameters.AddWithValue("@Poliza", "");
                    }
                    if (cliente != null)
                    {
                        comm.Parameters.AddWithValue("@Cliente", cliente);
                    }
                    else
                    {
                        comm.Parameters.AddWithValue("@Cliente", "");
                    }
                    if (aseguradora != null)
                    {
                        comm.Parameters.AddWithValue("@Aseguradora", aseguradora);
                    }
                    else
                    {
                        comm.Parameters.AddWithValue("@Aseguradora", "");
                    }
                    comm.Parameters.AddWithValue("@Titular", cliente);
                    if (Dependientes != null)
                    {
                        comm.Parameters.AddWithValue("@Dependientes", Dependientes);
                    }
                    else
                    {
                        comm.Parameters.AddWithValue("@Dependientes", "");
                    }
                    if (Batch != null)
                    {
                        comm.Parameters.AddWithValue("@Batch", Batch);
                    }
                    else
                    {
                        comm.Parameters.AddWithValue("@Batch", "");
                    }
                    comm.Parameters.AddWithValue("@TipoDoc", TipoDoc);
                    comm.Parameters.AddWithValue("@UsuarioReg", Session["IdUsuario"].ToString());
                    if (Parentesco != null)
                    {
                        comm.Parameters.AddWithValue("@Parentesco", Parentesco);
                    }
                    else
                    {
                        comm.Parameters.AddWithValue("@Parentesco", "");
                    }
                    comm.Parameters.AddWithValue("@Nro_Documento", Noreclamo);
                    comm.ExecuteNonQuery();
                    con.Close();

                    con.Open();
                    SqlCommand comm2 = new SqlCommand(comando2, con);
                    comm2.Parameters.AddWithValue("@Batch", Batch);
                    comm2.Parameters.AddWithValue("@TipoDoc", TipoDoc);
                    comm2.Parameters.AddWithValue("@PathDoc", pathfileserver);
                    comm2.ExecuteNonQuery();
                    // newi--;
                    //}
                    con.Close();

                    /// actualiza el estado del documento indexado_________
                    //con.Open();
                    //SqlCommand comm3 = new SqlCommand(comando3, con);
                    //comm3.Parameters.AddWithValue("@Path", pathfile);
                    //comm3.ExecuteNonQuery();
                    //con.Close();
                    respuesta = "true";

                }
                else
                {
                    respuesta = "ArchivoIndexado";
                }



            }
            catch (Exception ex)
            {
                respuesta = "false";
            }
            finally
            {

            }
            // }


            return Json(respuesta);
        }

        public ActionResult IndexConsulta()
        {
            List<BatchIndexado> batchIndexado = new List<BatchIndexado>();

            // var model = db.BatchIndexado.OrderBy(p => p.FecRegistro).ToList();
            //SqlConnection con = referencia.con();

            string stringcomando = " Select *,Batch + '_' + Tipo_Doc + '_' + Nro_Documento as Identificador From TBL_BatchIndexado ";


            try
            {
                using (SqlConnection con = referencia.con())
                {
                    SqlCommand cmd = new SqlCommand(stringcomando, con);

                    con.Open();
                    SqlDataReader odr = cmd.ExecuteReader();

                    if (odr.HasRows)
                    {
                        while (odr.Read())
                        {
                            var fechareg = odr["Fecha_Registro"].ToString();
                            batchIndexado.Add(
                                new BatchIndexado
                                {
                                    IdIndex = Convert.ToInt32(odr["ID_Index"]),
                                    Poliza = odr["Poliza"].ToString(),
                                    Titular = odr["Titular"].ToString(),
                                    Dependientes = odr["Dependientes"].ToString(),
                                    TipoDoc = odr["Tipo_Doc"].ToString(),
                                    FecRegistro = fechareg,
                                    NroDocumento = odr["Nro_Documento"].ToString(),
                                    Identificador = odr["Identificador"].ToString()
                                });
                        }

                        //batchIndexado[0].Titular = leer["Titular"].ToString();
                        //batchIndexado[0].Dependientes = leer["Dependientes"].ToString();
                        //batchIndexado[0].TipoDoc = leer["Tipo_Doc"].ToString();
                        //batchIndexado[0].FecRegistro = DateTime.Parse(leer["Fecha_Registro"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
            }
            return View(batchIndexado);
        }

        [HttpGet]
        public JsonResult Details(int id)
        {
            List<VMDatosDocumentoIndexado> details = new List<VMDatosDocumentoIndexado>();

            // var model = db.BatchIndexado.OrderBy(p => p.FecRegistro).ToList();

            string stringcomando = " select * from TBL_BatchIndexado BI "
                + " join TBL_BatchImagenes BIM on BIM.ID_Index = BI.ID_Index"
                                  + "  where BI.ID_Index = " + id + "";
            try
            {
                using (SqlConnection con = referencia.con())
                {
                    SqlCommand cmd = new SqlCommand(stringcomando, con);

                    con.Open();
                    SqlDataReader odr = cmd.ExecuteReader();

                    if (odr.HasRows)
                    {
                        while (odr.Read())
                        {
                            details.Add(
                                new VMDatosDocumentoIndexado
                                {
                                    IdImagen = Convert.ToInt32(odr["IdImagen"]),
                                    Batch = odr["Batch"].ToString(),
                                    Poliza = odr["Poliza"].ToString(),
                                    Cliente = odr["Cliente"].ToString(),
                                    Dependientes = odr["Dependientes"].ToString(),
                                    Parentesco = odr["Parentesco"].ToString(),
                                    TipoDoc = odr["Tipo_Doc"].ToString(),
                                    Aseguradora = odr["Aseguradora"].ToString(),
                                    FecRegistro = odr["Fecha_Registro"].ToString(),
                                    Titular = odr["Titular"].ToString(),
                                    PathDoc = odr["PathDoc"].ToString(),
                                    NroDocumento = odr["Nro_Documento"].ToString(),
                                    UsuarioReg = odr["Usuario_Registro"].ToString(),
                                });
                        }
                    }
                }
            }

            catch (Exception ex)
            {

            }
            finally
            {

            }

            return Json(details, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public FileStreamResult GetPDF(int id)
        {
            string ruta = "";

            SqlConnection con = referencia.roscon();
            string stringcomando = " select PathDoc from [dbo].[TBL_BatchImagenes] "
                                  + " Where IdImagen = " + id + " ";


            con.Open();
            SqlCommand commando = new SqlCommand(stringcomando, con);
            SqlDataReader leer = commando.ExecuteReader();
            if (leer.Read())
            {
                ruta = leer["PathDoc"].ToString();

            }


            //var doc = db.SP_GetDataBatchImagenes(id).Single();
            //ruta = doc;

            FileStream fs = new FileStream(path: ruta, FileMode.Open, FileAccess.Read);
            if (fs != null)
            {
                return File(fs, "application/pdf");
            }
            else
            {
                return ViewBag.Error = "No hay ningun documento En la Ruta";
            }

        }

        public string getFileImagen(string id)
        {
            string ruta = "";

            return ruta;
        }
        #endregion
        #region New Vistas Digitalizacion

        public ActionResult IndexDig()
        {

            return View();
        }
        public ActionResult IndexDigit()
        {

            return View();
        }

        public ActionResult Scan()
        {

            return View();
        }
        public ActionResult CreateForm()
        {

            return View();
        }

        [HttpPost]
        public JsonResult CreatePlantilla(string NombreList, string NombrePlantilla)
        {
            var query1 = "";
            var query2 = "";
            var TipoDatoConvert = "";
            var ListNombres = NombreList.Split('-');
            int plantilla = 0;


            try
            {
                AdoConnections.conneciones varcon = new AdoConnections.conneciones();
                SqlConnection con1 = varcon.con();

                query1 = "Insert Into DB_SmartSys3.dbo.TBL_Plantillas(Nombre_Plantilla)";
                query1 += "values('" + NombrePlantilla + "')";

                con1.Open();
                SqlCommand cmd1 = new SqlCommand(query1, con1);
                cmd1.ExecuteNonQuery();
                con1.Close();

                SqlConnection con2 = varcon.con();
                string stringcomando = " Select ID_Plantilla from DB_SmartSys3.dbo.TBL_Plantillas "
                                      + " Where Nombre_Plantilla = '" + NombrePlantilla + "'";


                con2.Open();
                SqlCommand commando = new SqlCommand(stringcomando, con2);
                SqlDataReader leer = commando.ExecuteReader();
                if (leer.Read())
                {
                    plantilla = Convert.ToInt32(leer["ID_Plantilla"]);
                }
                con2.Close();

                string data = NombreList.ToString();
                data = data.Remove(data.Length - 1);
                string[] list = Regex.Split(data, ",");

                string query = "CREATE TABLE " + NombrePlantilla + " ( ";

                foreach (var item in list)
                {

                    SqlConnection con3 = varcon.con();
                    query2 = "Insert Into DB_SmartSys3.dbo.TBL_Campos_Plantillas(ID_Plantilla,Nombre_Columna,Tipo_de_Dato)";
                    query2 += "values(" + plantilla + ",'" + item.Split('-')[0] + "','" + item.Split('-')[1] + "')";

                    con3.Open();
                    SqlCommand cmd3 = new SqlCommand(query2, con3);

                    cmd3.ExecuteNonQuery();
                    con3.Close();

                    switch (item.Split('-')[1])
                    {
                        case "Texto":
                            TipoDatoConvert = "varchar(150)";
                            break;
                        case "Texto Grande":
                            TipoDatoConvert = "varchar(max)";
                            break;
                        case "Fecha":
                            TipoDatoConvert = "datetime";
                            break;
                    }

                    query += " " + item.Split('-')[0] + " " + TipoDatoConvert + " NOT NULL,";
                }
                query = query.Remove(query.Length - 1);
                query += " ) ";

                SqlConnection con = referencia.con();
                SqlCommand cmd = new SqlCommand(query, con);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (SqlException e)
            {

            }
           

            return Json("");
        }

        public ActionResult Indexa()
        {
            ViewBag.Plantilla = new SelectList(db.TBL_Plantillas, "ID_Plantilla", "Nombre_Plantilla");

            return View();
        }


        public JsonResult GetPlantillaData(int IdPlantilla)
        {
            var data = (from pl in db.TBL_Plantillas
                        join cpl in db.TBL_Campos_Plantillas on pl.ID_Plantilla equals cpl.ID_Plantilla
                        where pl.ID_Plantilla == IdPlantilla
                        select new
                        {
                            cpl.Nombre_Columna,
                            cpl.Tipo_de_Dato
                        }).ToList();

            return Json(data);
        }

        [HttpPost]
        public JsonResult ConsultaPolizaIndexado(string poliza)
        {
            string Time = DateTime.Now.ToString("dd-MM-yyyy");
            string rutaTemp = GetPathServer(poliza);
            // @"//172.16.32.35/Digitalizacion/" + poliza;
            string ruta = Server.MapPath("/Temporal");
            string[] Data = new string[7];
            int numFile = 1;
            string rutaconfirmindex = "NO";

            SqlConnection con = referencia.roscon();
            string stringcomando = " Select Poliza, [Nombre CLiente], PersonaNumero, Aseguradora From GestionSeguros.dbo.Vw_ConsultaPoliza "
                                  + " Where Upper(Poliza) = '" + poliza + "' Group By Poliza, [Nombre CLiente], PersonaNumero, Aseguradora";


            try
            {
                //var rutaTemCompleta = Path.Combine(ruta, poliza + "-" + numFile);

                if (rutaTemp != "")
                {
                    string[] subdirectoryEntries = Directory.GetFiles(rutaTemp, "*.*", SearchOption.AllDirectories);
                    foreach (string subdirectory in subdirectoryEntries)
                    {
                        FileInfo info = new FileInfo(subdirectory);
                        //var rutatempfinal = Path.Combine(ruta, info.Name);
                        //FileInfo filetemppath = new FileInfo(rutatempfinal);
                        //if (filetemppath.Exists == false)
                        //{
                        //    System.IO.File.Copy(info.FullName, Path.Combine(ruta, info.Name));
                        //    // Do something with the Folder or just add them to a list via nameoflist.add();


                        //}

                        rutaconfirmindex = verificaindexados(poliza, info.FullName);


                        Data[5] += info.FullName + "*" + info.Name + "|";
                        Data[5].TrimEnd();
                        Data[6] += info.FullName + "#" + rutaconfirmindex + "|";
                        Data[6].TrimEnd();
                        //con2.Close();
                    }





                    con.Open();
                    SqlCommand commando = new SqlCommand(stringcomando, con);
                    SqlDataReader leer = commando.ExecuteReader();
                    if (leer.Read())
                    {

                        Data[0] = leer[2].ToString();
                        Data[2] = leer[1].ToString();
                        string BatchString = "DG | " + "Po." + poliza + "_" + "Id." + Data[0];
                        Data[1] = BatchString;
                        Data[3] = leer[3].ToString();

                    }
                    //con2.Open();
                    //SqlCommand commando2 = new SqlCommand(stringcomando2, con2);
                    //SqlDataReader leer2 = commando2.ExecuteReader();
                    //if (leer2.Read())
                    //{
                    //    while (leer2.HasRows)
                    //    {
                    //        Data[4] = leer2[0].ToString();
                    //        var NombreNuevo = poliza;
                    //        string sourceFile = System.IO.Path.Combine(Data[4], NombreNuevo);
                    //        string destFile = System.IO.Path.Combine(rutaTemp, NombreNuevo);

                    //        DirectoryInfo d = new DirectoryInfo(Data[4]);//Assuming Test is your Folder
                    //        FileInfo[] Files = d.GetFiles(); //Getting Text files
                    //        string str = "";
                    //        foreach (FileInfo file in Files)
                    //        {
                    //            System.IO.File.Copy(file.FullName, Path.Combine(rutaTemp, file.Name));
                    //            var fi = file;
                    //        }

                    //        Data[5] += destFile + "|";
                    //    }

                    //}


                }
                else
                {

                    Data[0] = "1";
                }
            }
            catch (Exception ex)
            {
                Data[0] = null;
                Data[1] = "";
            }
            finally
            {
                con.Close();

            }

            Localpoliza = poliza;

            return Json(Data.ToArray());
        }

        private string verificaindexados(string pol, string bat)
        {
            string retorna = "NO";

            SqlConnection con = referencia.con();
            string stringcomando2 = " select idimagen from [dbo].[TBL_BatchImagenes] "
                                  + " Where Batch = (select top 1 batch from [dbo].[TBL_DocumentosBatch] where Cod_Referencia = '" + pol + "') and PathDoc = '" + bat + "' ";

            con.Open();
            SqlCommand commando2 = new SqlCommand(stringcomando2, con);
            SqlDataReader leer2 = commando2.ExecuteReader();
            if (leer2.HasRows)
            {
                while (leer2.Read())
                {
                    retorna = "SI";
                }
            }

            return retorna;
        }

        public string GetPathServer(string Referencia)
        {
            string retorna = "";

            try
            {


                SqlConnection con = referencia.con();
                string stringcomando2 = " select PathDoc from [dbo].[TBL_DocumentosBatch] "
                                      + " Where Cod_Referencia = '" + Referencia + "' ";

                con.Open();
                SqlCommand commando2 = new SqlCommand(stringcomando2, con);
                SqlDataReader leer2 = commando2.ExecuteReader();
                if (leer2.HasRows)
                {
                    while (leer2.Read())
                    {

                        retorna = leer2["PathDoc"].ToString();
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return retorna;

        }

        [HttpPost]
        public JsonResult GetTemporalPath(string PathFile)
        {
            string ruta = Server.MapPath("/Temporal");
            string rutatemp = "";

            if (PathFile != "")
            {
                //string[] subdirectoryEntries = Directory.GetFiles(PathFile, "*.*", SearchOption.AllDirectories);
                //foreach (string subdirectory in subdirectoryEntries)
                //{
                //    FileInfo info = new FileInfo(subdirectory);
                FileInfo filetemppath = new FileInfo(PathFile);
                var rutatempfinal = Path.Combine(ruta, filetemppath.Name);
                FileInfo finaltemp = new FileInfo(rutatempfinal);
                if (finaltemp.Exists == false)
                {
                    System.IO.File.Copy(filetemppath.FullName, rutatempfinal);
                    // Do something with the Folder or just add them to a list via nameoflist.add();

                }
                rutatemp = rutatempfinal.Replace(@"\", "/");

                //}
            }
            return Json(rutatemp, JsonRequestBehavior.AllowGet);

        }

        public void DeleteFiles(string listfiles)
        {
            listfiles = listfiles.TrimEnd();
            var list = listfiles.Split(',');

            List<string> carpetas = new List<string>();
            carpetas.Add(@"\\172.16.32.35\Digitalizacion\Smartsys1");
            carpetas.Add(@"\\172.16.32.35\Digitalizacion\Smartsys2");
            carpetas.Add(@"\\172.16.32.35\Digitalizacion\Smartsys3");
            carpetas.Add(@"\\172.16.32.35\Digitalizacion\Smartsys4");
            carpetas.Add(@"\\172.16.32.35\Digitalizacion\Smartsys5");
            carpetas.Add(@"\\172.16.32.35\Digitalizacion\Smartsys6");
            carpetas.Add(@"\\172.16.32.35\Digitalizacion\Smartsys7");
            carpetas.Add(@"\\172.16.32.35\Digitalizacion\Smartsys8");
            carpetas.Add(@"\\172.16.32.35\Digitalizacion\Smartsys9");
            carpetas.Add(@"\\172.16.32.35\Digitalizacion\Smartsys10");

            for (int i = 0; i < list.Length; i++)
            {

                foreach (var rutaCarpeta in carpetas)
                {

                    //var rutainfo = new FileInfo(Path.Combine(rutaCarpeta, item.FileName));
                    //if (rutainfo.Exists == true) 
                    //{
                    //    File.Delete(rutaCarpeta);
                    //}

                    if (System.IO.File.Exists(Path.Combine(rutaCarpeta, list[i])))
                    {

                        // If file found, delete it    
                        System.IO.File.Delete(Path.Combine(rutaCarpeta, list[i]));

                    }

                }
            }

        }


        #endregion


    }


}