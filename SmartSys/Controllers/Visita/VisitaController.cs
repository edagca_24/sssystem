﻿using SmartSys.Models;
using SmartSys.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using OfficeOpenXml;
using System.IO;
using ApiMailsender;
using SmartSys.Models.Clases;

namespace SmartSys.Controllers.Visita
{
    public class VisitaController : Controller
    {

        DB_Smartsys3Entities db = new DB_Smartsys3Entities();
        SendEmailAndAppoiment appoiment = new SendEmailAndAppoiment();
        public ActionResult Index()
        {
            ViewBag.Motivo = new SelectList(db.TBL_Motivo_Visita, "ID_Motivo", "Motivo");

            return View();
        }



        public ActionResult CreateVisita()
        {

            ViewBag.Motivo = new SelectList(db.TBL_Motivo_Visita, "ID_Motivo", "Motivo");
            ViewBag.EstadoVisita = new SelectList(db.TBL_Estado_Visita, "ID_Estado_Visita", "Estado_Visita");

            if (db.TBL_Visita.Any(c => c.ID_Visita > 0))
            {
                ViewBag.VisitaNum = db.TBL_Visita.Max(c => c.ID_Visita) + 1;
            }
            else
            {
                ViewBag.VisitaNum = 1;
            }

            return View();
        }


        [HttpPost]
        public ActionResult CreateVisita(string Motivo,
        string Contacto,
        string Lugar,
        string Fecha,
        string HoraInicio,
        string HoraFin,
        string Asunto,
        string Comentario,
        int Responsable,
        string Integrantes,
        int Cliente)
        {
            var tipo = "";
            List<string> ListEmail = new List<string>();

            ViewBag.Motivo = new SelectList(db.TBL_Motivo_Visita, "ID_Motivo", "Motivo", Motivo);
            try
            {
                db.SP_SAVE_VISITA(
                                Contacto, Convert.ToInt32(Motivo), 1, Convert.ToInt32(Session["IdUsuario"]), Lugar, DateTime.Parse(Fecha)
                                , DateTime.Now, Asunto, Comentario, HoraInicio, HoraFin, Responsable,Cliente
                                );
                if (Integrantes != null)
                {
                    var IDVisita = db.TBL_Visita.Max(e => e.ID_Visita).ToString();
                    var visitaid = Convert.ToInt32(IDVisita);                  
                    string data = Integrantes.ToString();
                    data = data.Remove(data.Length - 1);
                    string[] integrantesguardar = Regex.Split(data, ",");
                    foreach (var item in integrantesguardar)
                    {
                       
                        string nombre = item.Split('|')[0];
                        string codigoid = item.Split('|')[1];
                        db.SP_SaveItegrantes(Convert.ToInt32(IDVisita), nombre,codigoid, 1,1);

                        if (codigoid.Split('-')[0] == "ENT")
                        {
                            tipo = "Cliente";
                        }
                        else
                        {
                            tipo = "Usuario";
                        }
                        var query = db.SP_GetPersonaEmail(codigoid.Split('-')[1], tipo).ToList();
                        foreach (var i in query)
                        {
                            ListEmail.Add(i);
                        }
                        
                    }
                }
                var Mot = Convert.ToInt32(Motivo);
                var queryMotivo = db.TBL_Motivo_Visita.Where(e => e.ID_Motivo == Mot).ToList();
                var queryResponsableEmail = db.TBL_Usuario.Where(e => e.ID_Usuario == Responsable).Select(e => e.Email).ToList();
                ListEmail.Add(queryResponsableEmail[0]);
               

                var fechaDesde = Fecha + " " + HoraInicio;
                var fechaHasta = Fecha + " " + HoraFin;

                appoiment.AppoimentCreator("Visita convocatoria", queryMotivo[0].Motivo, queryResponsableEmail[0], ListEmail, DateTime.Parse(fechaDesde), DateTime.Parse(fechaHasta), Lugar);
                //newAPISoap newAPISoap = new newAPISoap();
                //    newAPISoap.AppoimentCreator("Visita convocatoria","edgar.aguasvivas@ros.com.do","Made-0910",queryMotivo[0].Motivo,ListEmail, DateTime.Parse(Fecha), DateTime.Parse(Fecha),"");

            }
            catch (Exception e)
            {
                return Json("0", JsonRequestBehavior.AllowGet);
                throw;
            }
            

            return Json("1",JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetData(string searchTerm, string tipo)
        {
            tipo = tipo.ToString();
            var data = db.SP_GetPersona(searchTerm, tipo).ToList();
            var modifiedData = data.Select(x => new
            {
                id = x.ID,
                text = x.NombreCompleto
            });

            return Json(modifiedData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AgendarVisita(TBL_Integrantes_Visitas integrantes)
        {



            return View("Index");

        }

        [HttpGet]
        public ActionResult GetEvent(TBL_Visita visita)
        {

            var eventos = db.SP_GetDataVisitasEvent(Convert.ToInt32(Session["IdUsuario"])).Select(e => new
            {

                title = e.Titulo,
                start = e.FechaInicio,
                end = e.FechaFin,
                backgroundColor = e.Color

            });

            var row = eventos.ToArray();

            return Json(row, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDetailVisita(int? id)
        {


            if (id != null)
            {
                var retorna = db.TBL_Visita.Where(e => e.ID_Visita == id).Select(e => new
                {
                    IdVisita = e.ID_Visita,
                    IdMotivo = e.ID_Motivo_Visita,
                    Motivo = e.TBL_Motivo_Visita.Motivo,
                    Contacto = e.Contacto,
                    Estado = e.TBL_Estado_Visita.Estado_Visita,
                    UsuarioCreador = e.TBL_Usuario.Nombre,
                    Direccion = e.Direccion,
                    Fecha_Inicio = SqlFunctions.DatePart("MM", e.Fecha_Inicio_Visita) + "/" + SqlFunctions.DateName("DD", e.Fecha_Inicio_Visita) + "/" + SqlFunctions.DateName("YYYY", e.Fecha_Inicio_Visita),
                    HoraInicio = e.Hora_Inicio,
                    HoraFin = e.Hora_Fin,
                    Asunto = e.Asunto,
                    Comentario = e.Comentario,
                    Responsable = e.TBL_Usuario1.Nombre,
                    Cliente = e.TBL_Ente.Nombre,
                    IdCliente = e.ID_Ente

                }).ToList();



                return Json(retorna, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(HttpNotFound(), JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult getIntegrantes(int? id)
        {
       
            var exist = db.TBL_Integrantes_Visitas.Where(e => e.ID_Visita == id && e.Condicion_Registro == "0").FirstOrDefault();         

            
            if (id != null && exist != null)
            {
                var log = db.TBL_Integrantes_Visitas.Where(e => e.ID_Visita == id).Max(e => e.Log_No);
                var retorna = db.TBL_Integrantes_Visitas.Where(e => e.ID_Visita == id && e.Log_No == log).Select(e => new
                {
                    IdIntegrante = e.ID_Integrantes_Visitas,
                    Integrante = e.Nombre_Integrante                    
                }).ToList();

                return Json(retorna, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
           
        }

        [HttpPost]
        public ActionResult EditarVisita(int IdVisita,int Motivo, string Contacto,string Lugar,string Asunto,string Comentario,string Integrantes,int Cliente)
        {
        
            ViewBag.Motivo = new SelectList(db.TBL_Motivo_Visita, "ID_Motivo", "Motivo", IdVisita);
            //DateTime fechaInicio = DateTime.ParseExact(FechaInicio, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            //FechaInicio = fechaInicio.ToString("yyyy-MM-dd");

            //DateTime fechaFin = DateTime.ParseExact(FechaFin, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            //FechaFin = fechaFin.ToString("yyyy-MM-dd");
            try
            {
                db.SP_EditarVisita(IdVisita, Motivo, Contacto, Lugar, Asunto, Comentario,Cliente);

                var exist = (from c in db.TBL_Integrantes_Visitas
                             where c.ID_Visita == IdVisita                            
                             select new { c.Log_No }).ToList();


                if (Integrantes != null)
                {
                    if (exist.Count != 0)
                    {

                        var log = db.TBL_Integrantes_Visitas.Where(e => e.ID_Visita == IdVisita).Max(e => e.Log_No) + 1;
                        string data = Integrantes.ToString();
                        data = data.Remove(data.Length - 1);
                        string[] integrantesguardar = Regex.Split(data, ",");

                        foreach (var item in integrantesguardar)
                        {
                            string nombre = item.Split('|')[0];
                            string codigoid = item.Split('|')[1];
                            db.SP_SaveItegrantes(IdVisita, nombre, codigoid, Convert.ToInt32(log), 0);
                        }

                    }
                    else
                    {
                        string data = Integrantes.ToString();
                        data = data.Remove(data.Length - 1);
                        string[] integrantesguardar = Regex.Split(data, ",");

                        foreach (var item in integrantesguardar)
                        {
                            string nombre = item.Split('|')[0];
                            string codigoid = item.Split('|')[1];
                            db.SP_SaveItegrantes(IdVisita, nombre, codigoid, Convert.ToInt32(1), 0);
                        }
                    }
                }
                else
                {
                    if (exist.Count != 0)
                    {
                        var log = db.TBL_Integrantes_Visitas.Where(e => e.ID_Visita == IdVisita).Max(e => e.Log_No);
                        db.SP_DeleteItegrantes(IdVisita, log);
                    }
                }

                }
            catch (Exception e)
            {
                return Json('0',JsonRequestBehavior.AllowGet);
                throw;
            }
          
            return Json("1", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CompletarVisita(int IdVisita,string Comentario, string Hora,string Fecha,string Forma)
        {
            ViewBag.Motivo = new SelectList(db.TBL_Motivo_Visita, "ID_Motivo", "Motivo", IdVisita);

        

            try
            {
                db.SP_CompletarVisita(IdVisita, Hora, Convert.ToDateTime(Fecha), Comentario);
            }
            catch (Exception e)
            {
                return Json("0",JsonRequestBehavior.AllowGet);
                throw;
            }

            //if (Forma != "NuevaMinuta")
            //{

                var query = (from V in db.TBL_Visita
                          where V.ID_Visita == IdVisita
                          select new
                          {
                              V.ID_Ente,
                               V.TBL_Ente.Nombre,                         
                              Contacto = V.Contacto,
                              Motivo = V.TBL_Motivo_Visita.Motivo,
                              Direccion = V.Direccion,
                              Fecha = V.Fecha_Inicio_Visita
                          }).ToList();

            //ViewBag.ClienteId = id[0].ID_Ente;
            // ViewBag.ClienteNombre = id[0].Nombre;
            //}
            if (Forma != null) {
                Session["IdVisita"] = IdVisita;
            }
            
            return Json("1",JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult RedirectToMinuta() {
           return RedirectToRoute("CreateMinuta", "Minuta");
        }

        [HttpPost]
        public ActionResult PosponerVisita(int IdVisita,string HoraInicio, string HoraFin, string Fecha,string Comentario)
        {
            ViewBag.Motivo = new SelectList(db.TBL_Motivo_Visita, "ID_Motivo", "Motivo", IdVisita);
            List<string> ListEmail = new List<string>();
            var tipo = "";

            try
            {
            db.SP_PosponerVisita(Convert.ToInt32(IdVisita), HoraInicio, HoraFin, Convert.ToDateTime(Fecha), Comentario);
               

                /////////Enviando convocatoria de la reunion al outlook ////////////////////
                var integrantes = (from I in db.TBL_Integrantes_Visitas
                                   where I.ID_Visita == IdVisita
                                   select new
                                   {
                                       codigo = I.Codigo
                                   }).ToList();
                foreach (var item in integrantes)
                {
                    
                    string codigoid = item.codigo.Split('-')[0];
                    string id = item.codigo.Split('-')[1];
                    if (codigoid == "ENT")
                    {
                        tipo = "Cliente";
                    }
                    else
                    {
                        tipo = "Usuario";
                    }

                    var query = db.SP_GetPersonaEmail(id, tipo).ToList();
                    foreach (var i in query)
                    {
                        ListEmail.Add(i);
                    }

                }
                var datos = (from V in db.TBL_Visita
                             where V.ID_Visita == IdVisita
                             select new
                             {
                                 responsable = V.ID_Usuario_Responsable,
                                 motivo = V.TBL_Motivo_Visita.Motivo,
                                 V.Direccion
                             }).ToList();
                var responsable = datos[0].responsable;
                var queryResponsableEmail = db.TBL_Usuario.Where(e => e.ID_Usuario == responsable).Select(e => e.Email).ToList();
                ListEmail.Add(queryResponsableEmail[0]);

                var fechaDesde = Fecha + " " + HoraInicio;
                var fechaHasta = Fecha + " " + HoraFin;

                
                appoiment.AppoimentCreator("Visita Convocatoria Pospuesta",datos[0].motivo, queryResponsableEmail[0], ListEmail, DateTime.Parse(fechaDesde), DateTime.Parse(fechaHasta), datos[0].Direccion);
                /////////////////////////////////
                
            }
            catch (Exception e)
            {
                return Json("0", JsonRequestBehavior.AllowGet);
                throw;
            }

            return Json("1",JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult GenerarReporte()
        {
            string fileName = "";
            string ruta = Server.MapPath("/App_Data/Temporal/RosTemp_Reportes");           

            //Parametros del summit//
            var fechaDesde = Request.Form["fechaDesde"];
            var fechaHasta = Request.Form["fechaHasta"] == "" ? null : Request.Form["fechaHasta"];

            //Consulta a la base de datos//
            var data = db.SP_ReporteDeVisita(Convert.ToDateTime(fechaDesde), Convert.ToDateTime(fechaHasta), Convert.ToInt32(Session["IdUsuario"])).ToList();
            int contador = 2;

            //Creacion del excel//
            using (ExcelPackage excel = new ExcelPackage())
            {
                excel.Workbook.Worksheets.Add("Worksheet1");
                var excelWorksheet = excel.Workbook.Worksheets["Worksheet1"];

                //                foreach (var item in data)
                //                {

                //                    List<string[]> valuesRow = new List<string[]>()
                //                {
                //                    new string[] { ""+item.ID_Visita+"",""+item.ID_Contacto+"",""+item.ID_Motivo_Visita+"",""+item.ID_Estado_Visita+"",""+item.ID_Usuario+""
                //,""+item.Direccion+"",""+item.Motivo_Cambio+"",""+item.Fecha_Inicio_Visita+"",""+item.Fecha_Fin_Visita+"",""+item.Fecha_Realizada_Visita+""
                //,""+item.Fecha_Registro+"" ,""+item.Fecha_Modificacion+"",""+item.Asunto+"",""+item.Comentario+"",""+item.Comentario_Cierre+""
                //,""+item.HoraInicio+"",""+item.HoraFin+"",""+item.HoraRealizada+"",""+item.Comentario_Pospuesta+"" }
                //                };
                //                    excelWorksheet.Cells[headerRange].LoadFromArrays(valuesRow);
                //                }

                List<string[]> headerRow = new List<string[]>()
                {
                    new string[] { "ID_Visita","ID_Contacto","Motivo_Visita","Estado_Visita","Usuario"
                    ,"Direccion","Motivo_Cambio","Fecha_Inicio_Visita","Fecha_Fin_Visita","Fecha_Realizada_Visita"
                    ,"Fecha_Registro" ,"Fecha_Modificacion","Asunto","Comentario","Comentario_Cierre"
                    ,"HoraInicio","HoraFin","HoraRealizada","Comentario_Pospuesta" }
                };

                // Determine the header range (e.g. A1:E1)
                string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                // Popular header row data
                excelWorksheet.Cells[headerRange].LoadFromArrays(headerRow);

                for (int j = 0; j < data.Count; j++)
                {



                    excelWorksheet.Cells["A" + contador + ""].Value = data[j].ID_Visita;
                    excelWorksheet.Cells["B" + contador + ""].Value = data[j].Contacto;
                    excelWorksheet.Cells["C" + contador + ""].Value = data[j].Motivo_Visita;
                    excelWorksheet.Cells["D" + contador + ""].Value = data[j].Estado_Visita;
                    excelWorksheet.Cells["E" + contador + ""].Value = data[j].Usuario;
                    excelWorksheet.Cells["F" + contador + ""].Value = data[j].Direccion;
                    excelWorksheet.Cells["G" + contador + ""].Value = data[j].Motivo_Cambio;
                    excelWorksheet.Cells["H" + contador + ""].Value = data[j].Fecha_Fin_Visita;
                    excelWorksheet.Cells["I" + contador + ""].Value = data[j].Fecha_Fin_Visita;
                    excelWorksheet.Cells["J" + contador + ""].Value = data[j].Fecha_Realizada_Visita;
                    excelWorksheet.Cells["K" + contador + ""].Value = data[j].Fecha_Registro;
                    excelWorksheet.Cells["L" + contador + ""].Value = data[j].Fecha_Modificacion;
                    excelWorksheet.Cells["M" + contador + ""].Value = data[j].Asunto;
                    excelWorksheet.Cells["N" + contador + ""].Value = data[j].Comentario;
                    excelWorksheet.Cells["O" + contador + ""].Value = data[j].Comentario_Cierre;
                    excelWorksheet.Cells["P" + contador + ""].Value = data[j].Hora_Inicio;
                    excelWorksheet.Cells["Q" + contador + ""].Value = data[j].Hora_Fin;
                    excelWorksheet.Cells["R" + contador + ""].Value = data[j].Hora_Realizada;
                    excelWorksheet.Cells["S" + contador + ""].Value = data[j].Comentario_Pospuesta;


                    contador++;
                }

                var DirDestino = new DirectoryInfo(ruta);
                var file = new FileInfo(Path.Combine(DirDestino.FullName, "RosReporteVisita" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + ".xlsx"));
                if (DirDestino.Exists == false)
                {
                    DirDestino.Create();
                }
                fileName = String.Concat(file);


                FileInfo excelFile = new FileInfo(fileName);
                excel.SaveAs(excelFile);

                bool isExcelInstalled = Type.GetTypeFromProgID("Excel.Application") != null ? true : false;
                if (isExcelInstalled)
                {
                    System.Diagnostics.Process.Start(excelFile.ToString());
                }
            }


            return RedirectToAction("Index");

        }
        [HttpGet]
        public JsonResult GetEnte(string criterio)
        {
            if (criterio == "")
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            var data = db.SP_GetPersona(criterio, "Cliente").ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CreateIntegranteExterno(string NombreIntegrante)
        {
            if (NombreIntegrante == "")
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            var data = db.SP_Create_Integrante_Externo(NombreIntegrante,Convert.ToInt32(Session["IdUsuario"])).ToList();


            return Json(data, JsonRequestBehavior.AllowGet);
        }     
        
        [HttpPost]
        public JsonResult CreateMinutaTemporal(int? IdVisita,string Fecha)
        {
            var retorna = 0;
            var date = DateTime.Parse(Fecha);
            try
            {
                db.SP_SaveMinutaTemporal(IdVisita, date);
            }
            catch (Exception e)
            {
                retorna = 1;
                throw;
            }            

            return Json(retorna, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CargaMasivaVisita(List<VMVisitaData> datos)
        {
            var date = new DateTime();
            try
            {
                foreach (var item in datos)
                {
                    switch (item.Mes)
                    {
                        case "Enero":
                            date = new DateTime(DateTime.Now.Year, 1, 1);
                            break;
                        case "Febrero":
                            date = new DateTime(DateTime.Now.Year, 2, 1);
                            break;
                        case "Marzo":
                            date = new DateTime(DateTime.Now.Year, 3, 1);
                            break;
                        case "Abril":
                            date = new DateTime(DateTime.Now.Year, 4, 1);
                            break;
                        case "Mayo":
                            date = new DateTime(DateTime.Now.Year, 5, 1);
                            break;
                        case "Junio":
                            date = new DateTime(DateTime.Now.Year, 6, 1);
                            break;
                        case "Julio":
                            date = new DateTime(DateTime.Now.Year, 7, 1);
                            break;
                        case "Agosto":
                            date = new DateTime(DateTime.Now.Year, 8, 1);
                            break;
                        case "Septiembre":
                            date = new DateTime(DateTime.Now.Year, 9, 1);
                            break;
                        case "Octubre":
                            date = new DateTime(DateTime.Now.Year, 10, 1);
                            break;
                        case "Noviembre":
                            date = new DateTime(DateTime.Now.Year, 11, 1);
                            break;
                        case "Diciembre":
                            date = new DateTime(DateTime.Now.Year, 12, 1);
                            break;
                    }
                    db.SP_SAVE_VISITA(
                               item.Contacto, Convert.ToInt32(item.ID_Motivo), 1, Convert.ToInt32(Session["IdUsuario"]), item.Direccion, date
                               , DateTime.Now, item.Asunto, "", "09:00", "09:00", item.ID_Responsable, item.IDEntidad
                               );
                }
            }
            catch (Exception e)
            {
                return Json("0", JsonRequestBehavior.AllowGet);
                throw;
            }            

            return Json("1", JsonRequestBehavior.AllowGet);
        }
    }
}