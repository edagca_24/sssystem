﻿using SmartSys.Models;
using SmartSys.Models.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SmartSys.Controllers
{
    [HandleError]
    public class HomeController : Controller
    {
        DB_Smartsys3Entities db = new DB_Smartsys3Entities();
        SendEmailAndAppoiment appoiment = new SendEmailAndAppoiment();

        public ActionResult Index()
        { 
                return View();
        }

        public ActionResult RecuperarContraseña()
        {
         

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        //public ActionResult Login()
        //{
        //        return View();
           
        //}
        //[HttpPost]
        //public ActionResult Login(TBL_Usuario usuario)
        //{
        //    var user = Request.Form["Usuario"];
        //    var contraseña = Request.Form["contraseña"];

        //        if (user != "" && contraseña != "")
        //        {
        //            var data = db.TBL_Usuario.Where(a => a.Usuario.Equals(user) && a.Clave.Equals(contraseña.ToString())).FirstOrDefault();
        //            if (data != null)
        //            {
        //                Session["IdUsuario"] = data.ID_Usuario.ToString();
        //                Session["Usuario"] = data.Usuario.ToString();
        //                Session["Nombre"] = data.Nombre.ToString();

        //                return RedirectToAction("Index", "Home");
        //            }
        //            else
        //            {
        //                return ViewBag.ErrorInicio = "Usuario o contraseña incorrecta";
        //            }
        //        }
        //          return View(usuario);
        //}
        [HttpPost]
        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Index", "Home");


        }

        [HttpPost] 
        public string ValidMail(string mail)
        {
            string retorna = "0";

            var listaCorreo = db.TBL_Usuario.Select(e => e.Email).ToList();

            foreach (var item in listaCorreo)
            {
                if (item == mail)
                {
                    retorna = "1";
                    sendTokenToMail(mail);
                }
            }
          

            return retorna;
        }

        public void sendTokenToMail(string mail)
        {
            byte[] token;
            StringBuilder body = new StringBuilder();

           token = GeneratePasswordResetToken();

            TBL_Usuario tbusers;
            tbusers = db.TBL_Usuario.Where(e => e.Email == mail).First();
            tbusers.Token = token;

            db.TBL_Usuario.Add(tbusers);
            db.Entry(tbusers).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();


            body.Append("<p>Buenas,</p><br>");
            body.Append("Su codigo token es " + token + " puede proceder a restablecer su contraseña<br>");
            body.Append("Saludos");

            appoiment.SendEmail(body.ToString(),"Solicitud de cambio de contraseña",mail);

        }

        public byte[] GeneratePasswordResetToken()
        {
            byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
            byte[] key = Guid.NewGuid().ToByteArray();
            string token = Convert.ToBase64String(time.Concat(key).ToArray());
            byte[] data = Convert.FromBase64String(token);
            return data;
        }
    }
}
