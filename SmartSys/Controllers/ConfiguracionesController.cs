﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using System.Data.SqlClient;
using SmartSys.Models.ViewModels;
using System.IO;

namespace SmartSys.Controllers
{
    public class ConfiguracionesController : Controller
    {
        // GET: Configuraciones
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CargaPoliza()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ImportPoliza()
        {
            string path = Server.MapPath("~/Uploads/");
            var retorna = "";
            string filePath = string.Empty;
            AdoConnections.conneciones varcon = new AdoConnections.conneciones();
            SqlConnection con = varcon.con();

            string[] filesDirectory = Directory.GetFiles(path);
            foreach (string file in filesDirectory)
            {
                System.IO.File.Delete(file);                
            }

            try
            {
                if (Request.Files.Count > 0)
                {
                    HttpFileCollectionBase files = Request.Files;

                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                        
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }

                        filePath = path + Path.GetFileName(file.FileName);
                        string extension = Path.GetExtension(file.FileName);
                        file.SaveAs(filePath);

                    }
                    //Create COM Objects. Create a COM object for everything that is referenced
                    Excel.Application xlApp = new Excel.Application();
                    Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(filePath); ;
                    Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
                    Excel.Range xlRange = xlWorksheet.UsedRange;

                    int rowCount = xlRange.Rows.Count;
                    int colCount = xlRange.Columns.Count;

                    con.Open();
                    //iterate over the rows and columns and print to the console as it appears in the file
                    //excel is not zero based!!
                    for (int i = 2; i <= rowCount; i++)
                    {
                        var poliza = xlRange.Cells[i, 1].Value2.ToString();
                        var cliente = xlRange.Cells[i, 2].Value2.ToString();
                        var aseguradora = xlRange.Cells[i, 3].Value2.ToString();
                        var personanum = xlRange.Cells[i, 4].Value2.ToString();

                        if (PolizaExist(poliza) == false)
                        {

                        SqlCommand cmd = new SqlCommand("Insert Into DB_Smartsys3.dbo.TBL_Poliza(Poliza,Nombre_Cliente,Aseguradora,Persona_Numero)Values(@Poliza,@Cliente,@Aseguradora,@PersonaNum)", con);
                        cmd.Parameters.AddWithValue("@Poliza", poliza);
                        cmd.Parameters.AddWithValue("@Cliente", cliente);
                        cmd.Parameters.AddWithValue("@Aseguradora", aseguradora);
                        cmd.Parameters.AddWithValue("@PersonaNum", personanum);
                        cmd.ExecuteNonQuery();
                        }

                    }
                    con.Close();
                    //cleanup
                    GC.Collect();
                    GC.WaitForPendingFinalizers();

                    //rule of thumb for releasing com objects:
                    //  never use two dots, all COM objects must be referenced and released individually
                    //  ex: [somthing].[something].[something] is bad

                    //release com objects to fully kill excel process from running in the background
                    Marshal.ReleaseComObject(xlRange);
                    Marshal.ReleaseComObject(xlWorksheet);

                    //close and release
                    xlWorkbook.Close();
                    Marshal.ReleaseComObject(xlWorkbook);

                    //quit and release
                    xlApp.Quit();
                    Marshal.ReleaseComObject(xlApp);
                    retorna = "1";
                }
                else
                {
                    retorna = "0";
                }
            }
            catch (Exception e)
            {
                retorna = "-1";
                throw;
            }
            return Json(retorna, JsonRequestBehavior.AllowGet);
        }

       private bool PolizaExist(string poliza)
        {
            bool retorna = false;
            AdoConnections.conneciones referencia = new AdoConnections.conneciones();
           
            SqlConnection con = referencia.roscon();
            string stringcomando = " Select Poliza From DB_Smartsys3.dbo.TBL_Poliza "
                                  + " Where Poliza = '" + poliza + "'";

            try
            {
                con.Open();
                SqlCommand commando = new SqlCommand(stringcomando, con);
                SqlDataReader leer = commando.ExecuteReader();
                if (leer.Read())
                {
                    retorna = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
            return retorna;
        }

    }
}