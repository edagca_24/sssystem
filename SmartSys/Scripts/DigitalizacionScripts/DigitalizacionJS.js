﻿



//var existingfile;
var localfecha;
var localpoliza;
var batch;
var flag = false;
var Aseguradora;
var doc;


var fileList = [];
var fileListPreview = [];
var fileListPreviewScanConsulta = [];
var fileListPreviewIndexa = [];
var fileListPreviewIndexaPaths = [];
var fileListIndexaPaths = [];
var rutaarchivo = "";

$(document).ready(function () {

    //se procede a eliminar cualquier temporal cuando se cargar el sistema___//
    cleantemp("limpiar");

    // variables del modal_________________________________________//
    var span = document.getElementsByClassName("close")[0];
    var modal = document.getElementById("myModal");
    var span2 = document.getElementById("close2");
    var indexareg = document.getElementById("RegModal");

    //Reglas de inicio____________________________________________//
    $("#txtpoliza").attr('maxlength', '20');
    $("#txtcliente").attr('maxlength', '30');
    $("#txtpoliza").focus();
    //ocultar el modal________________________________________________//
    $("#btncargar").click(function () {
        //if (validarinputs())
        //{
        var files = $("#file").get(0).files;
        //var ssdata = new FormData();
        var filename;
        $.each($("input[type='file']")[0].files, function (i, file) {
            // data.append('file', file);

            ssdata.set("files", file);
            ssdata.append("Id", $("#txtpoliza").val());
            filename = file.val;
        });
        //var ssdata = new FormData();
        //ssdata.append("data", files[0]);
        //ssdata.append("Id", $("#txtpoliza").val());
        //var filename = files[0].val;          
        uploadfiles(ssdata, filename);

        //}

    });
    $("#btnConsultaExp").on("click",function () {
        if ($("#txtcliente").val() === "") {
            Swal.fire(
                "Consulte una poliza",
                "Consulte una poliza valida para continuar.",
                "error"
            );
        }
        else {
            $("#myModal").modal("show")
            //$("#myModal").attr("style", "display:block");
        }
    });
    $("#txtpoliza").keyup(function (e) {
        if (e.which === 13) {
            ConsultaPoliza();
        }
        else {
            $("#txtcliente").val("");
            $("#txtBatch").val("");
            $("#file").val("");
            $("#file-list-displayScan").empty();
            $("#image").attr("src", "");
            cleantemp("limpiar");

        }
    });
    $("#txtpoliza2").keyup(function (e) {
        if (e.which === 13) {
            ConsultaPolizaIndexacion();
        }
    });
    span.onclick = function () {
        modal.style.display = "none";

    };
    span2.onclick = function () {
        indexareg.style.display = "none";
    };
    //boton de registrar archivos
    $("#btnRegistrar").click(function () {

        if ($("#txtcliente").val() === "") {
            Swal.fire(
                'Poliza no encontrada',
                'Debe consultar una poliza valida e insertatar un archivo valido para poder continuar.',
                'error'
            );
        }
        else {
            filesearching()
            if (flag === true) {
                registerdirectory(localfecha, localpoliza, batch,"");
            }
            else {
                Swal.fire('Error',
                    'Debe cargar por lo menos un archivo para poder registrar un expediente',
                    'error'
                );
            }
        }

    });
    //boton de indexar los documentos...
    $("#btnindexar").click(function () {

        if ($("#txtcliente").val() == "") {
            swal.fire(
                "Sin poliza",
                "Debe consultar una poliza valida para poder continuar",
                "Error"

            );
        }
        else {
            ///funcion de abrir el dialogo que indexa los expedientes....
            $("#RegModal").attr("style", "display:block");
            $("#txtpoliza2").val($("#txtpoliza").val())
            $("#txtcliente2").val($("#txtcliente").val())
            $("#txtbatch2").val($("#txtBatch").val())
            $("#txtaseguradora").val(Aseguradora)

        }

    });
    $("#btnconfirmaindexar").click(function () {
        let items = document.getElementsByClassName("list-group-item active");

        indexar(items[0].id);
    });
    $("#btnfilanizar").click(function () {
       // finalIndexa();
        cleantemp("finalizar");
        //$("#txtpoliza").val("");        
        //indexareg.style.display = "none";

    });
    $("#btnconsultaindex").click(function () {
        consultaindex();

    });
    $("#PlantillaList").change(function () {
        GetPlantillaIndexar();
    });

   
});
//Cargador de arvhivos______________________________________//
function uploadfiles(ssdata) {
    var cont = 0;
    $.ajax({
        url: 'uploadfile',
        type: 'POST',
        contentType: false,
        processData: false,
        data: ssdata,
        success: function (data) {
            if (data === "1") {
                //var filedate = data[1];
                //var fileinfo = "...Temp File type " + data[5];
                //var icono;
                //cont = cont+1;	
                //if (data[5] === ".pdf") {
                //    icono = "/Plantilla/dist/img/pdf.png";
                //}
                //else if (data[5] === ".docx") {
                //    icono = "/Plantilla/dist/img/docx.png";

                //}
                //else {
                //    icono = "/Plantilla/dist/img/fileico.png";
                //}
                registerdirectory(localfecha, localpoliza, batch);
                Swal.fire(
                    'Cargado',
                    'Archivo cargado correctamente.',
                    'success'
                );

                $("#file").val("");
                //$("#image").attr("src", "");
                //$("#modal-table-body").append('\<tr class="tr-dialogbox"><td class="data">' + data[3] + '</td>><td>' + fileinfo + '</td><td>' + data[4] + '</td><td><img src=' + icono + ' style="width: 2.5em; height: 2.5em"></td><td><button id =btneliminar' + (cont) + ' onclick = "return deletefile(this,\'' + data[3] + '\',\'' + data[1] + '\',\'' + data[2] + '\')"  type="button" class="btn btn-danger">X</button></td></tr>');
                //localfecha = data[1];
                //localpoliza = data[2];
            }
            else if (data === "0") {
                Swal.fire(
                    'Sin archivo...',
                    'Debe cargar un archivo para poder continuar.',
                    'question'
                );
            }
            else if (data === "error") {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Algo ha salido mal, intente de nuevo!',
                    footer: '<a href>Why do I have this issue?</a>'
                });
            }
            else if (data === "?") {
                Swal.fire(
                    'Archivo incorrecto...',
                    'El tipo de archivo que intenta cargar es invalido.',
                    'question'
                );
            }

        }, error: function () {
            alert("Ha habido un error y no se ha podido subir el archivo.");
        }
    });
}
//Buscar polizaa____________//
function ConsultaPoliza() {
    
    $("#modal").show();
    $.ajax({
        url: 'ConsultaPoliza',
        type: 'POST',
        //dataType: 'json',
        //contentType: 'application/json',
        data: {
            Referencia: $("#txtpoliza").val()
        },
        success: function (data) {

            if (data[0] !== null) {
                $("#modal").hide();
                $("#txtcliente").val(data[2]);
                $("#txtBatch").val(data[1]);
                batch = $("#txtBatch").val();
                Aseguradora = data[3];
                if (data[4] !== null) {
                    let filespaths = data[4].split("|");
                    let display = "";
                    let iconoindexa = "";

                    $("#file-list-displayScan").empty();
                    fileListPreviewScanConsulta.length = 0;
                    $("#imageConsultScan").attr("src", "");

                    for (var i = 0; i < filespaths.length - 1; i++) {


                        let fileiterating = filespaths[i].split("*")[0];

                        let numcadena = fileiterating.search("Temporal");

                         let completfile = fileiterating.slice(numcadena);
                        fileListPreviewScanConsulta.push(completfile);
                        fileListPreviewScanConsulta.push(filespaths[i].split("*")[0]);
                        var contPreview = fileListPreviewScanConsulta.length === 1 ? 0 : fileListPreviewScanConsulta.length - 1;

                        switch (getFileExtension(filespaths[i].split("*")[1])) {
                            case "pdf":
                                iconoindexa = "/Plantilla/dist/img/pdf.png";
                                break;
                            case "doc":
                                iconoindexa = "/Plantilla/dist/img/docx.png";
                                break;
                            case "xls":
                                iconoindexa = "/Plantilla/dist/img/fileico.png";
                                break;
                            default:
                                iconoindexa = "/Plantilla/dist/img/fileico.png";
                        }


                        display += "<a href='#' onclick='GetTemporalPathScan(this.id,this.name)' id='" + contPreview + "' name='file" + contPreview + "' class='list-group-item list-group-item-action' style=''><img src='" + iconoindexa + "' style='width: 2.5em; height: 2.5em'> " + filespaths[i].split("*")[1] + "</a>";
                    }
                    $("#file-list-displayScan").append(display);
                    
                }
            }
            
            else {
                $("#modal").hide();
                Swal.fire(
                    'Poliza incorrecta',
                    Data[0],
                    'error'
                );
            }


        },
        error: function () {
            $("#modal").hide();
            alert(Data[0]);
        }

    });
}
//Validador de inputs___________________________________////
function validarinputs(poliza) {
    var txtpoliza = $("#txtpoliza").val();
    var txtcliente = $("#txtcliente").val();
    var data;
    var flag = 0;


    if (txtpoliza === "") {
        Swal.fire(
            "Campo vacio",
            "Debe consultlar una poliza para continuar",
            "Error"
        );
        return false;
        $("#txtpoliza").attr("requerd", true);
        $("#txtpoliza").focus();
    }
    else if ($("#txtcliente").val() === "") {
        Swal.fire(
            "Poliza no encontrada",
            "Esta poliza es incorreta",
            "Error"
        );
        $("#txtpoliza").attr("requerd", true);
        $("#txtpoliza").focus();
        return false;
    }
    else if ($("#file").val() === "") {
        Swal.fire(
            "Sin archivo",
            "Debe seleccionar una archivo para continuar.",
            "Error"
        );
        return false;
    }



    return true;

}
//Funcion para borrar los temporales_________////
function cleantemp(tipo) {
    localfecha = "";
    localpoliza = "";
    flag = false;
    batch = "";
    Aseguradora = "";
    $("#txtcliente").val("");
    $("#txtBatch").val("");
    $("#txtdependientes").val("");
    $("#txtparentesco").val(0);
    $("#txtnoreclamo").val("");
    $("#image").attr("src", "");



    $.ajax({
        url: 'erasetemp',
        type: 'POST',
        success: function () {
            if (tipo !== "limpiar") {

                swal.fire(
                    "Proceso Finalizado",
                    "success"
                );
            } else {
                return false;
            }
        }

    });

    $(".tr-dialogbox").each(function () {
        $(".tr-dialogbox").remove();
    });

}
//funcion para eliminar archivos de temporales____///
function deletefile(item, filename, fecha, poliza) {
    var td = item.parentNode;
    var tr = td.parentNode;
    tr.parentNode.removeChild(tr);


    $.ajax({
        url: 'erasefile',
        type: 'post',
        data: {
            filename: filename,
            feccha: fecha,
            polizza: poliza
        },
        success: function (d) {
            if (d !== null) {

                tr.parentnode.removechild(tr);
                swal.fire(
                    "archivo eliminado correctamente",
                    "success"
                );
            }
            else {
                swal.fire(
                    "error",
                    "ha habido un error y no se ha podido eliminar el archivo(problablemente no exista)",
                    "error"
                );
            }

        },


    });





}

////Funcion para eliminar los archivos que se registran
function DeleteFiles(filestodelete) {

    var viewbag = filestodelete;
    $("#modal").show();
    $.ajax({
        url: 'DeleteFiles',
        type: 'POST',
        data: {
            listfiles: viewbag
        
        },
        success: function (d) {
           
        },


    });
}


//funcion para registrar un archivo_________///
function registerdirectory(localfecha, localpoliza, batch,filestodelete) {
    $("#modal").show();
    $.ajax({
        url: 'registerfiles',
        type: 'post',
        data: {
            polizza: $("#txtpoliza").val(),
            batchh: batch,
            listFileDelete: filestodelete
        },
        success: function (d) {
            if (d === 'exito') {
                swal.fire(                   
                       'Archivos registrados',
                    "Archivos cargados exitosamente.",
                    'success',
                   
                ).then(function () {
                    location.reload();
          
                });;
                $('#formulario')[0].reset();
                $("#image").removeAttr("src", "");
                $("#file-list-display").empty();
                fileList.length = 0;
                fileListPreview = 0;
                
                $("#modal").hide();
            }
            else if (d === 'error') {
                $("#modal").hide();
                swal.fire(
                    "error",
                    "ha habido un error y no se ha podido registrar el expediente",
                    "error"
                );
            }

        },


    });

}
//funcion para verificar si existe un archivo en el sistema....///
function filesearching() {
    $.ajax({
        async: false,
        url: 'FileConsult',
        type: 'POST',
        //dataType: 'json',
        //contentType: 'application/json',
        data: {
            polizza: $("#txtpoliza").val()
        },
        success: function (data) {
            if (data !== true) {
                flag = false;
            }
            else {
                flag = true;
            }

        },


    });


}
//funcion de para verificar sin un archivo esta indexado.....////
function indexar(id) {
    


    $("#modal").show();
    $.ajax({
        url: 'Indexa',
        type: 'POST',
        data: {
            poliza: $("#txtpoliza2").val(),
            cliente: $("#txtcliente2").val(),
            aseguradora: $("#txtaseguradora").val(),
            titular: $("#txtcliente2").val(),
            Dependientes: $("#txtdependientes").val(),
            Batch: $("#txtbatch2").val(),
            TipoDoc: $("#txttipodocumento").val(),
            Parentesco: $("#txtparentesco").val(),
            Noreclamo: $("#txtnoreclamo").val(),
            pathfile: fileListPreviewIndexaPaths[id],
            pathfileserver: fileListIndexaPaths[id],
        },
        success: function (data) {
            if (data == "ArchivoIndexado") {
                swal.fire(
                    "El archivo esta indexado",
                    "Puede que otro usuario ya lo haya procesado",
                    "error"
                );
            }
            else if (data == "true") {
                $("#modal").hide();
                swal.fire(
                    "Indexado",
                    "Expediente indexado con exito.",
                    "success"
                );

                $("[name='file" + id + "']").prepend("<i class='far fa-check-circle' style='color: green'></i>");
                $("#file-list-displayIndexa a").removeClass("active");
                $("[name='file" + id + "']").addClass("indexado");
                $("#imageIndexa").length = 0;
                $("#txtdependientes").val('')
                $("#txtparentesco").val(0)
                $("#txttipodocumento").val(0)
                $("#txtnoreclamo").val('')
                ClearFormIndexa();
                //e = jQuery.Event("keyup")
                //e.which = 13 //choose the one you want
                //$("#txtpoliza2").keypress(function () {
                //}).trigger(e)
            }
            else if (data == "false") {
                $("#modal").hide();
                swal.fire(
                    "Error",
                    "Expediente no se ha podido registrar.",
                    "error"
                );
            }
            else {
                $("#modal").hide();
                swal.fire(
                    "Error",
                    "No existen registros relacionados con esta poliza.",
                    "error"
                );
            }
        }
    });


}
//funcion para limpiar todo el proceso---------------
function finalIndexa() {
    $.ajax({
        url: 'finalindex',
        type: 'post',
        data: {
            poliza: $("#txtpoliza").val(),

        },
        success: function (d) {
            if (d === null) {
                swal.fire(
                    "Error",
                    "La indexacion no ha podido culmunar con exito",
                    "error"
                );
            }
            else if (d === "404") {
                swal.fire(
                    "Error",
                    "El directorio no existe reintente registrar e indexar el expediente.",
                    "error"
                );
            }
            else if (d === "correct") {
                swal.fire(
                    "Terminado!",
                    "Proceso terminado",
                    "success"
                );
            }

        },

    });

}

function consultaindex() {
    $.ajax({
        url: 'indexconsult',
        type: 'post',
        success: function (d) {
        },
    });
}

////////////////////////////////////////////////////////////////////////////////Probandooooooooooooo

(function () {
    var fileCatcher = document.getElementById('formulario');
    var fileInput = document.getElementById('file');
    var fileListDisplay = document.getElementById('file-list-display');

    var renderFileList, sendFile;

    $("#btnRegistrarExp").on('click', function (e) {
        e.preventDefault();
        var formdata = new FormData();
        var meindata = [];

        formdata.append("Id", $("#txtpoliza").val());
        //formdata.append("data", fileList );
        formdata.append("datalengh", fileInput.files.length);
        var i = fileInput.files.length - 1;

        fileList.forEach(function () {
            meindata[i] = fileInput.files[i];
            formdata.append(("data" + i), meindata[i]);
            i--;
        });
        filesender(formdata);

    });

    fileInput.addEventListener('change', function (evnt) {
        fileList = [];
        for (var i = 0; i < fileInput.files.length; i++) {
            fileList.push(fileInput.files[i]);
        }
        renderFileList();
    });

    renderFileList = function () {
        //fileListDisplay.innerHTML = '';
        var fileDisplayEl = "";
        var cont = 1;
        var numFile = 0;
        var icono = "";
        fileList.forEach(function (file, index) {
            $("#file-list-display").empty();
            fileListPreview.push(file);
            var contPreview = fileListPreview.length === 1 ? 0 : fileListPreview.length - 1;

            switch (getFileExtension(file.name)) {
                case "pdf":
                    icono = "/Plantilla/dist/img/pdf.png";
                    break;
                case "doc":
                    icono = "/Plantilla/dist/img/docx.png";
                    break;
                case "xls":
                    icono = "/Plantilla/dist/img/fileico.png";
                    break;
                default:
                    icono = "/Plantilla/dist/img/fileico.png";
            }
            fileDisplayEl += "<a href='#' onclick='preview(this.id)' id='" + contPreview + "' class='list-group-item list-group-item-action'><img src='" + icono + "' style='width: 2.5em; height: 2.5em'> " + file.name + "</a>";
            //fileDisplayEl.innerHTML = (index + 1) + ': ' + file.name;

            cont++;
            numFile++;
        });
        $("#file-list-display").append(fileDisplayEl);

    };

    sendFile = function (file) {
        let formData = new FormData();
        let request = new XMLHttpRequest();
        for (let i = 0; i < fileList.length; i++) {
            formData.append(fileList[0].name, fileList[0]);
        }
        //formData.append('Poliza', $("#poliza").val());
        //formData.append('Cliente', $("#txtcliente").val());
        //formData.append('Batch', $("#txtBatch").val());
        request.open("POST", 'registerfiles');
        request.send(formData);



    };

    preview = function (id) {
        $("#image").attr("src", URL.createObjectURL(fileListPreview[id]));
    };

    
})();



function filesender(dato) {
    $("#modal").show();
    $.ajax({
        url: 'uploadfile',
        type: 'POST',
        contentType: false,
        processData: false,
        data: dato,
        success: function (data) {
            if (data[0] === "1") {
                //var filedate = data[1];
                //var fileinfo = "...Temp File type " + data[5];
                //var icono;
                //cont = cont + 1;
                //if (data[5] === ".pdf") {
                //    icono = "/Plantilla/dist/img/pdf.png";
                //}
                //else if (data[5] === ".docx") {
                //    icono = "/Plantilla/dist/img/docx.png";

                //}
                //else {
                //    icono = "/Plantilla/dist/img/fileico.png";
                //}
                registerdirectory(localfecha, localpoliza, batch,data[1]);
               

                $("#file").val("");
                //$("#image").attr("src", "");
                //$("#modal-table-body").append('\<tr class="tr-dialogbox"><td class="data">' + data[3] + '</td>><td>' + fileinfo + '</td><td>' + data[4] + '</td><td><img src=' + icono + ' style="width: 2.5em; height: 2.5em"></td><td><button id =btneliminar' + (cont) + ' onclick = "return deletefile(this,\'' + data[3] + '\',\'' + data[1] + '\',\'' + data[2] + '\')"  type="button" class="btn btn-danger">X</button></td></tr>');
                //localfecha = data[1];
                //localpoliza = data[2];
                $("#modal").hide();
            }
            else if (data[0] === "0") {
                Swal.fire(
                    'Sin archivo...',
                    'Debe cargar un archivo para poder continuar.',
                    'question'
                );
                $("#modal").hide();
            }
            else if (data[0] === "error") {
                $("#modal").hide();
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Algo ha salido mal, intente de nuevo!',
                    footer: '<a href>Why do I have this issue?</a>'
                });
            }
            else if (data[0] === "?") {
                $("#modal").hide();
                Swal.fire(
                    'Archivo incorrecto...',
                    'El tipo de archivo que intenta cargar es invalido.',
                    'question'
                );
            }

        }, error: function () {
            $("#modal").hide();
            alert("Ha habido un error y no se ha podido subir el archivo.");
        }
    });

}

function getFileExtension(filename) {
    var ext = /^.+\.([^.]+)$/.exec(filename);
    return ext == null ? "" : ext[1];
}

function DeleteFile(item) {
    Swal.fire({
        title: 'Seguro que deseas quitar este archivo?',
        text: "",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si!',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.value) {

            $("#" + item).remove();
            delete fileListPreview[item];
            Swal.fire(
                'Eliminado!',
                '',
                'success'
            );
        }
    });
}

function savePlantilla() {
    let forms = $('#formPlantilla')[0];
    let ListPlantilla = [];

    var list = "";
    var filas = document.querySelectorAll("#tableplantilla tbody tr");
    var columnas;

    if (filas.length !== 0) {
        filas.forEach(function (e) {
            columnas = e.querySelectorAll("td");
            list += columnas[0].firstElementChild.value + "-" + columnas[1].firstElementChild.value + ",";
            //ListPlantilla.push(columnas[0].firstElementChild.value + "-" + columnas[0].firstElementChild.value);
        });
    }
    console.log(ListPlantilla);
    let param = {
        NombreList: list,
        NombrePlantilla: $("#nombrePlantilla").val()
    };

    let Model = new Object();
    Model.NombreList = ListPlantilla,
        Model.NombrePlantilla = $("#nombrePlantilla").val();

    $.ajax({
        type: "POST", // Verbo HTTP
        url: "CreatePlantilla", // Url        
        data: param
    })
        // Se ejecuta si todo fue bien.
        .done(function (result) {

        });
    //let request = new XMLHttpRequest();
    //request.open("POST", 'CreatePlantilla');
    //  request.send(param);
}

function GetPlantillaIndexar() {

    let param = {
        IdPlantilla: $("#PlantillaList option:selected").val()
    };

    $.ajax({
        type: "GET", // Verbo HTTP
        url: "GetPlantillaData", // Url        
        data: param
    })
        // Se ejecuta si todo fue bien.
        .done(function (result) {
            $(result).each(function () {

                let html = "";

                html += '<tr><td style="position: relative;left: 228px;"><input type="text" class="form-control" name="" id="label" placeholder="Nombre del Campo" /></td>';
                html += '<td style="position: relative;left: 230px;"><select id="tipo" name="rol[]" class="form-control"><option selected = "" disabled = "">Tipo de Campo</option><option>Texto</option><option>Texto Grande</option><option>Fecha</option><option>Lista</option></select ></td>';
                html += '<td style="position: relative;left: 233px;top: 5px;"><p class="delete" onclick="DeleteItemsTable(this)">x</p></td></tr>';
                $('.dynamic-element').show();
                $("#tableplantillacampos").append(html);
            });

        });
}

function ConsultaPolizaIndexacion() {
    $("#modal").show();
    $.ajax({
        url: 'ConsultaPolizaIndexado',
        type: 'POST',
        //dataType: 'json',
        //contentType: 'application/json',
        data: {
            poliza: $("#txtpoliza2").val()
        },
        success: function (data) {
            let display = ""
            let iconoindexa = ""
            $("#modal").hide();
            if (data[0] !== null & data[0] !== "1") {

                $("#txtcliente2").val(data[2]).attr("disabled", true);
                $("#txtbatch2").val(data[1]).attr("disabled", true);
                $("#txtaseguradora").val(data[3]).attr("disabled", true);


            }
            else if (data[0] == "1") {
                Swal.fire(
                    'Esta póliza no tiene expedientes registrados',
                    '',
                    'error'
                );
            } else {
                Swal.fire(
                    'Hubo un error, favor contacte al soporte tecnico',
                    '',
                    'error'
                );
            }
            if (data[5] !== null) {
                
                fileListPreviewIndexaPaths.length = 0;
                fileListIndexaPaths.length = 0;
                fileListPreviewIndexa.length = 0;
                $("#file-list-displayIndexa").empty()
                $("#imageIndexa").attr("src","")

                let filestemp = data[5].split("|")
                

                for (var i = 0; i < filestemp.length - 1; i++) {

                    let filespaths = data[6].split("|")
                    fileListIndexaPaths.push(filespaths[i].split("#")[0]);

                    let fileiterating = filestemp[i].split("*")[0]
                    fileListPreviewIndexaPaths.push(filestemp[i].split("*")[1])
                    //let numcadena = fileiterating.search("Temporal");
                    //let completfile = fileiterating.slice(numcadena);
                    //fileListPreviewIndexa.push(completfile);
                    fileListPreviewIndexa.push(filespaths[i].split("#")[0]);
                    var contPreview = fileListPreviewIndexa.length === 1 ? 0 : fileListPreviewIndexa.length - 1;

                    switch (getFileExtension(filestemp[i].split("*")[1])) {
                        case "pdf":
                            iconoindexa = "/Plantilla/dist/img/PDFIcon.png";
                            break;
                        case "doc":
                            iconoindexa = "/Plantilla/dist/img/Word-icon.png";
                            break;
                        case "xls" || "xlsx":
                            iconoindexa = "/Plantilla/dist/img/Excel-icon.png";
                            break;
                        default:
                            iconoindexa = "/Plantilla/dist/img/fileico.png";
                    }

                    if (filespaths[i].split("#")[1] == "NO") {
                        display += "<a href='#' onclick='GetTemporalPathIndexa(this.id,this.name)' id='" + contPreview + "' name='file" + contPreview + "' class='list-group-item list-group-item-action'><img src='" + iconoindexa + "' style='width: 2.0em; height: 2.0em'> " + filestemp[i].split("*")[1] + " </a>";
                    } 
                        //else {
                        
                    //    display += "<a href='#' onclick='previewIndexa(this.id,this.name)' id='" + contPreview + "' name='file" + contPreview + "' class='list-group-item list-group-item-action indexado' style=''><i class='far fa-check-circle' style='color: green'></i><img src='" + iconoindexa + "' style='width: 2.5em; height: 2.5em'> " + filestemp[i].split("*")[1] + "</a>";
                    //}
                    
            //fileDisplayEl.innerHTML = (index + 1) + ': ' + file.name;

                }
                $("#file-list-displayIndexa").append(display);
            }


        },
        error: function () {
            $("#modal").hide();
            alert("Ha habido un error y no se puede consultar la poliza.");
        }

    });
}

function previewIndexa(path, id) {
    let numcadena = path.search("Temporal");
    let completfile = path.slice(numcadena);
    $("#file-list-displayIndexa a").removeClass("active")
    if ($("[name='" + id + "']").hasClass("indexado") != true) {

        $("[name='" + id + "']").addClass("active")
    }
   

    

    $("#imageIndexa").attr("src", "../" + completfile);
};

function previewScan(path, id) {
    
   
    let numcadena = path.search("Temporal");
    let completfile = path.slice(numcadena);
    //completfile = completfile.replace("\g","/");

    $("#file-list-displayScan a").removeClass("active")
        $("[name='" + id + "']").addClass("active")
    



    $("#imageConsultScan").attr("src", "../" + completfile);
};


function preindexarexp() {
    let items = document.getElementsByClassName("list-group-item active");
    if (items[0] != undefined) {
        var forms = document.getElementsByClassName('needs-validation')[0]
        if (validationForm(forms) === true) {
            indexar(items[0].id);
        }
    } else {
        Swal.fire('Error',
            'No has seleccionado un archivo para indexar',
            'error'
        );
        return false
    }
    
}

function GetTemporalPathScan(iterating, name) {
    
        let param = {
            PathFile: fileListPreviewScanConsulta[iterating]
        };
    
    $("#modal").show();

    $.ajax({
        type: "POST", // Verbo HTTP
        url: "GetTemporalPath", // Url        
        data: param
    })
        // Se ejecuta si todo fue bien.
        .done(function (result) {
            previewScan(result,name)
            $("#modal").hide();
        });

}

function GetTemporalPathIndexa(iterating, name) {

    let param = {
        PathFile: fileListPreviewIndexa[iterating]
    };

    $("#modal").show();

    $.ajax({
        type: "POST", // Verbo HTTP
        url: "GetTemporalPath", // Url        
        data: param
    })
        // Se ejecuta si todo fue bien.
        .done(function (result) {
            previewIndexa(result, name)
            $("#modal").hide();
        });

}

function ClearFormIndexa(){
    $("#formIndexa").find(':input').each(function () {
        var elemento = this;
        $("#" + elemento.id).removeClass("is-valid")
    });
}

