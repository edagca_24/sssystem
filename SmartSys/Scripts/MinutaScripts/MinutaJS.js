﻿/// <reference path="../jquery-3.4.1.min.js" />
/// <reference path="../../plantilla/plugins/datatables/datatables.bootstrap4.min.js" />
/// <reference path="../exctrascripts/jspdf.min.js" /> 


var num = 0;

$(document).ready(function () {
    //$("#modal").show();
    /////////////////////Instancias de select2 para cada selectlist////////////////////
    $(".SelectClienteM").select2({

        placeholder: "Buscar...",
        allowClear: false,
        //theme: "bootstrap4",
        width: '83%',
        ajax: {
            url: "/Minuta/GetData",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term,
                    tipo: 'Cliente'
                };
            },
            processResults: function (data, params) {
                return {
                    results: data
                };
            }
        }
    });

    $(".AsistentesMselect2").select2({

        placeholder: "Buscar asistente",
        allowClear: false,
        //theme: "bootstrap4",
        width: '83%',
        ajax: {
            url: "/Minuta/GetAsistentes",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term,
                    tipo: 'Asistentes'
                };
            },
            processResults: function (data, params) {
                return {
                    results: data
                };
            }
        }
    });

    $(".selectAsignado").select2({

        placeholder: "Buscar asistente",
        allowClear: false,
        //theme: "bootstrap4",
        width: '83%',
        ajax: {
            url: "/Minuta/GetData",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term,
                    tipo: 'Usuario'
                };
            },
            processResults: function (data, params) {
                return {
                    results: data
                };
            }
        }
    });

    //////////////////////////////////////////////

    //////
    $("#MinutaTemporalList").change(function () {
        GetDataMinutaTemporal(this.value);
    });

    $("#formCompletarAsignacion").hide();
    $("#formEditarMinuta").hide();
    //getAsistentes();

    /// Bloquea fechas anteriores en datepicker ////
    var today = new Date().toISOString().split('T')[0];
    document.getElementsByClassName("datepickerhtml")[0].setAttribute('min', today);
    /////////////////////////////////////////////

    $('#Cliente').keyup(function () { // aqui va el input text para la busqueda


        var input = document.getElementById("ClienteM");


        $('#result_Cliente').html(''); // aqui va la data que se va a mostrar

        if (this.value !== "") {
            getListMinuta(this.value, 'Cliente');
        } else {
            return false;
        }
    });


    $('.Asignado').keyup(function () { // aqui va el input text para la busqueda      

        if (this.value !== "") {
            getListMinuta(this.value, this.id);
        } else {
            return false;
        }
    });



    ////////////////////// document ready fin
});

$('#buttonSaveEditarM').on('click', function (e) {
    e.preventDefault();
    var forms = document.getElementsByClassName('needs-validation')[0];
    if (validationForm(forms)) {
        EditarMinuta();
    }

});

function getListMinuta(data, container) {

    var param = {
        criterio: data,
        tipo: container
    };

    $.ajax({
        url: "GetData", // Url
        type: "GET", // Verbo HTTP
        data: param
    })
        // Se ejecuta si todo fue bien.
        .done(function (result) {

            if (result !== null) {
                if (container === "Asignado") {
                    fillListAsignado(result);
                } else if (container === "Cliente") {
                    fillListCliente(result);
                }
            }
        })
        // Se ejecuta si se produjo un error.
        .fail(function (xhr, status, error) {

        })
        // Hacer algo siempre, haya sido exitosa o no.
        .always(function () {

        });
}

function getListAsignado(data, id) {
    $('#result_' + id).empty();
    var param = {
        criterio: data

    };

    $.ajax({
        url: "GetUsuarios", // Url
        type: "GET", // Verbo HTTP
        data: param
    })
        // Se ejecuta si todo fue bien.
        .done(function (result) {

            if (result !== null) {
                fillListAsignado(result, id);
            }
        })
        // Se ejecuta si se produjo un error.
        .fail(function (xhr, status, error) {

        })
        // Hacer algo siempre, haya sido exitosa o no.
        .always(function () {

        });
}


function getAsistentes() {


    $.ajax({
        url: "GetAsistentes", // Url
        type: "GET" // Verbo HTTP       
    })
        // Se ejecuta si todo fue bien.
        .done(function (result) {

            if (result !== null) {
                $('#AsistentesList').find('option').remove().end();
                $.each(result, function (val, text) {
                    $('#AsistentesList').append($('<option></option>').html(text));
                });
            }
        })
        // Se ejecuta si se produjo un error.
        .fail(function (xhr, status, error) {

        })
        // Hacer algo siempre, haya sido exitosa o no.
        .always(function () {

        });
}

function fillListCliente(data) {
    $('#result_Cliente').empty();
    for (var i = 0; i < data.length; i++) {


        $('#result_ClienteM').append(`<li class="list-group-item link-class" onclick="setItemCliente(this)" value="${data[i].ID_Ente}" name="${data[i].ID_Ente} +'|'+ ${data[i].NombreCompleto}">${data[i].NombreCompleto}</li>`);

        $('#result_Cliente').append(`<li class="list-group-item link-class" onclick="setItemCliente(this)" value="${data[i].ID_Ente}" name="${data[i].ID_Ente} +'|'+ ${data[i].NombreCompleto}">${data[i].NombreCompleto}</li>`);

    }

}

function fillListAsignado(data, id) {
    $('#result_Asignado' + id).empty();
    for (var i = 0; i < data.length; i++) {


        $('#result_Asignado' + id).append(`<li class="list-group-item link-class"  onclick='setItemAsignado(this,this.id)' value="${data[i].IdUsuario}" title="">${data[i].Usuario}</li>`);

        $('#result_' + id).append(`<li class="list-group-item link-class" onclick='setItemAsignado(this,this.name)' value="${data[i].IdUsuario}" title="${id}">${data[i].Usuario}</li>`);

    }

}

function setItemCliente(data) {

    $('#Cliente').val(data.textContent).attr('value', data.value);
    $("#result_Cliente").empty();
}

function setItemAsignado(data, id) {

    $('#' + num).val(data.textContent).attr('value', data.value);
    $("#result_" + num).empty();
}

function DeleteItemsTableMin(items) {
    $("#addr" + items).remove();
    conteo2--;

}


$("#AddAsistente").click(function () {
    num = num + 1;
    var idasignado = $("#Asignado option:selected").val();
    var nombreasignado = $("#Asignado option:selected").text();
    //contructor de nueva fila en string para la tabla...
    var newrow = "";
    newrow += "<tr id=''>";
    newrow += "<td>";
    newrow += "<input type='hidden' id='AsignadoId" + num + "' value='" + idasignado + "'>";
    newrow += "<input type='text' class='form-control required' id='Asignado" + num + "' value='" + nombreasignado + "' disabled>";
    //newrow += "  <select id='listAsignado" + num +"' class='form-control col-10 selectAsignado"+ num +"'><option></option></select>";
    newrow += "</td>";
    newrow += "<td>";
    newrow += "<textarea class='form-control required' type='text' name='Tarea' id='idTarea'></textarea>";
    newrow += "</td>";
    newrow += "<td>";
    newrow += "<textarea class='form-control ' type='text' name='Acuerdo' id='idAcuerdo'></textarea>";
    newrow += "</td>";
    newrow += "<td>";
    newrow += "<textarea class='form-control ' type='text' name='Recomendacion' id='idRecomendacion'></textarea>";
    newrow += "</td>";
    newrow += "<td>";
    newrow += "<input type='date' class='form-control datepickerhtml col-12 required' id='FechaEntrega' name='FechaEntrega' onkeypress='return pulsar(event)' onblur='validDateTime(this.value, this)' placeholder=''>";
    newrow += "</td>";
    newrow += "<td>";
    newrow += "<button onclick='SomeDeleteRowFunction(this)' class='btnmore btn btn-danger' name='btnmore' id='btnmore' type='button'>X</button>";
    newrow += "</td>";
    newrow += "</tr>";
    //var agregacion = $(newrow);
    $("#maintable").append(newrow);
    $('#Asignado').empty();
    $('#Asignado').val(null).trigger('change');

});
$("#AddAsistenteEdit").click(function () {
    num = num + 1;
    var idasignado = $("#AsignadoEdit option:selected").val();
    var nombreasignado = $("#AsignadoEdit option:selected").text();
    //contructor de nueva fila en string para la tabla...
    var newrow = "";
    newrow += "<tr id=''>";
    newrow += "<td>";
    newrow += "<input type='hidden' id='AsignadoIdEdit" + num + "' value='" + idasignado + "'>";
    newrow += "<input type='text' class='form-control required' id='AsignadoEdit" + num + "' value='" + nombreasignado + "' disabled>";
    //newrow += "  <select id='listAsignado" + num +"' class='form-control col-10 selectAsignado"+ num +"'><option></option></select>";
    newrow += "</td>";
    newrow += "<td>";
    newrow += "<textarea class='form-control required' type='text' name='Tarea' id='idTareaEdit'></textarea>";
    newrow += "</td>";
    newrow += "<td>";
    newrow += "<textarea class='form-control ' type='text' name='Acuerdo' id='idAcuerdoEdit'></textarea>";
    newrow += "</td>";
    newrow += "<td>";
    newrow += "<textarea class='form-control ' type='text' name='Recomendacion' id='idRecomendacionEdit'></textarea>";
    newrow += "</td>";
    newrow += "<td>";
    newrow += "<input type='date' class='form-control datepickerhtml col-12 required' id='FechaEntregaEdit' name='FechaEntrega' onkeypress='return pulsar(event)' onblur='validDateTime(this.value, this)' placeholder=''>";
    newrow += "</td>";
    newrow += "<td>";
    newrow += "<button onclick='SomeDeleteRowFunction(this)' class='btnmore btn btn-danger' name='btnmore' id='btnmoreEdit' type='button'>X</button>";
    newrow += "</td>";
    newrow += "</tr>";
    //var agregacion = $(newrow);
    $("#maintableEdit").append(newrow);


});

function SomeDeleteRowFunction(item) {

    Swal.fire({
        title: 'Seguro que deseas eliminar esta linea?',
        text: "",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar linea!',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.value) {
            var td = item.parentNode;
            var tr = td.parentNode; // the row to be removed
            tr.parentNode.removeChild(tr);
            Swal.fire(
                'Eliminado!',
                '',
                'success'
            );
        }
    });
}




function EditarMinuta() {
    var objectTable = [];
    var filas = document.querySelectorAll("#idTableCreacionMinutaEdit tbody tr");
    var columnas;

    if (filas.length !== 0) {
        filas.forEach(function (e) {
            columnas = e.querySelectorAll("td");

            var obj = {
                IdAsignado: columnas[0].firstElementChild.defaultValue,
                Tarea: columnas[1].firstElementChild.value,
                Acuerdo: columnas[2].firstElementChild.value,
                Recomendacion: columnas[3].firstElementChild.value,
                Fecha_Entrega: columnas[4].firstElementChild.value
            };

            objectTable.push(obj);
            console.log(objectTable);
        });
    }


    var ModelMinuta = new Object();


    ModelMinuta.Asignaciones = objectTable,
        ModelMinuta.Resumen = tinymce.get('ResumenEdit').getContent(),
        ModelMinuta.IdMinuta = $("#MinutaID").val();


    $.ajax({
        type: 'POST',
        url: 'EditarMinuta',
        data: JSON.stringify(ModelMinuta),
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            if (data === '1') {
                Swal.fire(
                    'Minuta actualizada correctamente',
                    'Click aqui↓',
                    'success'
                ).then(function () {
                    location.reload();
                });

            } else if (data === '0') {
                Swal.fire(
                    'Ocurrio un error, favor comunicarse con soporte tecnico',
                    'Click aqui↓',
                    'error'
                );
            }
        }, error: function (exception) {
            alert('Exeption:' + exception);
            $('#submit').val('Save');
        }

    });
}

function SetDetailsMinuta(Id) {

    var param = {
        Id: Id

    };

    $.ajax({
        url: "GetDetailsMinuta", // Url
        type: "GET", // Verbo HTTP
        data: param
    })
        // Se ejecuta si todo fue bien.
        .done(function (result) {

            if (result !== null) {
                fillDetails(result, Id);
            }
        })
        // Se ejecuta si se produjo un error.
        .fail(function (xhr, status, error) {

        })
        // Hacer algo siempre, haya sido exitosa o no.
        .always(function () {

        });
}

function fillDetails(data, Id) {
    $("#CompletarAsignacion").show();
    let verifica = 0;
    var html = "";
    var html2 = "";
    var htmlEdit = "";
    $('#Nombre_Cliente').text(data.Cliente);
    $('#Contacto').text(data.Contacto);
    $('#Fecha').text(formatDate(data.FechaReunion));
    $('#UsuarioCreador').text(data.UsuarioRegistro);
    $('#MotivoReunion').text(data.MotivoReunion);
    $('#Lugar').val(data.Lugar);
    tinymce.get('ResumenDetalle').setContent(data.Resumen);
    tinymce.get('ResumenDetalle').getBody().setAttribute('contenteditable', false);
    $('.tox-editor-header').hide();
    $('#MinutaID').val(Id);

    $("#tableAsistentes  tbody").empty();
    for (var i = 0; i < data.Asistentes.length; i++) {

        $("#tableAsistentes  tbody").append("<tr id=''><td>" + data.Asistentes[i].Asistente + "</td></tr>");
    }

    $("#tableAsignaciones  tbody").empty();
    for (var y = 0; y < data.Asignaciones.length; y++) {

        html += "<tr id=''>";
        html += "<td>" + data.Asignaciones[y].Asignado + "</td>";
        html += "<td><textarea ondblclick='ViewModal(this)' readonly style='resize: none; height: 58px; width:197px' data-toggle='tooltip' data-placement='top' title='Hacer doble click para visualizar el texto completo'>" + data.Asignaciones[y].Tarea + "</textarea></td>";
        html += "<td><textarea ondblclick='ViewModal(this)' readonly style='resize: none; height: 58px; width:197px' data-toggle='tooltip' data-placement='top' title='Hacer doble click para visualizar el texto completo'> " + data.Asignaciones[y].Acuerdo + "</textarea></td>";
        html += "<td><textarea ondblclick='ViewModal(this)' readonly style='resize: none; height: 58px; width:197px' data-toggle='tooltip' data-placement='top' title='Hacer doble click para visualizar el texto completo'>" + data.Asignaciones[y].Recomendacion + "</textarea></td>";
        html += "<td>" + formatDate(data.Asignaciones[y].Fecha_Entrega) + "</td></tr>";

        ///////       
    }

    $("#tableAsignaciones  tbody").append(html);

    for (var r = 0; r < data.Asignaciones.length; r++) {
        $("#tableCompAsignaciones  tbody").empty();

        html2 += "<tr id=''>";
        html2 += "<td>" + data.Asignaciones[r].Asignado + "</td>";
        html2 += "<td><textarea ondblclick='ViewModal(this)' readonly style='resize: none; height: 58px; width:197px' data-toggle='tooltip' data-placement='top' title='Hacer doble click para visualizar el texto completo'>" + data.Asignaciones[r].Tarea + "</textarea></td>";
        html2 += "<td><textarea ondblclick='ViewModal(this)' readonly style='resize: none; height: 58px; width:197px' data-toggle='tooltip' data-placement='top' title='Hacer doble click para visualizar el texto completo'> " + data.Asignaciones[r].Acuerdo + "</textarea></td>";
        html2 += "<td><textarea ondblclick='ViewModal(this)' readonly style='resize: none; height: 58px; width:197px' data-toggle='tooltip' data-placement='top' title='Hacer doble click para visualizar el texto completo'>" + data.Asignaciones[r].Recomendacion + "</textarea></td>";
        html2 += "<td>" + formatDate(data.Asignaciones[r].Fecha_Entrega) + "</td>";
        if (data.Asignaciones[r].Condicion === "Completada") {
            html2 += "<td style='color:green'>" + data.Asignaciones[r].Condicion + "</td>";
            html2 += "<td><i class='fas fa-check'></i></td>";
            verifica = 1;
        } else if (data.Asignaciones[r].Condicion === "En Proceso") {
            verifica = 0;
            html2 += "<td style='color:red'>" + data.Asignaciones[r].Condicion + "</td>";
            html2 += "<td><a href='#' class='btn btn-primary float-right' onclick='confirmCompletarAsignacion(this," + Id + ")' value='" + data.Asignaciones[r].IdAsignacion + "'>Completar</a></td></tr>";
        }
    }

    $("#tableCompAsignaciones  tbody").append(html2);
    //////////////////////////////////////Llenando campos de editar la minuta////////

    //var newOption = new Option(decodeURIComponent(data.NombreCliente), data.IdCliente, true, true);
    //$('#ClienteMEdit').append(newOption).trigger('change');
    //$('#ContactoEdit').val(data.Contacto);
    //$('#idPrioridadEdit').val(data.Prioridad);
    //$('#FechaEdit').val(data.FechaReunion);
    //$('#ContactoEdit').val(data.Contacto);
    //$('#ContactoEdit').val(data.Contacto);
    //$('#ContactoEdit').val(data.Contacto);
    tinymce.get('ResumenEdit').setContent(data.Resumen);

    $("#idTableCreacionMinutaEdit  tbody").empty();
    for (var y = 0; y < data.Asignaciones.length; y++) {
        format = function date2str(x, y) {
            var z = {
                M: x.getMonth() + 1,
                d: x.getDate(),
                h: x.getHours(),
                m: x.getMinutes(),
                s: x.getSeconds()
            };
            y = y.replace(/(M+|d+|h+|m+|s+)/g, function (v) {
                return ((v.length > 1 ? "0" : "") + eval('z.' + v.slice(-1))).slice(-2);
            });

            return y.replace(/(y+)/g, function (v) {
                return x.getFullYear().toString().slice(-v.length)
            });
        };
        date = format(new Date(data.Asignaciones[y].Fecha_Entrega), 'yyyy-MM-dd');

        num = num + 1;

        //contructor de nueva fila en string para la tabla...
        var newrow = "";
        newrow += "<tr id=''>";
        newrow += "<td>";
        newrow += "<input type='hidden' id='AsignadoIdEdit" + num + "' value='" + data.Asignaciones[y].IdAsignado + "'>";
        newrow += "<input type='text' class='form-control required' id='AsignadoEdit" + num + "' value='" + data.Asignaciones[y].Asignado + "' disabled>";
        //newrow += "  <select id='listAsignado" + num +"' class='form-control col-10 selectAsignado"+ num +"'><option></option></select>";
        newrow += "</td>";
        newrow += "<td>";
        newrow += "<textarea class='form-control required' type='text' name='Tarea' id='idTareaEdit' value:''>" + data.Asignaciones[y].Tarea + "</textarea>";
        newrow += "</td>";
        newrow += "<td>";
        newrow += "<textarea class='form-control ' type='text' name='Acuerdo' id='idAcuerdoEdit' value:''>" + data.Asignaciones[y].Tarea + "</textarea>";
        newrow += "</td>";
        newrow += "<td>";
        newrow += "<textarea class='form-control ' type='text' name='Recomendacion' id='idRecomendacionEdit' value:''>" + data.Asignaciones[y].Tarea + "</textarea>";
        newrow += "</td>";
        newrow += "<td>";
        newrow += "<input type='date' class='form-control datepickerhtml col-12 required' id='FechaEntregaEdit' name='FechaEntrega' onkeypress='return pulsar(event)' onblur='validDateTime(this.value, this)' value='" + date + "'>";
        newrow += "</td>";
        newrow += "<td>";
        newrow += "<button onclick='SomeDeleteRowFunction(this)' class='btnmore btn btn-danger' name='btnmore' id='btnmoreEdit' type='button'>X</button>";
        newrow += "</td>";
        newrow += "</tr>";
        //var agregacion = $(newrow);
        $("#maintableEdit").append(newrow);
    }

    ///////////////////

    ////////Funcion de que verifica y oculta los botones

    if (verifica === 1) {
        $("#CompletarAsignacion").hide();
        $("#EditarMinuta").hide();
    }
    $('#ModalDetalleMinuta').modal('show');

}

function ViewModal(param) {
    $("#textView").val('');
    $("#textView").val(param.value);
    $('#ModalView').modal('show');
}

function BotonCompletar() {
    $("#formDetalleMinuta").hide("slow");
    $("#formCompletarAsignacion").show("slow");

}

function volverMinutaAsignaciones() {
    $("#formCompletarAsignacion").hide("slow");
    $("#formDetalleMinuta").show("slow");
    $("#formCompletarAsignacion")[0].reset();
}

function confirmCompletarAsignacion(data, Id) {
    Swal.fire({
        title: 'Seguro que deseas dar por terminada esta asignacion?',
        text: "",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, seguro!',
        cancelButtonText: 'No, cancelar'
    }).then((result) => {
        if (result.value) {
            completarAsignacion(data, Id);
        } else {
            return false;
        }
    });
}

function completarAsignacion(data, Id) {



    var param = {
        Id: parseInt(data.attributes.value.value)
    };

    $.ajax({
        type: "POST", // Verbo HTTP
        url: "conpletarAsignacion", // Url        
        data: param
    })
        // Se ejecuta si todo fue bien.
        .done(function (result) {

            if (result !== null) {
                if (result === '1') {
                    Swal.fire(
                        'Asignacion completada correctamente',
                        'Click aqui↓',
                        'success'
                    ).then(function () {
                        SetDetailsMinuta(Id);
                    });

                } else if (result === '0') {
                    Swal.fire(
                        'Ocurrio un error, favor comunicarse con soporte tecnico',
                        'Click aqui↓',
                        'error'
                    );
                }
            }
        })
        // Se ejecuta si se produjo un error.
        .fail(function (xhr, status, error) {

        })
        // Hacer algo siempre, haya sido exitosa o no.
        .always(function () {

        });

}

function sortTable(n, idtable) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById(idtable);
    switching = true;
    // Set the sorting direction to ascending:
    dir = "asc";
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
        // Start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /* Loop through all table rows (except the
        first, which contains table headers): */
        for (i = 1; i < (rows.length - 1); i++) {
            // Start by saying there should be no switching:
            shouldSwitch = false;
            /* Get the two elements you want to compare,
            one from current row and one from the next: */
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            /* Check if the two rows should switch place,
            based on the direction, asc or desc: */
            if (dir === "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            } else if (dir === "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            /* If a switch has been marked, make the switch
            and mark that a switch has been done: */
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            // Each time a switch is done, increase this count by 1:
            switchcount++;
        } else {
            /* If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again. */
            if (switchcount === 0 && dir === "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}

function formModalControl(boton) {

    switch (boton) {
        case "CompletarAsignacion":
            $("#formCompletarAsignacion").show("slow");
            $("#formDetalleMinuta").hide("slow");
            $("#divBotonesDetalle").attr("hidden", true);
            break;
        case "EditarMinuta":
            $("#formEditarMinuta").show("slow");
            $("#formDetalleMinuta").hide("slow");
            $("#divBotonesDetalle").attr("hidden", true);
            break;
        case "botonVolverCompAsign":
            $("#formDetalleMinuta").show("slow");
            $("#formCompletarAsignacion").hide("slow");
            $("#divBotonesDetalle").attr("hidden", false);
            break;
        case "botonVolverEditar":
            $("#formDetalleMinuta").show("slow");
            $("#formEditarMinuta").hide("slow");
            $("#divBotonesDetalle").attr("hidden", false);
            break;
        case "closeModalDetalle":
            location.reload();
            $('#ModalDetalle').modal('show');
            //$("#formCompletar").hide("slow");
            //$("#formDetalle").show("slow");
            //$("#divBotonesDetalle").attr("hidden", false);
            //$('#formEditar')[0].reset();    
            break;
        //case "botonVolverPosponer":
        //    $("#formPosponer").hide("slow");
        //    $("#formDetalle").show("slow");
        //    $("#divBotonesDetalle").attr("hidden", false);
        //    $('#formPosponer')[0].reset();
        //    $('.required').removeClass('is-invalid')
        //    $('.required').removeClass('is-valid')
        //    break;
        //case "botonVolverCompletar":
        //    $("#formCompletar").hide("slow");
        //    $("#formDetalle").show("slow");
        //    $("#divBotonesDetalle").attr("hidden", false);
        //    $('#formCompletar')[0].reset();
        //    $('.required').removeClass('is-invalid')
        //    $('.required').removeClass('is-valid')
        //    break;
        //case "guardarEditar":
        //    $("#formEditar").hide("slow");
        //    $("#formDetalle").show("slow");
        //    $("#divBotonesDetalle").attr("hidden", false);
        //    $('#formEditar')[0].reset();
        //    $("#tbodyEdit").html('');
        //    $("#searchResponsableEdit").val('');
        //    break;
        //case "guardarCompletar":
        //    $("#formCompletar").hide("slow");
        //    $("#formDetalle").show("slow");
        //    $("#divBotonesDetalle").attr("hidden", false);
        //    $('#formCompletar')[0].reset();
        //    break;
        //case "guardarPosponer":
        //    $("#formPosponer").hide("slow");
        //    $("#formDetalle").show("slow");
        //    $("#divBotonesDetalle").attr("hidden", false);
        //    $('#formPosponer')[0].reset();
        //    break;

    }
}

function GetDataMinutaTemporal(id) {

    var param = {
        Id: id
    };

    $.ajax({
        url: "GetDataMinutaTemporal", // Url
        type: "GET", // Verbo HTTP
        data: param
    })
        // Se ejecuta si todo fue bien.
        .done(function (result) {
            //(result);
            $.each(result, function (index, value) {
                format = function date2str(x, y) {
                    var z = {
                        M: x.getMonth() + 1,
                        d: x.getDate(),
                        h: x.getHours(),
                        m: x.getMinutes(),
                        s: x.getSeconds()
                    };
                    y = y.replace(/(M+|d+|h+|m+|s+)/g, function (v) {
                        return ((v.length > 1 ? "0" : "") + eval('z.' + v.slice(-1))).slice(-2)
                    });

                    return y.replace(/(y+)/g, function (v) {
                        return x.getFullYear().toString().slice(-v.length)
                    });
                };

                var newOption = new Option(decodeURIComponent(value.NombreCliente), value.IdCliente, true, true);
                $('#ClienteM').append(newOption).trigger('change');
                $('#Contacto').val(value.Contacto);
                date = format(new Date(value.FechaReunion), 'yyyy-MM-dd');
                $('#Fecha').val(date);
                $('#MotivoReunion').val(value.Motivo);
                $('#lugar').text(value.Lugar);



                //    retorna += "$('#MotivoReunion').val(" + item.Motivo_Reunion + ");";
                //    retorna += "$('#lugar').text(" + item.Lugar + ");";
            });
        })
        // Se ejecuta si se produjo un error.
        .fail(function (xhr, status, error) {

        })
        // Hacer algo siempre, haya sido exitosa o no.
        .always(function () {

        });
}



function modalcontent() {
    $("#valorentidad").text($("#Nombre_Cliente").text());
    //$("#valorcontacto").text($("#Contacto").text());
    $("#valorfecha").text($("#Fecha").text());
    //$("#valorusario").text($("#UsuarioCreador").text());
    //$("#valormotivo").text($("#MotivoReunion").text());
    //$("#valorlugar").text($("#Lugar").html());

    //var iframe = tinymce.get('ResumenDetalle').getContent();

    //$("#valorresumen").html(iframe);
    //$("#valorasistentes").html($("#asistentesList").html());

    var filas = document.querySelectorAll("#asignacionesList tr");
    var columnas;
    var html = "";

    if (filas.length !== 0) {
        filas.forEach(function (e) {
            columnas = e.querySelectorAll("td");
           

            html += "<ul><li><b>Actividad:</b> " + columnas[1].textContent + "</li>";
            html += "<li><b>Responsable:</b> " + columnas[0].textContent + "</li>";
            html += "<li><b>Fecha Entrega:</b> " + columnas[4].textContent + "</li></ul>";

           
        });
        $("#FormularioDetalles").append(html);
    }


    //$("#ListaAsisgnaciones").html($("#tableAsignaciones").html());


    var mycustomhtml = $("#bodyelement").html();


    var doc = new jsPDF();

    // para estas imagenes se utilizo un image macker hecho por el mismo plugin desde la pagina de plugin

    var imgData = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABJAAAAJxCAYAAAAdGEg0AAAACXBIWXMAAC4jAAAuIwF4pT92AAAgAElEQVR4nOzdQW8kR5o39qixDmN7320u8MI+GEb3Ar435+Zbc242YKA5ddSlW1/A4gDUWdRZBYj6BOq+6MihPsE0b74N+xNsE4YBr2F4SL8Lv7s7aqaRrSgpVV0VLLIyIyMzfz+A0Iy6xarKyorK+OcTT8yqqgoAAAAAsMlvHBkAAAAAUgRIAAAAACQJkAAAAABIEiABAAAAkCRAAgAAACBJgAQAAABAkgAJAAAAgCQBEgAAAABJAiQAAAAAkgRIAAAAACQJkAAAAABIEiABAAAAkCRAAgAAACBJgAQAAABAkgAJAAAAgCQBEgAAAABJAiQAAAAAkgRIAAAAACQJkAAAAABIEiABAAAAkCRAAgAAACBJgAQAAABAkgAJAAAAgCQBEgAAAABJAiQAAAAAkgRIAAAAACQJkAAAAABIEiABAAAAkCRAAgAAACBJgAQAAABAkgAJAAAAgCQBEgAAAABJAiQAAAAAkgRIAAAAACQJkAAAAABIEiABAAAAkCRAAgAAACBJgAQAAABAkgAJAAAAgCQBEgAAAABJAiQAAAAAkgRIAAAAACQJkAAAAABIEiABAAAAkCRAAgAAACBJgAQAAABAkgAJAAAAgCQBEgAAAABJAiQAAAAAkgRIAAAAACQJkAAAAABIEiABAAAAkCRAAgAAACBJgAQAAABAkgAJAAAAgCQBEgAAAABJAiQAAAAAkgRIAAAAACQJkAAAAABIEiABAAAAkCRAAgAAACBJgAQAAABAkgAJAAAAgCQBEgAAAABJAiQAAAAAkgRIAAAAACQJkAAAAABIEiABAAAAkCRAAgAAACBJgAQAAABAkgAJAAAAgCQBEgAAAABJAiQAAAAAkgRIAAAAACQJkAAAAABIEiABAAAAkCRAAgAAACBJgAQAAABAkgAJAAAAgCQBEgAAAABJAiQAAAAAkgRIAAAAACQJkAAAAABIEiABAAAAkCRAAgAAACBJgAQAAABAkgAJAAAAgCQBEgAAAABJAiQAAAAAkgRIAAAAACQJkAAAAABIEiABAAAAkCRAAgAAACBJgAQAAABAkgAJAAAAgCQBEgAAAABJAiQAAAAAkgRIAAAAACQJkAAAAABIEiABAAAAkCRAAgAAACBJgAQAAABAkgAJAAAAgCQBEgAAAABJAiQAAAAAkgRIAAAAACQJkAAAAABIEiABAAAAkCRAAgAAACBJgAQAAABAkgAJAAAAgCQBEgAAAABJAiQAAAAAkgRIAAAAACQJkAAAAABIEiABAAAAkCRAAgAAACBJgAQAAABAkgAJAAAAgCQBEgAAAABJAiQAAAAAkgRIAAAAACQJkAAAAABIEiABAAAAkCRAAgAAACBJgAQAAABAkgAJAAAAgCQBEgAAAABJAiQAAAAAkgRIAAAAACQJkAAAAABIEiABAAAAkCRAAgAAACBJgAQAAABAkgAJAAAAgCQBEgAAAABJAiQAAAAAkgRIAAAAACQJkAAAAABIEiABAAAAkPSJwwMAcLfZfLEfQtgb4aG6rM6Orwt4HsDIzOaLJyGEJ4W9KmMePNCsqirHjo3ixXI96O87SozUZQhhm4uI6+rs+NJJAHnM5ou9Dd89m/790jYhT/3nT72VD3IVQniX+A/fbfjzdf/euAo92TDGrhs/U2Nu/e8fjfA9fLvh2vDNyv9fHdfeVWfHqfERBk+AxEdm88VhCGH5M8YvBejCpouNdQHVry5AqrPj1QsSGKw1k5LVu8+bJi2+bwhrxtLm+NicrAmf4OMKn9Xx9aDxvwXned3Ea8AQx7Tl//55HHP9xxAJkPjZbL6ov2RehRAeOyrQm+bdfRcc9G4lEGpOVJp3qp/47qBHzXFzGdr/PH4aMxmixpLZ5ri7DISEQePytjFm/fxPYxclEiDxwWy+qIOjF44GDMpFfLJvlgGTiw3uK948CI2JyfKfQiHGpjlmLidpeqGQXSOYX/1n7Zl3hIZlQH65/KdrPfokQEJ4BOPzdjkxqidKlnnQWOJw0LibbdkY/OSmMWaaoNGKxri7DIoOVA7RoquVaz1jFlkIkCZuNl+chhA+n/pxgJG7iXfc659zDR7HLU5aDuKkZd/dbHgwYTx32hDQG3fpw9vG9d4b1ZV0QYA0YXHZwp+nfhxgguq7Vud1zzMTouFrBEaHMTCy7Ay6c9GYnLnjPzGxL9G+gJ6BeBuv985d79EWAdKEzeaLN774YPKWYdKpyqThiLtlLkMjgRH05yKOoSqURkY1JyNz0wiTzr25PJQAaaLiHZS/TP04AL9yEauSXjks5Ymh0fJH7yIoz1VjqbAJ2sDEa+ODRmgknGeshEk8mABpovQ+AhLqC4vTWJVk/XyP4h3wI5VGMDgmaIVrVBgdCOaZsHqseqWtAdsSIE3UbL64tAsEcAdBUk9itdGRJRMwCjeNvnP6JvUoVhm9jKGR62D4tbfxuu/cdR+bCJAmajZfeOOBbQmSMpnNF/XE5kS1EYzWVRxPXxlP87D8F+5tWZWkPyYfESBNkN3XgAeqLyiO9Ehqn+AIJmdZlXRigta+RqXRS6ER7OS1cYomAdIECZCAHV3EIMla+R0JjgATtHbM5ou9GBgdGVOhdcYpPhAgTZAACWjJV9XZ8YmDeX/x7vipHkdAgwnaA8TxtA6NXgzuycPwGKcmToA0QQIkoEV1w8VDFxLbiXfIT+yCCSR8GydoeiQlxOvZE0E89OIrvTGn6TdTPwAA7KTexeYyNiklIU52LoVHwB3qMeKdcXW9+rjM5ot38Wao8Aj68aXrv2lSgTRBKpCAjnxbnR0fObgfm80Xp4Ij4AEuYpXn5O/yqziCYtXj1EvV6NMgQJogARLQoR/iRcTkJzvhp/H2Sdxp6WkBTwcYpps4rp5P8f2L42gdwj8v4OkA69mpdyIsYQOgTfUF/pvY62fSYln3pfAI2FG9Df2fYiXjpMzmi6M4jgqPoGz1OPXdbL44dw04biqQJkgFEpDBpJtrx0nPNwU8FWBcJlHlGauOXlmuBoN0Fa8BL71946MCCYAuLJtr70/t6M7mi1fCI6Ajo6/ybFRvCo9gmB6HEP4ymy9eev/GR4AEQFceTW05WwyPXhTwVIDxejrWsTUu0/tT/P4Ahu27eF3EiFjCNkGWsAGZ1cvZDiaw5EJ4BOQ0mrE1hmGv9DqCUXodG2zbYGUEVCAB0LXR3i1fEh4BPXgad3kctPjd8EZ4BKP1wgYr4yFAAiCHp/Hu8ugIj4AePRvy7myN8MhulTBuo7+ZOBUCJAByeT6bL07GdLSFR0ABPo/tCQZFeASTI0QaAQESADl9GXfYGby4u4jwCCjB+QAnZcIjmB4h0sAJkADI7dVsvngy5KMe7/Z/V8BTAQhx17LBLGWL1ZvCI5im0bY1mAIBEgC5PRryhUO8azb4xrXA6LwYwlK22XxxpHoTJu+5Lf6HSYAEQB+exUnEEJ3HEAygNEX3mYsB1zcFPBWgfy9iOwAGZFZVlfdrYuKX9597fNVvQwjXkzz4lGRfCNC7m/p9qM6O3w3lCccm4F8W8FT4Reo75V38Sdnm73z0e4d03q6KS0jvs4y0Hi9T/So2/Xn9GI93e7Y8wGfV2XFxd/Zj9ealc2LQbuJ7uMnlPa/xr+/4fQ9115i1KlW592zIb9hA/K46O+7iPKADnzio9OCoOjt+48AzFHcsCVh3kbIX//3q3xNY/dpyKdsgdg+azRf7wqNWXTR+2eqkY90k5LI6O3bzoQUx/LpPANbad/aa8Gp1vGz++Z4+OQ9yUugy4RPhUW9Wg/bVz/RHY+7Ar9U7fe5rrgub14LNMc0Ytp16E4B93/HDoAJpggqoQPq9AImpW7n4aF54HDT+3VQCpyLvlq+azReXLgTvtAyFmgHFz+O9sZ+HitUry0nZMmRq/jtVAr9W1LhawLXn2DQDoeW4+qtqHuNtWVbC8+a13p6bjB/8UJ0dj2KX3rETIE2QAAmGo3HBsbzIOBjhhcZVXMpW7J0nS9d+nqw0JyjLcXzQy7kYl3iNs5yQLcfPKYZLb6uz49VK2N7M5ot3qo+29rYx1l43Avlry3zGbc34tT+xG1d/qM6ObVJSOAHSBAmQYPji3fhlmHQwggnSV9XZcZHNX2OIdzniu4NXy8lJY8LyYZJirGYs4ud4vzFmTuGOfxHXWwL4jyz7CC2DoZ/HXUt4WCcuoW9e8401jK0/G098DsomQJogARKMTyNQOow/Q5sYFXvRELeZHcuW029j5dBlnKy4m81kxVDpoPEztklZ70tC4nfTuwkvz7lohEWXQiLaMPKx69vq7Hiou/ROggBpggRIMH6z+aKeNNRboz4f0Ist7qJhBH076mDuPIZG5yYusFmclB3GCdmQxs6Uf+xzienEqo8uVgJ6S3vJIlYoLW8gjmHJm13ZCiZAmiABEkxHnBAdxTBpCHeAe53srJrNF28GujzwdQyM9BKAB4iVM8sJ2ZDDpN6WB4+8+ugmhkUffkx2KUW87nsZf4ZamXRRnR0PYofeKRIgTZAACaYnXsgfDeBOcDG9kAZYfVT3Mjqtt+9WaQTtiePnyziGDm1CdlWdHT/Z4u+1boTVRxfLik6BEUMQr2NeDnQZvvlioQRIEyRAgumKd6ZeFVxVU0wvpAFVH9XB0UlJW3bDWMVrqJOBVSZmXw4youqjH2JoZAkwg9WoSjoa0GdSFVKhfjP1AwAwJfXysPiF/FkMa0rzKF7k9CpOEkufINbv3x/r6gLhEeRR3wCLY+jvY0XKEPTRW26ImzksvY3fkf9QNyGvx1fhEUMWr/3q4LsOkr4q9Ppv1bN4LUZhBEgAExQDh/14oVyaEhpp9x5i3eGHWKl1WvSzhJFaCZKuCn+VfezENsRdlF7Haq19oRFjVJ/TjSDp9QBeYhEtDfg1S9gmyBI2YCkuMzgtcH38H/pqAB1Lvf+pj8feQn3X8EjFEZRlAP1+sl17xR2h/pLjsVryOi4DtmsakxLnhKeF79xmR7bCqEACmLB4N+plgXei+qwAKrX6qK4WOxAeQXniXf3fFVyNlLMKqfQKzqUf4s6fL4VHTFGspKwD328LfvlDrGYcNQESAKHAEOl5rI7qQ4mTn2V45C4cFCp+PktdGixA+sVNrHI9FBzBh7HrKC7HLbE30mGP14OsIUAC4IMYIpXUFDb7JGQ2XxwWuE336xge6ccBhYtVnfsFVnU+jkvLOhXH0JKbZ7+N/eN6WSINpYpLXA8KDMAf9dTHjQ0ESAA0HRa0BKOPu9ilXaS8jcsrhEcwIAUG8iFODsfwGA/1OjbINp7CGrGKssQQaSjLYidBgATAz+KFdSkhytPY0DqLWCJdUoD0NtOED+jGYWETsRzjW6mVAq9jqAckxOvA0kKkZzmvB0kTIAHwK/EO1FeFHJWck5GDgpZe1H0IDt0ph+GKn9+XBfUVedblL49L5EpbAhyER3A/jRCppE0BLGMrhAAJgHVOC7lwyHnRX9LFieauMAIxkD8p5ZXEbbu7UmLF5IXwCO6vUZFeSgCuIrsQAiQAPhIvHEqY9DzNuPtGKQHSt7GZJTAC1dlxKYF86HgSVtoE70rVAjxcDMBL2Ub/eQHPYfKCAAmATaqz41eFTHo6nwDEpRclLF+7KqlaAWhNKVUwUwqQbEAAO4rXgj+UcBw7rqBkSwIkAFJKCDNyXDCUcpf6yIQHxidWFZbQlLaTPkgFhfBLKjmhPUeFLGUTIBVAgATARoVUIU1l6+m6V8d5Ac8D6MZpCce1o7v4JU3sVHJCi2JPxhLGLwFSAQRIANzlVc9H6HGG7Vs73Z1oSyY8MGIxkC/hLv7+QH7nQ52o5ITWnRYwfpVwrTZ5AiQA7tJ3gBS6vOtUyJr6C8stYBLGOp6WEiBdxaAOaFEMZXuvko7LZemRAAmApFi63HfvjrE3fjXhgWkoYRlIFxOwpx38zodQyQndGev4xT0IkADYRt8BR5cXDH0HSDfumMM0FBLI18uC99r6ZQXtjKT6CDoUt/Xvuy+mAKlnAiQAttF32fLTNic8K/peU2/CA9NSwme+zUlY1z3qtlVEk3IYub6vBwVIPRMgAXCnQu6at37RUMhaejuvwbSU0O+szaqhUgIkYTx0T4A0cQIkALbV96RnjDsH3WieDdNSyDKQNkOfEiZ0P9h5DbpXwDXLow4r0tmCAAmAbQmQ2ic8gmka03hawmROJSfkM7qKdLYnQAJgW31PeLpYJtH3RYhJD0xT3+Npm7um9d1HLhhLIavLng93KctmJ0mABMBW4vKAPpdddDFJ6XviowIJpqn3z34hPeDa8NbyNcjqXc+HW4DUIwESAPfR612n2XzR2kVDm7/rga5ic3JgYuJnf/B9kArZwl8QD3mNsaUBWxIgAXAfYypb7vsCpO9jCfTLJKwdAiTIq++bX5po90iABMB99B16tDnhESABfRrDeFrCUhIBEmRUQPW0JWw9EiABcB9juutkBzagT30HSG2MpyUsBdb/CPLrcye2x97v/giQANhadXbc94SnzX4bfZdAq0CCCavOjvsOkUvYPW1X+shBP3oNbmfzhWVsPREgAXBffTd+bUufkyd3zYHQ8138NiZhfU/iVHJCP/oObzXS7okACYD76vOioZULhgJ2YHPXHAgjmIT1PYkTxEM/XMdMlAAJgPvq86LhUUu/p+8AyV1zIIxsZ8s+WAoM06SRdk8ESADcV693nVpa964CCSiBAGk3KpCgH33fCBMg9USABMDQjGHraQESEPQR2U0BGzsATIoACYD7GsMFe9+TJpMeoIQAxE5GAGxNgATAfY1hyUCvkyY7sAENfe5suWuY3uduljc9PjZMWnV23PcStoOJvwW9ESABMDRtXDT0WYF00eNjA+UZw8YEfVDJCZCZAAmA+xpD/54+J02qj4CmXoOQ2XyhGS0AWxEgAXAv1dnxoAOkAiZL7poDTX2HygIk4CEsI50gARIAU2OyBJREI21giPocu1zL9USABMDQ7HrR0Pdkqe/Gk0BZ+q5AGvRW/sAkPfa290OABMDQ7BogmSwBJRlDXzkAJkCABAB56YEE/KyAvnK2wwZgKwIkAKam1wqk6uzYLmzAKs1oASieAAmAhxjyZKfPHkgmicA6fVYmaqINPISejhP0ydQPAAAPUk92njl092b5GlCap94RYEc397jGue7o75KBAAmAqRF8AaUZaihfTxofFfA8gMyqs+OTEMKJ4z4tlrABQD52WwLW6bU32my+eOjuln1WBrgZAJCZAAkA8hEgASV6aIAEwIQIkACYjNl8YbtqoESa0T7ADpVTADyAAAmAh7B04GFs4Q+UaKg7sQmQADISIAFAPnYSAUq0P9B3RYAEkJEACYApGeokCRix6ux4qEvY+n7exnSAjARIAAzNLsvAhrpMA4CPCZAAMhIgATA0Q14GZgkbUKKhBjH68QFkJEACgEyqs2NNtIFNbno8Mg+tzuw9FJ/NF6qQADIRIAEwJSYaQKmGWKFYQih+WMBzAJgEARIAQ7PLJEsPJID2CJAAJkSABMDQDHUZ2FUBzwFgnQdVZ1ZnxyVUTT21jA0gDwESAEMz1ADpXQHPAWCdRzsclT57Ny0dFfAcAEZPgATAvczmi4M+j9iOd7wtYQNK9Wag70wJVUiHs/nC+A7QMQESAEOy653up95tgFaVUF1ZV1CdFPA8AEZNgATAkAxxlyKAMStlee7neiEBdEuABMCQDLmP0FB7NwETsMPy5JKW3r2ylA2gOwIkAO6rzzu8Qw6QVE8BY1TSuFwvUz4t4HkAjJIACYD76vPu7lCbzALcZZAhc3V2/K6QndiWXszmi1dlPBWAcREgATAkD77TPZsvnningYINeZlraeFXHSKdW84G0C4BEgD31VcQcxPvdD+UAAmgGyVWhz6vgy2NtQHaI0AC4L76CmL0EALozi7VOqUuL34cQvjLbL44VY0EsDsBEgD31ddFuP5HAN15cKVOdXZc+vj8eb0EejZfvCzguQAMlgAJgPt62tMRG3qANOQd5ADuclH4EXoUQvhuNl8IkgAeSIAEwNb6bEQ9gDvcdxEgAWN2PpDX9rgRJJ1Y2gawPQESAPfRV4BU+p1tgJ2MICQfSoC0VAdJX4YQ/lpv+z+bLw7KeFoA5RIgAXAffe1mo/8RQMHiLplvB/oevQgh/DlWJR31WW0LUDIBEgD3MeQAyVbOAN16NfDjW1clfRNC+KfZfHEpTAL4NQESAPfRR4n/TUtLO/S5AOjW0JaxpTxdCZNOLHMDpu6TqR8AALYzmy/2493Z3MY0IQEYrXoZ22y++CGE8Hxkr/Fp/PlyNl+E2JevvrFxWf+zOju+7v8pAnRPgATAtvra9liABNC9tqprTkcYIK16Fn8+mM0XN8swKe64+W4ETdEBPiJAAmBbhz0cqXr5mgAJYCDq4GQ2X1w0A5YJeLQmVKr/sQyW3q38jGHXPWCCBEgA3Gk2X7y0fA2ALZ3Uu5o5WB8HS0sxYAqNkGlpNViq/2zTErnr6uz4csOfAbROgATANk56OkoCJICBmWgV0kM9WjlO9zpmjSAKVq2Gk2EloFz+70t9vNiWAAmApHrnmZ6qjyxfAxiuoxDCX7x/0JvVcDKs/P8vl/+j0Rz+utHPS7DER2zjD8BGcee1o56O0KuRvTOWGQCTEZdWfesdh8F4FhvgfxmXoP51Nl9czuaL09l80UcfTAqkAgmAtWbzxV4McR71dIROx/TOuIsHTNBJ3N3tqTcfBulp/Pk87jZYV4afqxCfLhVIAGxy3uNF/0V1dvzOOwNMzNsxvdwYnL+MvViAYatvKL4IIfxpNl9cx8qkfe/ptAiQAPiVuvJoNl+c99z8tK+m3QB9Gl2lYlzK1tdSaKAbdZj0ed3nLC5zO4qV64ycAAmAn8Uv/zdxDXxfruodfLwrAONQnR3Xy6E/83bCKNXV6t/EnkmvVCWNmwAJgA9ig8R3BfSqUH0EMDJCJJiEF7Eq6Y3G2+MkQAKYuNl88SQuWftTjw2zl67iJAOAkWmESHoiwbg9i72S3s3mi5fe6/EQIAFMVAyOTuL28n0uWWtSfQQwYjFEqndmu/I+w+g9DiF8J0gaD9v4A0zAbL44iK+y/ueTEMJ+gdsqqz4CmIC6sXbsk/KqoBsYQHeWQVJ9o/ClXpfDJUACGLB4Ab7c9WIZEj2JP6HnndTuS/URwETELf4P692b4vjf9xJqoHt1kPTn2XxxEYOkd475sAiQAArSqBQKsUpoGQ41Q6En8Qt4TC5UHwFMT3V2fBr78L0a2E0P4OHqz/o/zeaLr6qzYzcQB0SABNCBur9QI/AJjeqgsBIM7RW4lKwPLh4AJipWIRzEXZtOR3iTBFjvy/i5r6uRLh2j8gmQABJWlojtxfBnqflnwZ3TB3ttLTwA1dnxeb39dwjhKP5Y1gbj9zRu/a8aaQAESMBkbLk8TEVQXjdxkgAAy95IJ7P54jR+P3zpqMAkfBmv1Q/jOECBBEjA4DWqhJYBUTMcUhVUtiMXCQCsagRJr+Iy50MVSTB69XV7vUvjoSVtZRIgAYMymy/24kXkQaFb0bM9jbMBSIr9kV7G739L22D86h5ob2bzRd0X6dz7XRYBElC8Rmh0JDAajZv4ngLAnVaWth3GqiTNtmGc6pD4T7P54jM3G8siQAKK5W7jqL20dA2A+4rfHfWE8lXsl1JfIzx3IGGUvpvNF0GIVI7fTP0AAGWKW3q+i80zhUfj8q2SZAB2Ve/gWZ0d19cL/xhC+CqEcOWgwuh8F+cFFECABBQnNsz8k+BolN5WZ8d2XQOgNXWfpHr77+rsuN5R9Q8hhNdxqTQwDq/ipjn0TIAEFGU2X9Q7LrzwroySvkcAdKqucK3Ojl+GEOow6bMQwg+OOAxefVP5PLa3oEd6IAHFiJVHmmSP10HcTQcAOrXSK2m5GcehfkkwWI/jZ9rNyB6pQAKKEBthqjwar3oXjcupHwQA8qvDpLoJb+yX9A8qk2Cwns/mC60QeiRAAkpx6p0YLVuwAmznmePUrQ1hkp5JMBwns/niiferH5awAb2LTfEsXRun18IjAErUXOYWfrkeqYOlA2EeFOtRvPFsKVsPBEhACQ68C6P0OjYyJS7TrLecdiwAyhSXWv+83Dour69/9uPPY28dFOG566p+CJCAEthRYXyERwAMWpyc/jxBjctmnjRCpScqqKE3J25C5ydAAqBtf6zOjvW0AmBU4k6i75qhUvhl6dtenMzuNcIlFUvQnWeqkPITIAHQJg2zAZiUxi6jH01kZ/PFMlAKjWqJ5r8L+i2xg5vmsstoSufT0brPHd0RIAHQlnUXMQAwWbFR93KCe+dEdyVwatq35H8S1p0j142Q8l4a59Pyn8vll49GcjDrXkhPYnUgGQiQgBJcexdGob4YeRPLiQVJAMPiu7gAK4FTkyoL7m3lfDpf/vdx2eXLuJPZ0JdaHsUfMviNgwwU4NybMBrLEMldUoBhEfzDRNQ3+qqz46Pq7Lju1fW7EMK3IYSrgb562/lnJEACehfLTr/1ToxGqSGSu7cAAA0rYdIfQgg/DOz4PI4VVWQgQAJKUW/F+da7MRpPVSIBAAxHdXZ8Xp0d1xU9/xhCeD2gp/6ygOcwCQIkoAhxjXbd2O/COzIaQiQAgIGpVwdUZ8cvBxQkWcaWiQAJKEYdIlVnx3WI9FXc0YvhEyIBbKHegMBxAkrSCJJ+V/hKAcvYMhEgAcWpzo7r5WxPYpA01IZ+/OKp/kMAAMMU+yTtx2vzUgnhMxAgAUWK1Ugnjd0hvrK8bdCezuaLV1M/CAAAQxVv8v6u0JUCAqQMPhn9KwQGr77r0dxeOJao1sHSslR19Quj/rPH3vnivJjNFyGWQgMAMDD1dXm8Fj+PVealECBlIEACBqcRKJ2nnvtsvngSw6TQ+FJp/rv9uOU8+dQh0pvq7HiK1UgHlvIBBTM+AVupeyPFvm1vCmdrlG0AACAASURBVAqRHtXX/vVzK+C5jJYACRit+AWy/BJJXhjHOynLRs97jeqmsBI6BRVOO/tuNl/USxSTASAAAGWq203M5ouX8Rq7lBuy+41rfzogQAL4paqpaetwI+4wtrrzQzOQuuvfTbEK6lV952rNcQcAYADicrY6RPpTIc92/z7X8NyfAAlgR/UdmDUVTg9eCrCy9G5pNZBq/p29wtagb+NRI0S6zvGA1dnxm7oHE0Ch9O8ABqeuKJ/NF6/rNgUFPHfjaMcESACFWVl6t3RnINWohDqIP88Kf2/r0KvuhXRYwHMBAOBhjuL1XN9V9as3YGmZbfwBRqKu5KmrbOotVquz4zpA+ocQwmchhB8KfoXPY+kzAAADFKvJTwt45nqUdkyABDBSMVB6VZ0dH8Yw6Y8hhKsCX+1pXLYHQE/qGxCOPbCDInbYjRvj0BEBEsAExDDptDo7fhKrkm4KetWPSrno6NhqA3UAgFGILRhKqHp3vdUhARLAxNRVSXGN+LcFvfJnE1jK5o4YkGKMAIauhBuCxtIOCZAAJihWJNUND39fUDXSaWwE3qWSKq8Amtw1B4auhKWwxtIOCZAAJiz2vKjv1Lwt4CjUS9lOOn6My45/P8AQldgfDxiY2Ez7oudnra9mhwRIABMX16wfFBIifa6hNkB27xxyoCV9VyG5juyQAAmA5R2jg0LuQnddhQRQIpMeYAzs6DhiAiQAPogh0mEBR+MwQy+kPpgcAimPHR1gBPquaHS91SEBEgA/q86O6x5BX/V8ROpeSGPckc3kECiVJWxAK2JrhD653uqQAAmAX6nOjk8KWMp21NHvVVYN8DEBEtCmvhtp0xEBEgDr9N2H6PFsvtjv+TkAZDGbLw4caQBK94l3CIBV1dnxq9l8cRqXk/Xl0Lb7AOWKwdefNzzBq44qm941fu+b6uxYZSmUp/6MPvO+jI8ACYBNXtXb6vd4dA7HtiNbPdky2QEK1MW49LijXiTNSanxFMrU67LYuoo99vWkZZawAbBJ3xfmTzv4nS4mgBINdQnbGHfMBIbP2NQRARIAa1Vnx+d9H5kO+oJct/z7AKZMrzqACREgAZDS9y4aY5ucuCMGrNPr2GBpLQDbECABkNL31s5jC1zcrQfWMTYAUDwBEgApfQdIT9r8Ze6yA3zkZsCHpO/vKIBJESABkNJ3z6BWA6QCWMIGrNNnBdJgNxeozo4FSAAZCZAASLFrWbssUwHWeeSoAFA6ARIAU/PWOw6UYjZf9F1p6UYBAFsRIAFQsmcdPLc+l+WNbUkesLu+x4W+lyoDMBACJABS3Jlu1+MxvRigFX33RhMgAWNjXOuIAAmAjaqz4zF+Afe6E9tsvtBIG2jquzeaGwVA23qtrKzOjo1rHREgAUBeGmkDTZawAWNjyf5ICZAAmBrbPgMlcacegEEQIAFQtNl8cdDy8+s7QGr79QDD1mdV4o1zB+iA5fojJUAC4C5j2/a+7+UaLqqApkc9Ho1BVx/pKQfFetrjExOMd0iABMBdRtUfo4DlGnogAR90UGF5X0Nf0ms8BVZZltshARIAU9Tn3SkTHmCp70azesIBrZrNF65zRkyABMAU9Xl36pFlF0BkC39gbPq+xrGzZIcESABMUd933d2dA0IBY8GuEy0BFLCq76W5xqUOCZAAmCI7sQEleNbnc6jOjt/s+CtsSgCs8rkcMQESAFOkkTbQqwL6hFz1/PhtMJZCefr+XO4ajJMgQALgLmNcS64CCeibHdiAMeq1spJuCZAAuEvf1Tqtl0IXsJX/I7uUwOT1HSC1cZd+dN8PwMOVcG3TwtJcEgRIAJSuq4uRtz2/blVIMG2DbzRbnR33XaEqiIey+EyOnAAJgKnq+865AAkmKt6lf9Tzq7eEDWhb39c2Fz0//ugJkACYKgES0JfeP/8tLuXts5pTrxUoi95uIydAAmCqSuiDdDjdww+T9rLnF9/mXfpel7HN5gt9kKAAs/niSQjhcc/PRIDUMQESAJNUSJNFARJMTAw8nvb8qtsM0PVBAkIh1zQaaHdMgATAlPXdSFuABNNTwue+zQCp72pOARKUoe/KylDAeDR6AiQApswyNiC3sU2yVCDBxMXla31XVl4VsDPk6AmQAJiyEkqdS5hMAhnESVbfjZ9vWmygHWxIAIywspINBEgATFkJAdLzOKkExq+EwLjtca/vprWPjaHQu6MCnoP+RxkIkACYrOrsuJ743BTw+lUhwTSMLkCK42jfLAWGnszmi4MCdl8LAqQ8BEgATF0JFxxHtqKGcZvNFy9HPMm66OB33ocQHvpzUsCxb3tpLhsIkAC4y9i/kM8LeA6PCin/BrpTwme8q0lW31VITy1jg/xi9VHffd2C6qN8BEgA3GXsO1qUctGhCglGKk6y+t6hKHQ43pVwo0EID/mVUH0UCrkZOAkCJAAmLfbvuCrgGDwq6EIMaNdpIcdzzAHSSyE85BOX5ZZQfRRUIOUjQAKAcu5cfT6bL/YLeB5AS+Ikq4Tqo9DVWFedHZcwebMUGDKJS0ZLCcbfFtLMfxIESABQ1p2rVwU8B6AFsSKmlEnWVceTrLcd/u5tWQoMHYufsfMY2pbAdVNGAiQAJq86Oz4vZDv/EJvBWsoG4/CqoElW15WWpVQhmUxCt04LqqoM+h/lJUACgJ+UdAHyZWy6CwxUXLr2vKBnP4UAqfZ8Nl8cFvA8YHRm80Ud0L4o6HVZvpaZAAkAflLaHaxz/ZBgmOJnt5SlayFu3991wFPUUmDb+kN76mVrBYZHQcVhfgIkAChvGVtYLsXQzwOGpcD+ICFHQF6dHV+HEC66fpwtPYohvPETdhTD2DcFhkdBgJSfAAkAflFaFVLdY+CNSRAMQ/ys1hOtx4U94VyTrJLG0Kcml7CbuBz0srCeR0uvY3BNRgIkAPhFiZONZYhkORsULH5GS5xoXWXcZr+0EP55XHYD3ENddTSbL+rP858Kq6Zs8tnugQAJAKI4yboq8HgsQySNtaFA8S59iZVHIWeoE5vZlrCdf9OL2XxxqZIT7hZ7HZ3EMLykTQBWvc0YjNMgQAKAXyv1jlZ9B/DPtviHcjQay5Z8lz53M+9SKzkvVXLCZnHnyDo4+rLg8WyppE0KJkWABAC/VvpFSb3F/zvVSNCvONl6V2hj2aWLHra4LjWEr6vD/iKEh18sK47q64oQwneFVlGuqpflWr7WEwESADTEhoyvCz8mj2M1kmVtkFn9mauXRMXJVul36bNPsgYwhn4Zl7QZO5msOI7V48NfY8XREIKjJSFwjz6Z7CsHgM1eFV5VsPQsBkn11tmn1dlxaQ1sYTRixdFRobsRrdPnXfrSx9CnjbHzRC8VpiAu4azHscOBBUZNqo96JkACgBX1ZCJOLJ4N5NjUz/PZbL64ig1zX1Vnx5cFPC8YtHonohgavRxAtdGq3iZZcQx9O4CwbRnCX8Xly69sC86YxEq7w4GHRk1H5TyVaRIgAcB6dYn0nwd2bOqLw8/rn0aYdO7uOmwv3qU/iKHRUKqNVt0U0M/tNC7zG4J67Pym/pnNFz/EsfNND/2jYCeN8Wv5M7TgO+VCpXX/BEgAsMYAq5BWNcOk+o8u4jbjbwRK8Iu4vfvyLv3BSO7Sn/ZdSVMvM4kNq4d2PJ8vty+PVVRvGmOn6iSKECuL6rFrv/HP/ZEFRqtUHxVAgAQAmw2xCmmTZ/HnyxgovY3b9b6Lk6NLkyOmIE689hs/Q60y2qSE6qOlkwFVIa3zNP7UYfwyULpc/gjj6UqsJFoNiPYmEBJt8pWl+WUQIAHABiOoQkp52pg41zuwhEal0nUjXKp/rl24MTQrd+jrnycjDIvW6b36aClWIQ2p8fhdluPmhwbhjTD+XSNYuhYscZfGLoDLf049IEq5KigUnzwBEgCk1ZOfv0zoGC3DsufNfxknSiFOlpaT02XQ1HTZ+PMlARStatydX064wspEbKoTsJKqj5aORlTJuc4yVPp5zIzj5U1jPGz+MwiYxikuh12OR83/vRyvnoxkiWxuL1VIl0OABAAJdfAxmy9eD2Rb/xxWKwmeb/OYjQCq6WZNALW0rH66758Fy/GGo3EXfmk50Vo6WPkzd+bTjko792Ml5xTH0EdbBPJXjbGsGb7/aowTOHWvEUo3PYk/Tat/TyjUra+c/2URIAHA3Y5ig12T13Y9SiwP3GnZ4IbAKtwRWi2tq6Ja564gK/kYfU30N0yUttG8o77Jpt+9N5HlY32qdyjqbev+OxhD13vcCB82jnlrxrOLNX9t0yT7PuNUtnFpTXi8rW3Gr9TfEUQPRz2mnUz9IJRGgAQAd6gvqGMfjyE3g+UnqdBqKUvPq0TIBQ9R7A5FcQx9GUL4UwFPZwzWjVGtjFvGJQrxNobOFOY33hAAuFu8s7/uri9A34rfoag6Oz4PIbwu4KkAZbvR96hcAiQA2N7LeGEDUIq3A1rmcRQrCwDWqa+xDmy8US4BEgBsqTo7fhdDJIAS3AxpTIoVBYJ4YJMj4VHZBEgAcA9xGca3jhlQgMFNtuLzFcQDqz4reCMAIgESANxTdXZsGQbQt9dDnWzFIP6zAp4KUAbh0UAIkADgYQ6ESEBP6u2tB13FEyeLmmrDtNXLWX8nPBoOARIAPIBeHkBPRrO9dQzBhEgwTRpmD5AACQAeKF70HAiRgEzexgnXaLa3FiLBJNVj2RPh0fAIkABgB0IkIJPRhUdLQiSYlLp/2/4Yx7IpECABwI6ESEDHLsYaHi3FEEljbRivm9gs2y6MAyZAAoAWNEIkjbWBNtV360cdHi3FRrp/EMbD6NTXRvuaZQ+fAAkAWtIIkS4cU6AFk7tbH7f4F8bDeHwVl6y9854OnwAJAFpUVwnU1QL1BZPjCjzQ2ylvbd0I438o4OkAD1PfTPvH6uz4xPEbj0+mfgAm6rrnu+MapsGw9D1mDPKOVX3BNJsv3oQQ6rvpjwp4SsAwfBtCOJl6g9n4+g9n88VRfTyMozAYVyGEo1hNyMjMqqryngJAR2bzxV4I4TSE8MIxBhLqqqOXtrX+2Gy+eBJCqKuxnpX23ICf3cTrndOpB+BjJkACgAxm88VBnAA9dryBhptYcXTqoKSpRoIiCY4mRIAEABmZAAGRSdcDqOqEYhjDJkiABACZxQnQUfwRJMG0mHS1IFZ1nljWBtldxTHslTFsegRIANCTGCS9jEGSpW0wbldxGavgqEWCJMjmIo5fmmNPmAAJAAowmy9exjDJJAjG5SLeqZ/klvy5CJKgE8vgux7DBrkrLu0SIAFAQeJuQ3VF0qGqJBisetJ1Hu/Wm3RlNJsv9htjqCXCcH/L8euVXSFZJUACgELFidBLYRIMwk2cdJ1b4tG/uET4MIZJT6d+POAOQiO2IkACgAGIYdJh/DEZgjLUk643QqOyxcrOwxjIGz/hp8D7TQyN3qiUZFsCJAAYmMad9YP4ozoJ8rloTLrcqR+YRph0qF8SE1KH3ZcxNDJ28WACJAAYuDgh2o9h0r5JEbTmZmXS9cahHY8Yxh80flQnMQZ1WPQujlv1+HWpwoi2CJAAYITikrf650kjWNJQFtLeNgKjS3fpp2UlUBLGU7JlSLT8qceqayE3XRMgAcBExMnRMlRaVi3tmSQxQXVQdB2DonfCIjaZzRfLMGk5dhov6doyHApxjArNsEg1EX0SIAEAH8SJUmgES8vAaWnPEg8GYN3k6zIGRnVQdO1NZBexwvNJI1gSxJOyXAq79GbN/74WYjMEAiQAYCeN4KlpOalate7vBpMvNlhWCi1dNv7/dWNSZvJF7xpVnqv/DMa4wWsG06FREbTp/wurGSUBEgBQrHinf10QFdZUSG2y7d9bZcK32eod9XVWJ1RNlyvBUBACMQVx04Mn8aU2x7d145TedQ+TGp/W9QhaNx4JgGANARIAQIfuCMF6peEqDM+WY0qx484dth2TBDzQAwESAAAAAEm/cXgAAAAASBEgAQAAAJAkQAIAAAAgSYAEAAAAQJIACQAAAIAkARIAAAAASQIkAAAAAJIESAAAAAAkCZAAAAAASBIgAQAAAJAkQAIAAAAg6ROHZ9L+pxDC/5jjAPz4/m97VVVdZ3io397e/vsnIVT/0v1DVXs//vjvOV5TqKrb376//fFfczzWbfXjb2+r93ke6/bHR1X1/ibHY72//XGvChnOwao+L/4123lxW+U5L6rq/d/d3v6Y4XNVnxfvf3tb3WY6B/99rwq3Gd6vau/H93/Lcl6EUP3H9+//7f/O8kgfxqa/ZRqb3v+2yjU2vf/3varKcV6ET25v//Z3IcfYVJ+Dt/m+s27fZzovbn/MNg7eVu///jbc/r9ZHus213VTtff+9t9ynRd/f3v771mOX1Xd/of31d/+U47Hqr+zco1N7+vzItt31r/9SwjVj3keK8/340/XTbm+s25/W2W6lnlfVf+xCiHL9/7t7f+zF0Ir79d1/Fn6P0MIy+P1r198ev2/tfAYdECANG3/cwjhf81zBGb1sJ3pYOd6nNl/DiH8l3keanYVQnic5bFCzsfK9V5lNJtle6xZfV5Uud6rkPG8CH8NIfxDlkfKNjTlOy8yv1f/ewjhv8/xQLMQrqp8ryujbN9Z2V7SLOf3SMZxsMr6nZVrcMr8nZXpvKhCle2xZrPwz1UV/tscj5X3Oyvb+f7PIeQ5frPZ7P8IVfjvsjzWaL+zMmSKP8l64cT2LGGbtv9h6gdgF1VV/V8ZH8xndUf5Lvyrv2Z6oNGqQpUnPArjzDBzqkJ1m+vhqhD+vogXPVBxQp1FFXxn7arK99H6z7keaKyqqsoTHo31+P1SddL9Y1W32dKPelVExsfKyIXT1PmCn7b/Ot+rN9jsIuckLYTqv8n3WONTVSFLeX746aLVGL6jKmT7aAkWd5YxWGQ3GdOPUFWCxZ3kuxnmO2t3gsXhqIJgkXEykJNFVWWr/vhn7+huqlDlWZaX9bwYq4zVH5VgcRdVqASLA5JxkiZY3FEVbgWLg5H1ZphgcQdVxmAxVOaju8p3NV0JFvGBZXSylcGGcKuH2M4Ei8MhWByOrNUfgsWd5AsWLYXeXcYeSILFHeVcCu07azdVuM24FNp31m5u8wWLFMvFBDxQVeXrIldV1Qib8OWTc319qLIGiyNdX5+LYHFXeSsWMxYvjFDeflUmabtRsTgsgsXhyPmdNYLDBWv40qBzVZWvcWfQuHNnJmm7qbJsefsT6+t3ljFYrDIGi5VgcSdZg8VxVrJWKhaHI+dS6Fs3w3aQcyl0qG5dT+9MsMg4GRwYlSpr+mF9/W5yBosjnaRlJFjcTdZgsboVLO6isiPQsIxyKfRIv7MydmrxnbWTrBWLqux3UoWcm7f8zfU0AiR4qKrKuSOQO6y7qKp8k3c7Au3GVuPDYpI2HFUQLO5ivFuNq1jcjWBxSHxn7SrrTQ8K5eKZDEzSdpWxRF8Z7I6qKuOOQJZu7CbrVuNBsLgTweKQ2Gp8OASLO1OxyBqq7BkvF0l0LvOyMo07d2Cr8WGxI9Bw5NwRiB3lvUUtWNxB1q3GXTPvTLA4HFX1XrA4EFXIt6mPYJHgy5Dxyde407KyXQkWhyNn407fS7vKOEkTLO4sY7CoYnEneXvCCBZ3kTNYrHxn7UywOCT/JljEoEcWo0yr8y0rs9X4rvJuNW6Stou8W43fChZ3oGJxWFQsDkm+YNF31q5ubd4yGBmDRXNsRszJTeduK1uN7yJn485Q3eZcR21HoN0eR7C4s5zBosadu7HV+GBUOSsWBYu7EywORdal0ILF3WTtsajKnrx88TIqJmm7ybmO2lbjO8vZuFOwOBh2BNqdrcaHwlbjQyJYHBIVi8NRhZDxZtj7ERwxdqVr+7T9L/Ec2Gschd+uTOD+bvfzpKp/x790f6RzPc6Hi/7/KoTw/+V5rOo/hBD+U47HCqHK9rpyvV9VqP6uqqpc58WjEMJNnsfK917lfazbXJ/j/yJUHy66spyDoxybbt/ne6yQcxwMY/zOynb8cj5WyHi+57yWqarbuldLhpnaSMemkPUaLdv3fqjyXTeN8by4rd7nGwdDyDg25frO+vHvQnif5bzIeZOU+5tZ+wwAAABAihJPAAAAAJIESAAAAAAkCZAAAAAASBIgAQAAAJAkQAIAAAAgSYAEAAAAQJIACQAAAIAkARIAAAAASQIkAAAAAJI+cXgAAMjt6+/39kMI9c+TLR/6OoRw+cWn12+8WQCQ36yqKocdAIAsvv5+72UI4SSE8PiBj3cTQjj94tPrE+8YAOQjQAIAIIuvv987CiF809JjffvFp9dH3jkAyEMPJAAAcmmzaujzr7/f23b5GwCwIwESAACd+/r7vYMQwqOWH+fAOwcAeWiiDcCDxbv/O1cADL0pbmwGvNfm79QouBttnbNrXH/x6fXlLr/g6+/39mJT6V29++LT63cdvMauXIQQtjnfv1zz74quQGrxfNvp/OpijIqGdq5tLQaeu9p5XKBfLZ0H69QbAlx7exkaPZAAuJevv987jMtQnrZ85OrGuOchhKO7Lqq+/n6vnmw+y/DO/X5TkBMbAR91cByatjom8QL3z+v+7ItPr2e7PIGvv9872TBx/yrVxDjDe7Q8Nid3TWDj5Ll+rs87fD5LVyGEV9s2eG48ty6qc36IzaaLCCMT52nyXGr89+suWrf6b3PqcIwM9zm/4mf35Q7Nyu/jh/hZTIYla97D+vUcthWyJMarjWN547/di2N6F8fsbXzfTu/6ixvO8y7H8osvPr3eGJIkxvJWP3ubXnfHPnoNHZ8Hq7b6Hkt9x7doq3M0cT7c+Rnbxtff751v+K6ux4p9oVv/LGEDYGvxAvRPHU2M6snzi/qOdryAK9bX3+/VF1jfdRweNY/Jm9KPSQ+Wx+YydWxiBcibTOFRiBOOL+M5khTDo+Vzazs8CvH3/jkGGiU7GMv5HZuEdzVGhsb5dX7H8ziNYUGO8CjEc+3NA3pS1c/vLzGQ79ubDo9ZfT588/X3e68KeJ2knWf87Gz1PZZJ7+doDMo2fVc/jsEePRMgAXAfOb68H5V8kRAnSJ9nftin8W4oH3t0R2Pmk47Cmbts0+D5KNNzuzPMymTTHfZnDwgf3saql9KWeuaqhnoeA8iP9DRGhS0+iynf9TxxfZnhhkDthcbv5YoBRo7q5lWPCvqOf7FpbMngrjHgS5+f/gmQANhKRw1wNym5MW5fF1aaBW+Wek/6PG53nSu5KoMel1DhE5doXG3446fxLnzq/fpDCOF39VKeLz69rpcyHJbUKyzzGBkS509fY1TY8fNWT1wve5og5jxmpVcETlmf3xclnRfZn0uscN+m6ksVX8800QZgW5smoK8TlQXbaPOO37bNeLe17nVtmmhctXRh8ySWtK8ayxK2P9ZBwT3/m2Vz6cMHVAlsuiD96p6/J2VTr4z9uBxik3Vhw67n8KbzZ7+Qap2TuPxznUdxyd1nX3x6/dFn6YtPr5PLtgq269hw3zGy6zFqaV0/nV2X/SyDxNzh4Lpj1sbxWjc2WI683n3G5E2fifu+Z6vn2Kbwss1ri01j9EP9/oHPYT+en31U6P5KDI23rTx/FseHoX4fDJ4ACYBtbZqUvNrlQj/edVq9EHxooPSmx4a679p47FjF0ObFZWkuH3i+nMe+L39p4/W03Pz1oMV+GTudw6WfP3Uw9PX3H+bPm0KkEJczHXzx6fVYlm3uNDZsGCOzP49VX3+/ty5AasMySOy7QfrOx6vlsWHU7nOsE5+JXd+zTQFSa9cWbY/RO15/XW8IgnM7XRNkXcWAa13z8NO6mbeG2v2whA2AEryNlUx/XC5R8a58uHi6iHdlP9M88sOF8mU8V0i7if2Bvop3p/+hoJ3Y9rZcLlQvZ9I8fto+NAsfwTlQj+PfxiWY/1jaroFMWu/fC4nG2Sfxe+tizZ9pqN0jFUgA9CpeTLugjuIFkwBtM3ccE+L5U+SEOzYqXneneZNlc+2X99nmPT7OfpyAOF+G7flDzoFSpLbHBz5Yt+TwqrGM+WhD5XEdML+KvfXISAUSADAkKlIGpt7Rp64misvW7ttv42kMELaaiMedvL6Lu5CNqYLpqlHJsqwsG0sz2as7KguX54CdKGFEvv5+72jDEs+fP+sxOH694VVrqN0DFUgAbGvTXZ7TuI5+V5exz8AujREPYm+ErVhKMCyx0WaOrbb79HLbsOQO7+Jn6lVfVTgxvDnZYkv5ixgMbnpvk821G491vtIX5emQq1eWJlCl+S42yH+1YSlLiOdA3Rur3n0v19KVZfC5q+v4WTwf8nnIKPVWIdf4flh1sWbJ9VEcI1ZvQGio3QMBEgDb2hQgtTWhryd+n3/9/V59J/rggZPeZ/dsNpt1UhYnIw9thnsxkuUQ+7GJ8n0d9LzFchdu1lwQP26p6e6z2Kj1JDakzjpxrS/qYyCQqjiqX/9RbKy9LgBatba5dh0qxMdaNxYtQ6QnlrOVK743h/EGQKqp7+cxYH3od8Qml2vOvUct7hD6PC65+TZjAMYEPPCGw7IXXZ/n4qblzB9dl9Wf9a+/3zvdMDZoqJ2ZAAmArdR3hL7+fm/dhLdtT+OFheUK4/TN1A9Aw5tExUVbHsXHybaUKy41Su2yFuKShKPlRX/850FcgpbaoehFrEQ7jJOKgxg8bRqX6jHrpcnFMNTVVl9/v3d5R/hYf0e8azkYzRWw1gHYeSlN7RmFdbuUFS2xE93rxGdjeV24eoPlcQydBLOZ6IEEwH2cZjpah94V7mGoy0JOYsDRtUctLYu7U6wkSo0Tdb+b39dVROtCnVhd9Nkdj7Nsrn0SJ0+bgoarWKliecOAxPfr4I6+SPV7/pfYQ2VncWlkrh0e3RyhdF2PmZu+IzZWhcfvi01//nmsRCUDARIAW4u9OD7LcKH96IHNb5eNZrf9YfhuMgabrYrVEwdxy/2u5Vr+t58IdL764tPrp2lcYAAAClNJREFUJ3dVX8TJ/O/vCNee3rHUqR6j9vWcGaZ7fDa+qavWWmqWfhAr47oOdZ90/PthFz90OW7G0HfdcuPXd+2oFr8bNl27DfI6YIgsYQPgXuIX+IdGti1VNbzcUMq8H5fe3Merwhtjv9riNT25YwkPP7mJd0lPhryNb7xQ/1BxF5dmtTG57HNJw6Yx4av7fDbjktmDRG+jlNerfZKGLC4JPIhjx+VUQrF79EV6EXur7fSex8f78DtiINVGRcPpBBr/Mw5XXV9DJRpnh3s0rd/0HfksbphgZ7aOCZAAeLA2+jjkWlpTgm0ubBK9Acbij2uWnO3d0fPk7Up/g8sx9rSJQdjOYdgDm5QXpw5KGj2Otm1m/McvPr0e253oZaj8YVyI7299F/50Csvz7tEX6U1by1nj+NLG95veW3Tt92t+//4d/QZfN7fAz9iT6+SOz/CuTmOPMZ+7DgmQALi3ZaWERqA8wOW68yaeU28SO2mddrDzUhEa1Q7vhlxN1YVGc+1tdjB825wUjdyz+HmZRH+nOijboiKtlV3Tlr1ULH9kCDZch72J4eWmnc7qMPo6546A8XP1eccP80hD7e7pgQTAneoL93oL1XoSFy9K/mmIO39QrmVQkOiv9fPOS2N4G+tlOfHzVAdGf42fJ81114g9M7YJBn7esj/7k6RzXfUMq5e9xD5KdcVbVTfn1k+FoYsVzweJnl6fx++gXCWruT5TGmp3TAUSANs4yHDniIlrbMt+umEZX3138c9ff783hmVKqZ4uY/TkgeHfph5pm9Qh0mXLW7xTiHv0RbqPl21ULkFpGsuA32yoRHoWx8vDjhtn5/6MnWbcOGJyBEgAbGPtsqF4Eb+rTV/yD1mqdNDSc1p6d4+GjNs2gLxLq3cDWzgeWS/Clo1sY5+XTcHBN/EO49FDl7S19F4ttXW3s+3ztw9vNkzsX2Ts7bXc4v2zQhqq7jo2tFVR1dYY1bvYF2m5hG9TT5VdPGnps9haNVyBY3mb49WbQpbEv9sQdLxssfo1a5O6GCLtx8/KuuWfj2Pl5lEX42WscNp0w+f3u7zviSBZQ+0OCZAA2MamO1OdVVE88G7Ys5bvcl2s6amyqUdNK/03ElJBSapvziArXepdtOJyyU2Vb8udlw7v6Bt0k7jz2rX7nsNtn79NuXor5XqcP8bKkVTj1e/qiVPOPh8bdD02rOprjApxJ6cs4k59qYnxttaFBo8LHDu7ej4P7SvX9nhVSoC0zuP4M0j1d2SjEmndZ+VRh+PlpsbZr1sIDU/j98C690ZD7Y7ogQTANnIvBdnUB6cEfV3kbnzcGKBkm7hFnR+HeCH7WeKv/LxcKfF3+mwynPrctNrHZQtZztt4Ll50/DCfxSWMB1s81uexv03nd/3jZGhTv5EubDq3+5yIZ33seL4dxF2lHir38bprTOr687PKZhi/6HPZa6ffVY0+g6nvnlb7IiUaZ98ktvPfWnxNm37PozYeg48JkAC4U/yS/jbjkSr2Sz9OWHIeixAvtu4qxc7ZE+htruUGsQQ9FSIt+yJtumvaV6+kb++ojMr5vO56Lm076ihIqUPS3y2XJdTj0hefXm8THryISzRyLB3J9b5ebKrS7GmMWsr+eYvnwctYlfYQ5xlvWpQ2ll9NaOfCO9W7/fV0A2ub82Jn8bNyeMeY+SxuWNHG0uxN5/JpW99J8ftgU+iqoXYHBEgAbCVWg3zVcaVLfeH2h3gRV6xMx2KpvtDbv6sMO1Zk/DHDc3rdQ1+k+gLxd3eEEt+sqzSJk+zfZ7yrXx//r+5aBhADuK6f11bPpW3xmO/Hc6WNIOkmvo4n60KTGB6kQsbQ2MWv08lE3Zun48/hTRx7Du94HjnHqBDP49/12bg8joG/v+8516jMaOt83WTbsbz+/vtDhveurkQ5sMTnIznOhaYftjkv2hTHzFSItOwj9+CdQevl5RuWOF51EJKmbjraUbFls6qqRvWCAOhenKS3PRG73PYCKk4Cc1QTXN81IYpbhnexbfidj71JV8fnPlVHieew9fu85nduc6zfpe5sttgI9d6PndLB83rwc2nbrp+Rbc+7Lc/7B3+u7quDcfJB72mHY9QH9xwX1p3nrb4nG15vn98vu4zlnbx3LY3lbbrXuZ04LjnOpbZs/VxTY8mOTajvfG9Xf/+23+2Jv9fJd1PqtRTSoH00BEgAAAAAJFnCBgAAAECSAAkAAACAJAESAAAAAEkCJAAAAACSBEgAAAAAJAmQAAAAAEgSIAEAAACQJEACAAAAIEmABAAAAECSAAkAAACAJAESAAAAAEkCJAAAAACSBEgAAAAAJAmQAAAAAEgSIAEAAACQJEACAAAAIEmABAAAAECSAAkAAACAJAESAAAAAEkCJAAAAACSBEgAAAAAJAmQAAAAAEgSIAEAAACQJEACAAAAIEmABAAAAECSAAkAAACAJAESAAAAAEkCJAAAAACSBEgAAAAAJAmQAAAAAEgSIAEAAACQJEACAAAAIEmABAAAAECSAAkAAACAJAESAAAAAEkCJAAAAACSBEgAAAAAJAmQAAAAAEgSIAEAAACQJEACAAAAIEmABAAAAECSAAkAAACAJAESAAAAAEkCJAAAAACSBEgAAAAAJAmQAAAAAEgSIAEAAACQJEACAAAAIEmABAAAAECSAAkAAACAJAESAAAAAEkCJAAAAACSBEgAAAAAJAmQAAAAAEgSIAEAAACQJEACAAAAIEmABAAAAECSAAkAAACAJAESAAAAAEkCJAAAAACSBEgAAAAAJAmQAAAAAEgSIAEAAACQJEACAAAAIEmABAAAAECSAAkAAACAJAESAAAAAEkCJAAAAACSBEgAAAAAJAmQAAAAAEgSIAEAAACQJEACAAAAIEmABAAAAECSAAkAAACAJAESAAAAAEkCJAAAAACSBEgAAAAAJAmQAAAAAEgSIAEAAACQJEACAAAAIEmABAAAAECSAAkAAACAJAESAAAAAEkCJAAAAACSBEgAAAAAJAmQAAAAAEgSIAEAAACQJEACAAAAIEmABAAAAECSAAkAAACApP+/HTsQAACAgSD0/tQj2BEURgIJAAAAgCSQAAAAAEgCCQAAAIAkkAAAAABIAgkAAACAJJAAAAAASAIJAAAAgCSQAAAAAEgCCQAAAIAkkAAAAABIAgkAAACAJJAAAAAASAIJAAAAgCSQAAAAAEgCCQAAAIAkkAAAAABIAgkAAACAJJAAAAAASAIJAAAAgCSQAAAAAEgCCQAAAIAkkAAAAABIAgkAAACAJJAAAAAASAIJAAAAgCSQAAAAAEgCCQAAAIAkkAAAAABIAgkAAACAJJAAAAAASAIJAAAAgCSQAAAAAEgCCQAAAIAkkAAAAABIAgkAAACAJJAAAAAASAIJAAAAgCSQAAAAAEgCCQAAAIAkkAAAAABIAgkAAACAJJAAAAAASAIJAAAAgCSQAAAAAEgCCQAAAIAkkAAAAABIAgkAAACAJJAAAAAASAIJAAAAgCSQAAAAAEgCCQAAAIDftgMnTR8wel9UZgAAAABJRU5ErkJggg=='

    var elementHTML = mycustomhtml;

    var specialElementHandlers = {
        '#Downloadpdf': function (element, renderer) {
            return true;
        }
    };
    doc.addImage(imgData, 'PNG', 5, 5, 40, 25);
    doc.fromHTML(elementHTML, 10, 30, {
        'width': 170,
        'elementHandlers': specialElementHandlers
    });

    // Save the PDF
    doc.save('RosPDF.pdf');

}

//function GetDataVisitaDetail(id) {

//    var param = {
//        IdVisita: id
//    };

//    $.ajax({
//        url: "GetDataMinutaFromVisita", // Url
//        type: "POST", // Verbo HTTP       
//        data: JSON.stringify(param)
//    })
//        // Se ejecuta si todo fue bien.
//        .done(function (result) {
//            //(result);
//            $.each(result, function (index, value) {
//                format = function date2str(x, y) {
//                    var z = {
//                        M: x.getMonth() + 1,
//                        d: x.getDate(),
//                        h: x.getHours(),
//                        m: x.getMinutes(),
//                        s: x.getSeconds()
//                    };
//                    y = y.replace(/(M+|d+|h+|m+|s+)/g, function (v) {
//                        return ((v.length > 1 ? "0" : "") + eval('z.' + v.slice(-1))).slice(-2)
//                    });

//                    return y.replace(/(y+)/g, function (v) {
//                        return x.getFullYear().toString().slice(-v.length)
//                    });
//                };

//                var newOption = new Option(decodeURIComponent(value.Entidad), value.IDEntidad, true, true);
//                $('#ClienteM').append(newOption).trigger('change');
//                $('#Contacto').val(value.Contacto);
//                date = format(new Date(value.FechaReunion), 'yyyy-MM-dd');
//                $('#Fecha').val(date);
//                $('#MotivoReunion').val(value.Motivo);
//                $('#lugar').text(value.Direccion);


//            });
//        })
//        // Se ejecuta si se produjo un error.
//        .fail(function (xhr, status, error) {

//        })
//        // Hacer algo siempre, haya sido exitosa o no.
//        .always(function () {

//        });
//}
window.onload = function () {
    $("#modal").hide();
};