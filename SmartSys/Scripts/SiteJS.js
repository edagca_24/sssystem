﻿
////// Intancia de Pluging para dar opciones de formato a los textareas

// tinymce
tinymce.init({
    selector: '.tynemce',   
    branding: false,
    menubar: 'edit'
});

////////////

/// Bloquea fechas anteriores en datepicker ////
//var today = new Date().toISOString().split('T')[0];
//document.getElementsByClassName("datepickerhtml")[0].setAttribute('min', today);
////

$('[data-toggle="tooltip"]').tooltip({
    container: 'body'
})

$('.select-normal').prepend('<option value="0" disabled selected>Seleccione</option>');

function formatDate(date) {

    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    switch (month) {
        case "01":
            month = "Enero";
            break;
        case "02":
            month = "Febrero";
            break;
        case "03":
            month = "Marzo";
            break;
        case "04":
            month = "Abril";
            break;
        case "05":
            month = "Mayo";
            break;
        case "06":
            month = "Junio";
            break;
        case "07":
            month = "Julio";
            break;
        case "08":
            month = "Agosto";
            break;
        case "09":
            month = "Septiembre";
            break;
        case "10":
            month = "Octubre";
            break;
        case "11":
            month = "Noviembre";
            break;
        case "12":
            month = "Diciembre";
            break;
    }

    return [day, month, year].join('-');

}

function formatDateJS(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}


function cargandoOpen() {

    $("#loader").attr("display", false);
}

function cargarndoClose() {
    $("#modalCarga").hide();
}


(function () {
    'use strict';
    window.addEventListener('load', function () {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();


function validation(param) {


    if ($("#" + param).val() === "") {
        $("#" + param).addClass('is-invalid');
        $("#" + param).focus();


        return false;
    } else {
        $("#" + param).removeClass('is-invalid');
        $("#" + param).addClass('is-valid');
    }
    if (param === "select2-search__field") {
        if ($("." + param).val() === "") {
            $("." + param).addClass('is-invalid');
            $("." + param).focus();
            return false;
        } else {
            $("." + param).removeClass('is-invalid');
            $("." + param).addClass('is-valid');
        }
    }

}

function validationForm(forms) {
    //const forms = document.querySelectorAll(formId);
    //const form = forms[0];
    let retorna = true;

    [...forms.elements].forEach((input) => {

        if (input.type === "search") {
        //if ($("#" + input.id).val().length !== 0) {
        //    $(".select2-selection").css('border-color', '')
        //}
        //if ($("#" + input.id).val().length === 0) {
        //    $(".select2-selection").css('border-color', '#dc3545')
        //    retorna = false;
        //}
            
            return;
        }

        if (input.type === "button") {
            return false; 
        }

        if ($("#" + input.id).hasClass("required") === true) {

            if (input.type === "text") {
                if ($("#" + input.id).val() !== "" || $("#" + input.id).val() !== null) {
                    $("#" + input.id).removeClass('is-invalid');
                    $("#" + input.id).addClass('is-valid');
                }
                if ($("#" + input.id).val() === "" || $("#" + input.id).val() === null) {
                    $("#" + input.id).addClass('is-invalid');
                    retorna = false;
                }
            } else if (input.type === "textarea") {
                if ($("#" + input.id).hasClass("tynemce") === true) {
                   
                    if (tinymce.get(input.id).getContent() !== "") {
                       
                        return false;
                    }
                    if (tinymce.get(input.id).getContent() === "") {
                        Swal.fire(
                            "Debe crear el resumen de la minuta para proceder"
                        )
                        retorna = false;
                    }
                } else {

                if ($("#" + input.id).val() !== "" || $("#" + input.id).val() !== null) {
                    $("#" + input.id).removeClass('is-invalid');
                    $("#" + input.id).addClass('is-valid');
                }
                if ($("#" + input.id).val() === "" || $("#" + input.id).val() === null) {
                    $("#" + input.id).addClass('is-invalid');
                    retorna = false;
                }
                }
            }
            else if (input.type === "select-one") {
                if ($("#" + input.id + " option:selected").val() !== "" || $("#" + input.id + "option:selected").val() !== null || $("#" + input.id + "option:selected").val() !== "0") {
                    $("#" + input.id).removeClass('is-invalid');
                    $("#" + input.id).addClass('is-valid');
                    $(".select2-selection--single").css('border-color', '')
                }
                if ($("#" + input.id + " option:selected").val() === "" || $("#" + input.id + " option:selected").val() === null || $("#" + input.id + " option:selected").val() === "0") {
                    $("#" + input.id).addClass('is-invalid');                   
                    retorna = false;
                }
            }
            else if (input.type === "date") {
                if ($("#" + input.id).val() !== "" || $("#" + input.id).val() !== null) {
                    $("#" + input.id).removeClass('is-invalid');
                    $("#" + input.id).addClass('is-valid');
                }
                if ($("#" + input.id).val() === "" || $("#" + input.id).val() === null) {
                    $("#" + input.id).addClass('is-invalid');
                    retorna = false;
                }
            }
            else if (input.type === "time") {
                if ($("#" + input.id).val() !== "" || $("#" + input.id).val() !== null) {
                    $("#" + input.id).removeClass('is-invalid');
                    $("#" + input.id).addClass('is-valid');
                }
                if ($("#" + input.id).val() === "" || $("#" + input.id).val() === null) {
                    $("#" + input.id).addClass('is-invalid');
                    retorna = false;
                }
            }
            else if (input.type === "select-multiple") {
                if ($("#" + input.id).val() !== "" || $("#" + input.id).val() !== null) {
                    $("#" + input.id).removeClass('is-invalid');
                    $("#" + input.id).addClass('is-valid');
                }
                if ($("#" + input.id).val() === "" || $("#" + input.id).val() === null) {
                    $("#" + input.id).addClass('is-invalid');
                    retorna = false;
                }
                if ($("#" + input.id).val().length !== 0) {
                    $(".select2-selection--multiple").css('border-color', '')
                }
                if ($("#" + input.id).val().length === 0) {
                    $(".select2-selection--multiple").css('border-color', '#dc3545')
                    retorna = false;
                }
            }
        }
    });
    return retorna;
}

function validDateTime(date, imput) {
    $("#" + imput.id).removeClass('is-invalid');
    let today = new Date();
    day = today.getDate(),
        month = today.getMonth() + 1, //January is 0
        year = today.getFullYear();
    today = new Date(month + "/" + day + "/" + year + " " + 00 + ":" + 00 + ":" + 00)
    let entrada = new Date(date);
    dayentrada = entrada.getDate() + 1,
        monthentrada = entrada.getMonth() + 1, //January is 0
        yearentrada = entrada.getFullYear();
    hour = today.getHours();
    min = today.getMinutes();
    seg = today.getSeconds();
    entrada = new Date(monthentrada + "/" + dayentrada + "/" + yearentrada + " " + 00 + ":" + 00 + ":" + 00)

    var enterdate = moment(entrada, "mm-dd-yyyy");
    var todaydate = moment(today, "mm-dd-yyyy");
    if (entrada < today) {
        Swal.fire(
            "La fecha no puede ser anterior a la fecha actual"
        )
        $("#" + imput.id).addClass('is-invalid');
        return false;
    }
}

function validaHora(input1, input2, form) {
    var retorna = true;

    var currentTime = moment();    // e.g. 11:00 pm
    var startTime = moment(input1, "HH:mm a");
    var endTime = moment(input2, "HH:mm a");

    let fechaEntrada = "";
    switch (form) {
        case 'formCreacionVisita':
            fechaEntrada = $("#idFechaInicio").val()
            break;
        case 'formPosponerVisita':
            fechaEntrada = $("#pospFechaInicio").val()
            break;
    }

    let entrada = new Date(fechaEntrada);
    dayentrada = entrada.getDate() + 1,
        monthentrada = entrada.getMonth() + 1, //January is 0
        yearentrada = entrada.getFullYear();
    entrada = new Date(monthentrada + "/" + dayentrada + "/" + yearentrada)
    let actualDate = new Date();

    if (entrada.getDay() === actualDate.getDay() && entrada.getDate() == actualDate.getDate() && entrada.getFullYear() === actualDate.getFullYear()) {
        if (startTime < moment(actualDate.getHours() + ":" + actualDate.getMinutes(), "HH:mm a")) {
            Swal.fire(
                'La hora inicio no puede ser anterior a la actual'
            )
            retorna = false;
        }
    }

    if (startTime > endTime) {
        Swal.fire(
            'La hora fin no puede ser anterior a la de inicio'
        )
        retorna = false;
    }
    if (startTime._isValid === false || endTime._isValid === false) {
        Swal.fire(
            'Campos de Hora incorrectos'
        )
        retorna = false;
    }

    return retorna;
}



function CleanForms(items) {

    location.reload();

}

function pulsar(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    return (tecla != 13);
}

function deleteValue(input) {
    $("#" + input.id).attr("value", '')
}

function reloadPage(param) {

    //if (param === 'Inicio') {
    //    $(".container-fluid").hide();
    //    $(".container-fluid").append('<div id="Loader" style="position: relative;left: 45 %;font-size: xx-large;">Actualizando...</div >');
    //} else if(param === 'Fin') {
    //    $("#Loader").hide();
    //    $(".container-fluid").show();
       
    //}
    
}
