﻿

//var existingfile;
var localfecha;
var localpoliza;
var batch;
var flag = false;
var Aseguradora;
var doc;

$(document).ready(function () {

    //se procede a eliminar cualquier temporal cuando se cargar el sistema___//
    cleantemp();   

    // variables del modal_________________________________________//
    var span = document.getElementsByClassName("close")[0];   
    var modal = document.getElementById("myModal");
    var span2 = document.getElementById("close2");
    var indexareg = document.getElementById("RegModal");

    //Reglas de inicio____________________________________________//
    $("#txtpoliza").attr('maxlength', '20');
    $("#txtcliente").attr('maxlength', '30');    
    $("#txtpoliza").focus();
    //ocultar el modal________________________________________________//
    $("#btncargar").click(function () {
        if (validarinputs())
        {
            var files = $("#file").get(0).files;            
            var ssdata = new FormData();
            ssdata.append("data", files[0]);
            ssdata.append("Id", $("#txtpoliza").val());
            var filename = files[0].val;           
            uploadfiles(ssdata, filename);
  
        }
        
    });  
    $("#btnarchivo").click(function () {
        if ($("#txtcliente").val() === "") {
            Swal.fire(
                "Consulte una poliza",
                "Consulte una poliza valida para continuar.",
                "error"
            );
        }
        else {
            $("#myModal").attr("style", "display:block");
        }
    });
    $("#txtpoliza").keyup(function (e) {
        if (e.which === 13) {
            ConsultaPoliza();
        }
        else
        {
            $("#txtcliente").val("");
            $("#txtBatch").val("");
            $("#file").val("");
            $("#image").attr("src", "");
            cleantemp();
            
        }
    }); 
    $("#file").change(function () {

        $("#image").attr("src", URL.createObjectURL($("#file").get(0).files[0]));

    });
    span.onclick = function () {
        modal.style.display = "none";
       
    };
    span2.onclick = function () {        
        indexareg.style.display = "none";
    };
    //boton de registrar archivos
    $("#btnRegistrar").click(function () {       

        if ($("#txtcliente").val() === "") {
            Swal.fire(
                'Poliza no encontrada',
                'Debe consultar una poliza valida e insertatar un archivo valido para poder continuar.',
                'error'
            );
        }
        else
        {
            filesearching()
            if (flag === true) {
                registerdirectory(localfecha, localpoliza, batch);
            }
            else {
                Swal.fire('Error',
                    'Debe cargar por lo menos un archivo para poder registrar un expediente',
                    'error'
                );
            }
        }
                
    });
    //boton de indexar los documentos...
    $("#btnindexar").click(function () {

        if ($("#txtcliente").val() == "")
        {
            swal.fire(
                "Sin poliza",
                "Debe consultar una poliza valida para poder continuar",
                "Error"

            );
        }
        else {
            ///funcion de abrir el dialogo que indexa los expedientes....
            $("#RegModal").attr("style", "display:block");
            $("#txtpoliza2").val($("#txtpoliza").val()) 
            $("#txtcliente2").val($("#txtcliente").val()) 
            $("#txtbatch2").val($("#txtBatch").val())
            $("#txtaseguradora").val(Aseguradora)
           
        }

    }); 
    $("#btnconfirmaindexar").click(function () {
        indexar();
    });
    $("#btnfilanizar").click(function ()
    {
        finalIndexa();
        //cleantemp();
        //$("#txtpoliza").val("");        
        //indexareg.style.display = "none";        

    });

    $("#btnconsultaindex").click(function () {
        consultaindex();          

    });
});
//Cargador de arvhivos______________________________________//
function uploadfiles(ssdata) {
    var cont = 0;	
    $.ajax({
        url: 'uploadfile',
        type: 'POST',
        contentType: false,
        processData: false,
        data: ssdata,
        success: function (data) {
            if (data[0] === "1") {
                var filedate = data[1];
                var fileinfo = "...Temp File type " + data[5];
                var icono;
               		cont = cont+1;	
                if (data[5] === ".pdf") {
                    icono = "/Plantilla/dist/img/pdf.png";
                }
                else if (data[5] === ".docx") {
                    icono = "/Plantilla/dist/img/docx.png";
                        
                }
                else {
                    icono = "/Plantilla/dist/img/fileico.png";
                }
                Swal.fire(
                    'Cargado',
                    'Archivo cargado correctamente.',
                    'success'
                );
               
                $("#file").val("");
                $("#image").attr("src", "");
                $("#modal-table-body").append('\<tr class="tr-dialogbox"><td class="data">' + data[3] + '</td>><td>' + fileinfo + '</td><td>' + data[4] + '</td><td><img src=' + icono + ' style="width: 2.5em; height: 2.5em"></td><td><button id =btneliminar' + (cont) + ' onclick = "return deletefile(this,\'' + data[3] + '\',\'' + data[1] + '\',\'' + data[2] + '\')"  type="button" class="btn btn-danger">X</button></td></tr>');
                localfecha = data[1];
                localpoliza = data[2];
            }
            else if (data[0] === "0") {
                Swal.fire(
                    'Sin archivo...',
                    'Debe cargar un archivo para poder continuar.',
                    'question'
                );
            }
            else if (data[0] === "error") {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Algo ha salido mal, intente de nuevo!',
                    footer: '<a href>Why do I have this issue?</a>'
                });
            }
            else if (data[0] === "?") {
                Swal.fire(
                    'Archivo incorrecto...',
                    'El tipo de archivo que intenta cargar es invalido.',
                    'question'
                );
            }

        }, error: function () {
            alert("Ha habido un error y no se ha podido subir el archivo.");
        }
    });
    }
//Buscar polizaa____________//
function ConsultaPoliza() {

    $.ajax({
        url: 'ConsultaPoliza',
        type: 'POST',
        //dataType: 'json',
        //contentType: 'application/json',
        data: {
            poliza: $("#txtpoliza").val()
        },
        success: function (data) {

            if (data[0] !== null) {

                $("#txtcliente").val(data[2]);
                $("#txtBatch").val(data[1]);
                batch = $("#txtBatch").val();
                Aseguradora = data[3];
            }
            else {
                Swal.fire(
                    'Poliza incorrecta',
                    'Esta poliza no se encuentra en el sistema.',
                    'error'
                );
            }


        },
        error: function () {
            alert("Ha habido un error y no se puede consultar la poliza.");
        }

    });
}
//Validador de inputs___________________________________////
function validarinputs(poliza)
{
    var txtpoliza = $("#txtpoliza").val();
    var txtcliente = $("#txtcliente").val();
    var data;
    var flag = 0;


    if (txtpoliza === "")
    {
        Swal.fire(
            "Campo vacio",
            "Debe consultlar una poliza para continuar",
            "Error"
        );
        return false;
        $("#txtpoliza").attr("requerd", true);
        $("#txtpoliza").focus();
    }
    else if ($("#txtcliente").val() === "")
    {
        Swal.fire(
            "Poliza no encontrada",
            "Esta poliza es incorreta",
            "Error"
        );
        $("#txtpoliza").attr("requerd", true);
        $("#txtpoliza").focus();
        return false;
    }
    else if ($("#file").val() === "")
    {
        Swal.fire(
            "Sin archivo",
            "Debe seleccionar una archivo para continuar.",
            "Error"
        );
        return false;
    }

 

    return true;

}
//Funcion para borrar los temporales_________////
function cleantemp()
{
    localfecha = "";
    localpoliza = "";
    flag = false;
    batch = "";
    Aseguradora = "";
    $("#txtcliente").val("");
    $("#txtBatch").val("");
    $("#txtdependientes").val("");
    $("#txtparentesco").val("");
    $("#txtnoreclamo").val("");
    $("#image").attr("src", "");
    
    

    $.ajax({
        url: 'erasetemp',
        type: 'POST',       
        success: function (data) { },
        error: function () {
            alert("Ha habido un error!");
        }

    });

    $(".tr-dialogbox").each(function () {
       $(".tr-dialogbox").remove();
     });

}
//funcion para eliminar archivos de temporales____///
function deletefile(item,filename,fecha,poliza) {
    var td = item.parentNode;
	var tr = td.parentNode;
	tr.parentNode.removeChild(tr);    


     $.ajax({
         url: 'erasefile',
         type: 'post',
         data: {
            filename: filename,
			feccha: fecha,
			polizza: poliza
        },
        success: function (d) {
            if (d !== null) {
                
                 tr.parentnode.removechild(tr);
                swal.fire(
                     "archivo eliminado correctamente",                    
                     "success"
                );
             }
             else
             {
                swal.fire(
                    "error",
                     "ha habido un error y no se ha podido eliminar el archivo(problablemente no exista)",
                     "error"
                );                 
             }

         },
         error: function () {
             alert("ha habido un error!");
         }

     });


   
 

}
//funcion para registrar un archivo_________///
function registerdirectory(fecha, poliza) {
    $.ajax({
        url: 'registerfiles',
        type: 'post',
        data: {           
            feccha: fecha,
            polizza: poliza,
            batchh: batch
        },
        success: function (d) {
            if (d == 'exito') {               
                swal.fire(
                    "Registrado",
                    "Archivos registrados exitosamente.",
                    "success"
                );
            }
            else if (d == 'error'){
                swal.fire(
                    "error",
                    "ha habido un error y no se ha podido registrar el expediente",
                    "error"
                );
            }

        },
        error: function () {
            alert("ha habido un error!");
        }

    });

}
//funcion para verificar si existe un archivo en el sistema....///
function filesearching()
{   
    $.ajax({
        async: false,
        url: 'FileConsult',
        type: 'POST',
        //dataType: 'json',
        //contentType: 'application/json',
        data: {
            polizza: $("#txtpoliza").val()            
        },
        success: function (data) {
            if (data !== true)
            {
                flag = false;                
            }
            else {
                flag = true;               
            }           
            
        },
      

    });


}
//funcion de para verificar sin un archivo esta indexado.....////
function indexar()
{
  
    

    $.ajax({
        url: 'Indexa',
        type: 'POST',
        data: {
            poliza: $("#txtpoliza").val(),
            cliente: $("#txtcliente").val(),
            aseguradora: Aseguradora,
            titular: $("#txtcliente2").val(),
            Dependientes: $("#txtdependientes").val(),
            Batch: $("#txtbatch2").val(),
            TipoDoc: $("#txttipodocumento").val(),
            Parentesco: $("#txtparentesco").val(),
            Noreclamo: $("#txtnoreclamo").val(),
        },
        success: function (data)
        {
            if (data == "true") {
                swal.fire(
                    "Indexado",
                    "Expediente indexado con exito.",
                    "success"
                );
            }
            else if ( data =="false") {
                swal.fire(
                    "Error",
                    "Expediente no se ha podido registrar.",
                    "error"
                );
            }
            else
            {
                swal.fire(
                    "Error",
                    "No existen registros relacionados con esta poliza.",
                    "error"
                );
            }
        }
    });


}
//funcion para limpiar todo el proceso---------------
function finalIndexa()
{
    $.ajax({
        url: 'finalindex',
        type: 'post',
        data: {
            poliza: $("#txtpoliza").val(),
            
        },
        success: function (d) {
            if (d === null) {
                swal.fire(
                    "Error",
                    "La indexacion no ha podido culmunar con exito",
                    "error"
                );                
            }
            else if (d === "404")
            {
                swal.fire(
                    "Error",
                    "El directorio no existe reintente registrar e indexar el expediente.",
                    "error"
                );                
            }
            else if (d === "correct")
            {
                swal.fire(
                    "Terminado!",
                    "Proceso terminado",
                    "success"
                );
            }

        },      

    });

}

function consultaindex()
{
    $.ajax({
        url: 'consultaindex',
        type: 'post',      
        success: function (d) { 
        },
    });
}

