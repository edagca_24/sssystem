
var datos = [];
var conteo = 0;
var conteo2 = 0;
var idevent = 0;
$(document).ready(function () {
    // cargarEventos();

    $("#formEditar").hide();
    $("#formPosponer").hide();
    $("#formCompletar").hide();

    $(".SelectCliente").select2({

        placeholder: "Buscar...",
        allowClear: false,
        //theme: "bootstrap4",
        width: '83%',
        ajax: {
            url: "/Visita/GetData",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term,
                    tipo: 'Cliente'
                };
            },
            processResults: function (data, params) {
                return {
                    results: data
                };
            }
        }
    });

    $(".SelectResponsable").select2({

        placeholder: "Buscar...",
        allowClear: false,
        //theme: "bootstrap4",
        width: '83%',
        ajax: {
            url: "/Visita/GetData",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term,
                    tipo: 'Usuario'
                };
            },
            processResults: function (data, params) {
                return {
                    results: data
                };
            }
        }
    });

    $(".SelectIntegrante").select2({

        placeholder: "Buscar...",
        allowClear: false,
        //theme: "bootstrap4",
        width: '83%',
        ajax: {
            url: "/Visita/GetData",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term,
                    tipo: 'Integrante'
                };
            },
            processResults: function (data, params) {
                return {
                    results: data
                };
            }
        }
    });

    $(".SelectIntegranteEdit").select2({

        placeholder: "Buscar...",
        allowClear: false,
        //theme: "bootstrap4",
        width: '83%',
        ajax: {
            url: "/Visita/GetData",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term,
                    tipo: 'Integrante'
                };
            },
            processResults: function (data, params) {
                return {
                    results: data
                };
            }
        }
    });

    $(".SelectClienteMEdit").select2({

        placeholder: "Buscar...",
        allowClear: false,
        //theme: "bootstrap4",
        width: '83%',
        ajax: {
            url: "/Visita/GetData",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term,
                    tipo: 'Cliente'
                };
            },
            processResults: function (data, params) {
                return {
                    results: data
                };
            }
        }
    });

    $(".add-row").click(function () {
        var integrante = $("#Integrante option:selected").text();
        var idIntegrante = $("#Integrante option:selected").val();

        if (idIntegrante === undefined || idIntegrante === "") {
            Swal.fire({
                title: 'Este integrante es incorrecto o no existe, Deseas agregarlo como integrante externo?',
                text: "",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#0a8a1b',
                cancelButtonColor: '#fc2626',
                confirmButtonText: 'Si',
                cancelButtonText: 'No'
            }).then((result) => {
                if (result.value) {
                    AddIntegranteExterno(integrante);
                }
            });
            $("#Integrante").empty();
            $('#Integrante').val(null).trigger('change');
            //$("#searchIntegrante").addClass('is-invalid');
            return false;
        }

        var stop = 1;
        var filas = document.querySelectorAll("#TableIntegrantes tbody tr");
        var columnas;
        if (integrante === "") {
            return false;
        }
        if (filas.length !== 0) {
            filas.forEach(function (e) {
                columnas = e.querySelectorAll("td");
                if (columnas[0].textContent === integrante) {
                    stop = 0;
                }
            });
            if (stop === 0) {

                Swal.fire(
                    'Este integrante ya esta agregado'
                );

            }
        }

        if (stop !== 0) {
            var markup = "<tr  id='addr" + conteo + "'><td><input id='" + idIntegrante + "' type='hidden'  value='" + idIntegrante + "'/>" + integrante + "</td><td><button type='button' class='btn btn-danger' id='botonEliminar " + idIntegrante + "' onclick='DeleteItems(this)'>X</button></td></tr>";
            $("#TableIntegrantes  tbody").append(markup);
            if ($("#Integrantes").val() === "") {
                $("#Integrantes").val("" + idIntegrante + "" + ",");
            } else {

                $("#Integrantes").val($("#Integrantes").val() + "" + idIntegrante + "" + ",");
            }
            $("#Integrante").empty();
            $('#Integrante').val(null).trigger('change');
            conteo++;
        }
    });

    $(".add-rowEdit").click(function () {
        var integrante = $("#IntegranteEdit option:selected").text();
        var idIntegrante = $("#IntegranteEdit option:selected").val();
        if (idIntegrante === undefined || idIntegrante === "") {
            Swal.fire(
                'Este integrante es incorrecto'
            );
            $("#searchResponsableEdit").focus();
            $("#searchResponsableEdit").addClass('is-invalid');
            return false;
        }

        var stop = 1;
        var filas = document.querySelectorAll("#TableIntegrantesEdit tbody tr");
        var columnas;
        if (integrante === "") {
            return false;
        }
        if (filas.length !== 0) {
            filas.forEach(function (e) {
                columnas = e.querySelectorAll("td");
                if (columnas[0].textContent === integrante) {
                    stop = 0;
                }
            });
            if (stop === 0) {
                Swal.fire(
                    'Este integrante ya esta agregado'                    
                );
            }
        }

        if (stop !== 0) {
            var markup = "<tr  id='addr" + conteo2 + "'><td><input id='" + idIntegrante + "' type='hidden'  value='" + idIntegrante + "'/>" + integrante + "</td><td><button type='button' class='btn btn-danger' id='botonEliminar " + idIntegrante + "' onclick='DeleteItems(this)'>X</button></td></tr>";
            $("#TableIntegrantesEdit  tbody").append(markup);
            if ($("#editIntegrantes").val() === "") {
                $("#editIntegrantes").val("" + idIntegrante + "" + ",");
            } else {

                $("#editIntegrantes").val($("#editIntegrantes").val() + "" + idIntegrante + "" + ",");
            }
            $("#IntegranteEdit").empty();
            $('#IntegranteEdit').val(null).trigger('change');
            conteo2++;
        }

    });

    $('#guardarvisita').on('click', function (e) {
        e.preventDefault();
        var forms = document.getElementsByClassName('needs-validation')[0];
        if (validationForm(forms) === true) {
            if (validaHora($("#idHoraInicio").val(), $("#idHoraFin").val(), 'formCreacionVisita') === true) {
                SaveVisita();
            }
        }

    });

    $(".close").click(function () {

        $('#formCompletar')[0].reset();
        $('#formPosponer')[0].reset();
        $('#formEditar')[0].reset();
        $('#formReporte')[0].reset();
        $('#formDetalle label').text('');
        $('#tableIntegrantes tbody td').html('');
        $('#formCompletar').hide();
        $('#formPosponer').hide();
        $('#formEditar').hide();
        $("#divBotonesDetalle").attr("hidden", false);
        $('#formDetalle').show();
        location.reload();

    });

    $("#btnGenerarReport").click(function () {
        $('#ModalReporte').modal('show');
    });

    ///////Busqueda cliente

    $('#Cliente').keyup(function () { // aqui va el input text para la busqueda

        $('#result_Cliente').html(''); // aqui va la data que se va a mostrar

        if (this.value !== "") {
            getListData(this.value, 'Cliente', 'Cliente');
        } else {
            return false;
        }
    });

    ////////// Fin Document ready /////////////
});

$(function () {
    // cargarEventos();

    // cargarEventos();

    //$('#TableIntegrantes').DataTable();

    $('[data-mask]').inputmask();

    $('#Responsable').keyup(function () { // aqui va el input text para la busqueda

        $('#Responsable').html(''); // aqui va la data que se va a mostrar

        if (this.value !== "") {
            getListData(this.value, 'Usuario', 'Responsable');
        } else {

            return false;
        }
    });

    $('#ResponsableEditar').keyup(function () { // aqui va el input text para la busqueda

        $('#resultResponsableEditar').html(''); // aqui va la data que se va a mostrar

        if (this.value !== "") {
            getListData(this.value, 'Usuario', 'ResponsableEditar');
        } else {
            return false;
        }
    });

    //$('#searchResponsableEdit').keyup(function () { // aqui va el input text para la busqueda

    //    $('#resultResponsableEdit').html(''); // aqui va la data que se va a mostrar

    //    if (this.value !== "") {
    //        getListData(this.value, 'idResponsableEdit');
    //    } else {
    //        return false;
    //    }
    //});

    $('#searchIntegrante').keyup(function () { // aqui va el input text para la busqueda

        $('#resultIntegrantes').html(''); // aqui va la data que se va a mostrar

        $('#state').val('');

        var searchPerson = $('#search').val(); // aqui optenemos el valor en el evento KeyUp
        if (this.value !== "") {
            getListData(this.value, 'Integrante', 'Integrantes');
        } else {
            return false;
        }

        // aqui optenemos la url que va hacer la búsqueda y va construyendo la lista mientras busca



    });


    ///////Inicializacion del Calendario ///////
    var Calendar = FullCalendar.Calendar;

    var calendarEl = document.getElementById('calendar');

    var calendar = new Calendar(calendarEl, {
        plugins: ['bootstrap', 'interaction', 'dayGrid', 'timeGrid'],
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay',
            locale: 'es'
        },

        events: '/Visita/GetEvent/',
        eventClick: function (info) {
            id = info.event.title.split("-")[0];
            var today = new Date().toISOString().split('T')[0];
            document.getElementsByClassName("datepickerhtml")[0].setAttribute('min', today);
            getDetailVisita(id);
            $('#ModalDetalle').modal('show');
            // change the border color just for fun
            info.el.style.borderColor = 'white';
        }
    });

    calendar.render();
    calendar.setOption('locale', 'es');

});


/* ADDING EVENTS */
var currColor = '#3c8dbc' //Red by default
//Color chooser button
var colorChooser = $('#color-chooser-btn');
$('#color-chooser > li > a').click(function (e) {
    e.preventDefault();
    //Save color
    currColor = $(this).css('color');
    //Add color effect to button
    $('#add-new-event').css({
        'background-color': currColor,
        'border-color': currColor
    });
});
$('#add-new-event').click(function (e) {
    e.preventDefault()
    //Get value and make sure it is not null
    var val = $('#new-event').val();
    if (val.length === 0) {
        return;
    }

});



function fillListResponsable(data) {
    $('#result_Responsable').empty();

    for (var i = 0; i < data.length; i++) {

        $('#result_Responsable').append(`<li class="list-group-item link-class" onclick="setItem(this,'Responsable')" value="${data[i].ID}" name="${data[i].ID} +'|'+ ${data[i].NombreCompleto}">${data[i].NombreCompleto}</li>`);
        //$("#listResponsable").append("<a href='#' name='" + data[i].IdPersona + "' onclick='setItem(this,'idResponsableVisita')' class='list-group-item list-group-item-action'>" + data[i].nombre + "</a>");

    }
}

function fillListResponsableEditar(data) {
    $('#resultResponsableEditar').empty();
    for (var i = 0; i < data.length; i++) {

        $('#resultResponsableEditar').append(`<li class="list-group-item link-class" onclick="setItem(this,'ResponsableEditar')" value="${data[i].ID}" name="${data[i].ID} +'|'+ ${data[i].NombreCompleto}">${data[i].NombreCompleto}</li>`);
        //$("#listResponsable").append("<a href='#' name='" + data[i].IdPersona + "' onclick='setItem(this,'idResponsableVisita')' class='list-group-item list-group-item-action'>" + data[i].nombre + "</a>");

    }
}

function fillListIntegrantes(data) {
    $('#resultIntegrantes').empty();
    for (var i = 0; i < data.length; i++) {

        $('#resultIntegrantes').append(`<li class="list-group-item link-class" tittle="${data[i].ID}" onclick="setItem(this,'Integrantes')" value="${data[i].ID}" name="${data[i].ID} +'|'+ ${data[i].NombreCompleto}">${data[i].NombreCompleto}</li>`);
        //$("#listResponsable").append("<a href='#' name='" + data[i].IdPersona + "' onclick='setItem(this,'idResponsableVisita')' class='list-group-item list-group-item-action'>" + data[i].nombre + "</a>");

    }
}

//function setItem(data) {

//    $('#searchResponsable').val(data.textContent).attr('value', data.value);
//    $("#searchResponsable").removeClass('is-invalid');
//    $("#resultResponsable").empty();
//}

//function setItemEdit(data) {

//    $('#searchResponsableEdit').val(data.textContent).attr('value', data.value);
//    $("#searchResponsableEdit").removeClass('is-invalid');
//    $("#resultResponsableEdit").empty();
//}

function setItem(data, input) {
    switch (input) {
        case 'Responsable':
            $('#Responsable').val(data.textContent).attr('value', data.value);
            $("#Responsable").removeClass('is-invalid');
            $("#result_Responsable").empty();
            break;
        case 'Integrantes':
            $('#searchIntegrante').val(data.textContent).attr('value', data.value);
            $("#searchIntegrante").removeClass('is-invalid');
            $("#resultIntegrantes").empty();
            break;
        case 'ResponsableEditar':
            $('#ResponsableEditar').val(data.textContent).attr('value', data.value);
            $("#ResponsableEditar").removeClass('is-invalid');
            $("#resultResponsableEditar").empty();
            break;
    }

}

function DeleteItems(items) {
    Swal.fire({
        title: 'Seguro que deseas eliminar esta linea?',
        text: "",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar linea!',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.value) {
            var td = items.parentNode;
            var tr = td.parentNode; // the row to be removed
            tr.parentNode.removeChild(tr);
            Swal.fire(
                'Eliminado!',
                '',
                'success'
            );
        }
    });
}



function getList(data, container) {

    if (data === "") {
        return false;
    }

    var param = {
        criterio: data

    };

    $.ajax({
        url: "GetData", // Url
        type: "GET", // Verbo HTTP
        data: param
    })
        // Se ejecuta si todo fue bien.
        .done(function (result) {
            if (result !== null) {
                if (container === "idResponsableEdit") {
                    fillListEdit(result);
                } else if (container === "idResponsable") {
                    fillList(result);
                } else if (container === "Responsable") {
                    fillListResponsable(result);

                }

            }
        })
        // Se ejecuta si se produjo un error.
        .fail(function (xhr, status, error) {

        })
        // Hacer algo siempre, haya sido exitosa o no.
        .always(function () {

        });
}

function getDetailVisita(id) {

    var param = {
        id: id
    };

    $.ajax({
        url: 'GetDetailVisita',
        type: 'POST',
        data: JSON.stringify(param),
        dataType: 'json',
        processData: false,
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data[0] === undefined) {
                alert("Esta visita fue agendada erroniamento o sus datos fueron eliminados");
                return false;
            }

            setDetailVisita(data, "detalle");
            getIntegrantes(data[0].IdVisita, 'Detalle');
        }
    });
}

/////////Funcion para llenar el detalle de la visita/////////////////////////

function setDetailVisita(result) {

    $("#divBotonesDetalle").show();

    ///Llenando campos del detalle ///
    $("#idVisitaCompletar").val(result[0].IdVisita);
    $("#idVisitaPosponer").val(result[0].IdVisita);
    $("#idVisitaEditar").val(result[0].IdVisita);
    $("#motivo").text(result[0].Motivo);
    $("#contacto").text(result[0].Contacto);
    $("#estado").text(result[0].Estado);
    $("#usuarioc").text(result[0].UsuarioCreador);
    $("#lugar").text(result[0].Direccion);
    $("#fecha").text(formatDate(result[0].Fecha_Inicio));
    $("#horaInicio").text(result[0].HoraInicio);
    $("#horaFin").text(result[0].HoraFin);
    $("#asunto").text(result[0].Asunto);
    $("#comentario").text(result[0].Comentario);
    $("#ID_Motivo").val(result[0].IdMotivo);
    $("#Responsable").text(result[0].Responsable);
    $("#Cliente").text(result[0].Cliente);
    $("#ClienteId").val(result[0].IdCliente);

    ///Llenando campos de editar ///
    $("#idVisitaCompletar").val(result[0].IdVisita);
    $("#idVisitaPosponer").val(result[0].IdVisita);
    $("#idVisitaEditar").val(result[0].IdVisita);
    $("#ID_Motivo").val(result[0].IdMotivo);
    $("#editcontacto").val(result[0].Contacto);
    $("#editlugar").val(result[0].Direccion);
    $("#editAsunto").val(result[0].Asunto);
    $("#editComentario").val(result[0].Comentario);
    //$("#editCliente").val(result[0].Cliente);
    var newOption = new Option(result[0].Cliente, result[0].IdCliente, true, true);
    // Append it to the select
    $('#editCliente').append(newOption).trigger('change');
    $("#searchResponsableEdit").val('');


    if (result[0].Estado === "Completada") {
        $("#divBotonesDetalle").hide();
    }

}

/////////Funcion para traer los integrantes de la visita/////////////////////////

function getIntegrantes(id, modo) {
    var param = {
        id: id
    };

    $.ajax({
        url: 'getIntegrantes',
        type: 'POST',
        data: JSON.stringify(param),
        dataType: 'json',
        processData: false,
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            setIntegrantes(data, modo);
        }
    });
}

/////////Funcion para llenar la lista de los integrantes de la visita/////////////////////////

function setIntegrantes(data, modo) {

    var responsable;

    if (modo === "Editar") {
        $.each(data, function (i, v) {
            $("#TableIntegrantesEdit  tbody").append("<tr id=''><td><input id='" + v.Integrante + "' type='hidden'  value='" + v.Integrante + "'/>" + v.Integrante + "</td><td><button type='button' id='botonEliminar " + v.IdPersona + "'  class='btn btn-danger' id='' onclick='DeleteItems(this)'>X</button></td></tr>");
        });
    }
    else {
        $("#integrantesList").empty();
        $.each(data, function (i, v) {
            if (v.Responsable === 1) {
                responsable = "<input type='checkbox' class='custom-control-input'  id='chekResponsableEdit' checked />";
            } else {
                responsable = "";
            }
            $("#integrantesList").append("<tr><td>" + v.IdIntegrante + "</td><td>" + v.Integrante + "</td></tr>");
        });
    }

}

/////////Funcion para habilitar los campos a editar  /////////////////////////

function editarVisita(boton) {

    switch (boton) {
        case "botonEditar":
            $("#formEditar").show("slow");
            $("#formDetalle").hide("slow");
            $("#divBotonesDetalle").attr("hidden", true);

            $("#idVisitaCompletar").val($("#idVisitaCompletar ").val());
            $("#idVisitaPosponer").val($("#idVisitaCompletar ").val());
            $("#idVisitaEditar").val($("#idVisitaCompletar ").val());
            $("#editmotivo").val($("#ID_Motivo").val());
            $("#editcontacto").val($("#contacto").text());
            $("#editlugar").val($("#lugar").text());
            $("#editAsunto").val($("#asunto").text());
            $("#editComentario").val($("#comentario").text());
            var newOption = new Option($("#Cliente").text(), $("#ClienteId").val(), true, true);
            // Append it to the select
            $('#editCliente').append(newOption).trigger('change');
            getIntegrantes($("#idVisitaEditar ").val(), 'Editar');
            break;
        case "botonPosponer":
            $("#formPosponer").show("slow");
            $("#formDetalle").hide("slow");
            $("#divBotonesDetalle").attr("hidden", true);
            break;
        case "botonCompletar":
            $("#formCompletar").show("slow");
            $("#formDetalle").hide("slow");
            $("#divBotonesDetalle").attr("hidden", true);
            break;
        case "botonVolverEditar":
            $("#formEditar").hide("slow");
            $("#formDetalle").show("slow");
            $("#divBotonesDetalle").attr("hidden", false);
            $('#formEditar')[0].reset();
            $("#tbodyEdit").html('');
            $("#searchResponsableEdit").val('');
            $('.required').removeClass('is-invalid')
            $('.required').removeClass('is-valid')
            break;
        case "botonVolverPosponer":
            $("#formPosponer").hide("slow");
            $("#formDetalle").show("slow");
            $("#divBotonesDetalle").attr("hidden", false);
            $('#formPosponer')[0].reset();
            $('.required').removeClass('is-invalid')
            $('.required').removeClass('is-valid')
            break;
        case "botonVolverCompletar":
            $("#formCompletar").hide("slow");
            $("#formDetalle").show("slow");
            $("#divBotonesDetalle").attr("hidden", false);
            $('#formCompletar')[0].reset();
            $('.required').removeClass('is-invalid')
            $('.required').removeClass('is-valid')
            break;
        case "guardarEditar":
            $("#formEditar").hide("slow");
            $("#formDetalle").show("slow");
            $("#divBotonesDetalle").attr("hidden", false);
            $('#formEditar')[0].reset();
            $("#tbodyEdit").html('');
            $("#searchResponsableEdit").val('');
            break;
        case "guardarCompletar":
            $("#formCompletar").hide("slow");
            $("#formDetalle").show("slow");
            $("#divBotonesDetalle").attr("hidden", false);
            $('#formCompletar')[0].reset();
            break;
        case "guardarPosponer":
            $("#formPosponer").hide("slow");
            $("#formDetalle").show("slow");
            $("#divBotonesDetalle").attr("hidden", false);
            $('#formPosponer')[0].reset();
            break;

    }

    ////// Funcion para llenar campos en el formulario de editar////////

}

function formatDate(date) {

    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    switch (month) {
        case "01":
            month = "Enero";
            break;
        case "02":
            month = "Febrero";
            break;
        case "03":
            month = "Marzo";
            break;
        case "04":
            month = "Abril";
            break;
        case "05":
            month = "Mayo";
            break;
        case "06":
            month = "Junio";
            break;
        case "07":
            month = "Julio";
            break;
        case "08":
            month = "Agosto";
            break;
        case "09":
            month = "Septiembre";
            break;
        case "10":
            month = "Octubre";
            break;
        case "11":
            month = "Noviembre";
            break;
        case "12":
            month = "Diciembre";
            break;
    }

    return [day, month, year].join('-');

}

//$('#buttonSaveCompletar').on('click', function (e) {
//    e.preventDefault();
//    var form = $(this).parents('#formCompletar');



//});

$('#buttonSavePosponer').on('click', function (e) {
    e.preventDefault();
    var forms = document.getElementsByClassName('needs-validation')[2];
    if (validationForm(forms)) {
        if (validaHora($("#pospHoraInicio").val(), $("#pospHoraFin").val(), 'formPosponerVisita') === true) {
            PosponerVisita();
        }
    }

});

$('#buttonSaveCompletar').on('click', function (e) {
    e.preventDefault();
    var forms = document.getElementsByClassName('needs-validation')[1];
    if (validationForm(forms) === true) {
        Swal.fire({
            title: 'Que deseas hacer al completar la visita?',
            text: "",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0a8a1b',
            cancelButtonColor: '#095899',
            confirmButtonText: 'Crear la minuta pero seguir agendando las visitas!',
            cancelButtonText: 'Ir a completar la minuta!'
        }).then((result) => {
            if (result.value) {
                //CompletarVisita('NuevaMinuta');
                CreateMinutaTemporal();
            } else {                
                
                CompletarVisita();
            }
        });
    }
});

$('#buttonSaveEditar').on('click', function (e) {
    e.preventDefault();
    var forms = document.getElementsByClassName('needs-validation')[0];
    if (validationForm(forms)) {
        EditVisita();
    }

});

function CompletarVisita(data) {

    var param = {
        IdVisita: $("#idVisitaCompletar").val(),
        Comentario: $("#realComentarioCierre").val(),
        Hora: $("#realHoraRealizada").val(),
        Fecha: $("#realFechaRealizada").val(),
        Forma: data
    };

    $.ajax({
        url: 'CompletarVisita',
        type: 'POST',
        data: JSON.stringify(param),
        dataType: 'json',
        processData: false,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {

            result = result.split("|");

            if (result[0] === '1') {
                if (data === "NuevaMinuta") {
                    Swal.fire(
                        'Visita completada correctamente',
                        'Click aqui↓',
                        'success'
                    ).then(function () {
                        window.location.href = "/Minuta/CreateMinuta/"+ result[1] +"";
                        $('#formCompletar')[0].reset();
                        $('#formCompletar').hide();
                        $('#formDetalle').show();
                       
                    });
                } else {
                    Swal.fire(
                        'Visita completada correctamente',
                        'Click aqui↓',
                        'success'
                    ).then(function () {
                        window.location.href = "/Visita/Index";
                    });
                }
            } else if (result === '0') {
                Swal.fire(
                    'Ocurrio un error, favor comunicarse con soporte tecnico',
                    'Click aqui↓',
                    'error'
                );
            }


        }
    });
}

function PosponerVisita() {

    var param = {
        IdVisita: $("#idVisitaPosponer").val(),
        HoraInicio: $("#pospHoraInicio").val(),
        HoraFin: $("#pospHoraFin").val(),
        Fecha: $("#pospFechaInicio").val(),
        Comentario: $("#ComentarioPospuesta").val()

    };

    $.ajax({
        url: 'PosponerVisita',
        type: 'POST',
        data: JSON.stringify(param),
        dataType: 'json',
        processData: false,
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data === '1') {
                Swal.fire(
                    'Visita pospuesta correctamente',
                    'Click aqui↓',
                    'success'
                ).then(function () {
                    $('#formPosponer')[0].reset();
                    $('#formPosponer').hide();
                    getDetailVisita(id);
                    $('#formDetalle').show();
                    $("#divBotonesDetalle").attr("hidden", false);
                });
            } else if (data === '0') {
                Swal.fire(
                    'Ocurrio un error, favor comunicarse con soporte tecnico',
                    'Click aqui↓',
                    'error'
                );
            }


        }
    });
}

function EditVisita() {
    var listIntegrantes = [];
    var filas = document.querySelectorAll("#TableIntegrantesEdit tbody tr");
    var columnas;
    if (filas.length !== 0) {
        filas.forEach(function (e) {
            columnas = e.querySelectorAll("td");
            listIntegrantes += columnas[0].textContent + "|" + columnas[0].childNodes[0].value + ",";
        });
        if (stop === 0) {
            return false;
        }
    }

    var param = {
        IdVisita: $("#idVisitaEditar").val(),
        Motivo: $("#editmotivo").val(),
        Contacto: $("#editcontacto").val(),
        Lugar: $("#editlugar").val(),
        Asunto: $("#editAsunto").val(),
        Comentario: $("#editComentario").val(),
        Integrantes: listIntegrantes,
        Cliente: $("#editCliente").val()
    };

    $.ajax({
        url: 'EditarVisita',
        type: 'POST',
        data: JSON.stringify(param),
        dataType: 'json',
        processData: false,
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            if (data === '1') {
                Swal.fire(
                    'Visita actualizada correctamente',
                    'Click aqui↓',
                    'success'
                ).then(function () {
                    $('#formEditar')[0].reset();
                    $('#formEditar').hide();
                    getDetailVisita(id);
                    $('#formDetalle').show();
                    $("#divBotonesDetalle").attr("hidden", false);
                    $("#tbodyEdit").empty();
                });
            } else if (data === '0') {
                Swal.fire(
                    'Ocurrio un error, favor comunicarse con soporte tecnico',
                    'Click aqui↓',
                    'error'
                );
            }


        }
    });
}

function SaveVisita() {


    if ($("#Integrante option:selected").val() !== undefined) {
        Swal.fire(
            'Tiene un integrante cargado pero no lo ha agregado a la lista, debe agregarlo a la lista'
        );
        return false;
    }

    var listIntegrantes = [];
    var filas = document.querySelectorAll("#TableIntegrantes tbody tr");
    var columnas;
    if (filas.length !== 0) {
        filas.forEach(function (e) {
            columnas = e.querySelectorAll("td");
            listIntegrantes += columnas[0].textContent + "|" + columnas[0].childNodes[0].value + ",";
        });
        if (stop === 0) {
            return false;
        }
    }

    //if ($("#Responsable").attr("value") === undefined) {
    //    Swal.fire(
    //        'El responsable es incorrecto'
    //    );
    //    $("#Responsable").focus();
    //    $("#Responsable").addClass('is-invalid');
    //    return false;
    //}

    var param = {
        Motivo: $("#Motivo").val(),
        Contacto: $("#ID_Contacto").val(),
        Lugar: $("#idLugarReunion").val(),
        Fecha: $("#idFechaInicio").val(),
        HoraInicio: $("#idHoraInicio").val(),
        HoraFin: $("#idHoraFin").val(),
        Asunto: $("#idAsunto").val(),
        Comentario: $("#idComentarioGen").val(),
        Responsable: $("#Responsable option:selected").val(),
        Integrantes: listIntegrantes,
        Cliente: $("#Cliente option:selected").val()
    };

    $.ajax({
        url: 'CreateVisita',
        type: 'POST',
        data: JSON.stringify(param),
        dataType: 'json',
        processData: false,
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            if (data === '1') {
                Swal.fire(
                    'Visita creada correctamente',
                    'Click aqui↓',
                    'success'
                ).then(function () {
                    window.location.href = "CreateVisita";
                });
            } else if (data === '0') {
                Swal.fire(
                    'Ocurrio un error, favor comunicarse con soporte tecnico',
                    'Click aqui↓',
                    'error'
                );
            }


        }
    });
}

function GenerarReporte() {
    var param = {
        id: id
    };

    $.ajax({
        url: 'getIntegrantes',
        type: 'POST',
        data: JSON.stringify(param),
        dataType: 'json',
        processData: false,
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data !== "") {
                setIntegrantes(data);
            } else { return false; }

        }
    });
}


function ViewModal(param) {
    $("#textView").val('');
    $("#textView").val(param.value);
    $('#ModalView').modal('show');
}

function getListData(data, tipo, input) {

    var param = {
        criterio: data,
        tipo: tipo
    };

    $.ajax({
        url: "GetData", // Url
        type: "GET", // Verbo HTTP
        data: param
    })
        // Se ejecuta si todo fue bien.
        .done(function (result) {

            if (result !== null) {
                switch (input) {
                    case 'Cliente':
                        fillListCliente(result);
                        break;
                    case 'Responsable':
                        fillListResponsable(result);
                        break;
                    case 'Integrantes':
                        fillListIntegrantes(result);
                        break;
                    case 'ResponsableEditar':
                        fillListResponsableEditar(result);
                        break;
                }

            }
        })
        // Se ejecuta si se produjo un error.
        .fail(function (xhr, status, error) {

        })
        // Hacer algo siempre, haya sido exitosa o no.
        .always(function () {

        });
}


function fillListCliente(data) {
    $('#result_Cliente').empty();
    for (var i = 0; i < data.length; i++) {

        $('#result_Cliente').append(`<li class="list-group-item link-class" onclick="setItemCliente(this)" value="${data[i].ID_Ente}" name="${data[i].ID_Ente} +'|'+ ${data[i].NombreCompleto}">${data[i].NombreCompleto}</li>`);
    }

}

function AddIntegranteExterno(nombre) {

    var param = {
        NombreIntegrante: nombre
    };

    $.ajax({
        url: 'CreateIntegranteExterno',
        type: 'POST',
        data: JSON.stringify(param),
        dataType: 'json',
        processData: false,
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            var markup = "<tr  id='addr" + conteo + "'><td>" + nombre + "</td><td><button type='button' class='btn btn-danger' id='botonEliminar " + data + "' onclick='DeleteItems(this)'>X</button></td></tr>";
            $("#TableIntegrantes  tbody").append(markup);

        }
    });
}

function CreateMinutaTemporal() {
    var param = {
        IdVisita: $("#idVisitaCompletar").val()
    };

    $.ajax({
        url: 'CreateMinutaTemporal',
        type: 'POST',
        data: JSON.stringify(param),
        dataType: 'json',
        processData: false,
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            if (data == 0) {
                CompletarVisita();
            } else {
                Swal.fire(
                    "Ocurrio un error al crear la minuta, no se pudo completar la visita"
                );
                return false;
            }

        }
    });
}

