﻿/// <reference path="../jquery-3.4.1.min.js" />
/// <reference path="../../plantilla/plugins/datatables/datatables.bootstrap4.min.js" />
var num = 0;

$(document).ready(function () {

    /////////////////////Instancias de select2 para cada selectlist////////////////////
    $(".SelectClienteM").select2({

        placeholder: "Buscar...",
        allowClear: false,
        //theme: "bootstrap4",
        width: '83%',
        ajax: {
            url: "/Minuta/GetData",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term,
                    tipo: 'Cliente'
                };
            },
            processResults: function (data, params) {
                return {
                    results: data
                };
            }
        }
    });

    $(".AsistentesMselect2").select2({

        placeholder: "Buscar asistente",
        allowClear: false,
        //theme: "bootstrap4",
        width: '83%',
        ajax: {
            url: "/Minuta/GetAsistentes",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term,
                    tipo: 'Asistentes'
                };
            },
            processResults: function (data, params) {
                return {
                    results: data
                };
            }
        }
    });

    $(".selectAsignado").select2({

        placeholder: "Buscar asistente",
        allowClear: false,
        //theme: "bootstrap4",
        width: '83%',
        ajax: {
            url: "/Minuta/GetData",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term,
                    tipo: 'Usuario'
                };
            },
            processResults: function (data, params) {
                return {
                    results: data
                };
            }
        }
    });

    //////////////////////////////////////////////

    //////
    $("#MinutaTemporalList").change(function () {
        GetDataMinutaTemporal(this.value);
    });

    $("#formCompletarAsignacion").hide();
    $("#formEditarMinuta").hide();
    //getAsistentes();

    /// Bloquea fechas anteriores en datepicker ////
    var today = new Date().toISOString().split('T')[0];
    document.getElementsByClassName("datepickerhtml")[0].setAttribute('min', today);
    /////////////////////////////////////////////

    $('#Cliente').keyup(function () { // aqui va el input text para la busqueda


        var input = document.getElementById("ClienteM");


        $('#result_Cliente').html(''); // aqui va la data que se va a mostrar

        if (this.value !== "") {
            getListMinuta(this.value, 'Cliente');
        } else {
            return false;
        }
    });


    $('.Asignado').keyup(function () { // aqui va el input text para la busqueda      

        if (this.value !== "") {
            getListMinuta(this.value, this.id);
        } else {
            return false;
        }
    });



    ////////////////////// document ready fin
});

$('#buttonSaveEditarM').on('click', function (e) {
    e.preventDefault();
    var forms = document.getElementsByClassName('needs-validation')[0];
    if (validationForm(forms)) {
        EditarMinuta();
    }

});

function getListMinuta(data, container) {

    var param = {
        criterio: data,
        tipo: container
    };

    $.ajax({
        url: "GetData", // Url
        type: "GET", // Verbo HTTP
        data: param
    })
        // Se ejecuta si todo fue bien.
        .done(function (result) {

            if (result !== null) {
                if (container === "Asignado") {
                    fillListAsignado(result);
                } else if (container === "Cliente") {
                    fillListCliente(result);
                }
            }
        })
        // Se ejecuta si se produjo un error.
        .fail(function (xhr, status, error) {

        })
        // Hacer algo siempre, haya sido exitosa o no.
        .always(function () {

        });
}

function getListAsignado(data, id) {
    $('#result_' + id).empty();
    var param = {
        criterio: data

    };

    $.ajax({
        url: "GetUsuarios", // Url
        type: "GET", // Verbo HTTP
        data: param
    })
        // Se ejecuta si todo fue bien.
        .done(function (result) {

            if (result !== null) {
                fillListAsignado(result, id);
            }
        })
        // Se ejecuta si se produjo un error.
        .fail(function (xhr, status, error) {

        })
        // Hacer algo siempre, haya sido exitosa o no.
        .always(function () {

        });
}


function getAsistentes() {


    $.ajax({
        url: "GetAsistentes", // Url
        type: "GET" // Verbo HTTP       
    })
        // Se ejecuta si todo fue bien.
        .done(function (result) {

            if (result !== null) {
                $('#AsistentesList').find('option').remove().end();
                $.each(result, function (val, text) {
                    $('#AsistentesList').append($('<option></option>').html(text));
                });
            }
        })
        // Se ejecuta si se produjo un error.
        .fail(function (xhr, status, error) {

        })
        // Hacer algo siempre, haya sido exitosa o no.
        .always(function () {

        });
}

function fillListCliente(data) {
    $('#result_Cliente').empty();
    for (var i = 0; i < data.length; i++) {


        $('#result_ClienteM').append(`<li class="list-group-item link-class" onclick="setItemCliente(this)" value="${data[i].ID_Ente}" name="${data[i].ID_Ente} +'|'+ ${data[i].NombreCompleto}">${data[i].NombreCompleto}</li>`);

        $('#result_Cliente').append(`<li class="list-group-item link-class" onclick="setItemCliente(this)" value="${data[i].ID_Ente}" name="${data[i].ID_Ente} +'|'+ ${data[i].NombreCompleto}">${data[i].NombreCompleto}</li>`);

    }

}

function fillListAsignado(data, id) {
    $('#result_Asignado' + id).empty();
    for (var i = 0; i < data.length; i++) {


        $('#result_Asignado' + id).append(`<li class="list-group-item link-class"  onclick='setItemAsignado(this,this.id)' value="${data[i].IdUsuario}" title="">${data[i].Usuario}</li>`);

        $('#result_' + id).append(`<li class="list-group-item link-class" onclick='setItemAsignado(this,this.name)' value="${data[i].IdUsuario}" title="${id}">${data[i].Usuario}</li>`);

    }

}

function setItemCliente(data) {

    $('#Cliente').val(data.textContent).attr('value', data.value);
    $("#result_Cliente").empty();
}

function setItemAsignado(data, id) {

    $('#' + num).val(data.textContent).attr('value', data.value);
    $("#result_" + num).empty();
}

function DeleteItemsTableMin(items) {
    $("#addr" + items).remove();
    conteo2--;

}


$("#AddAsistente").click(function () {
    num = num + 1;
    var idasignado = $("#Asignado option:selected").val();
    var nombreasignado = $("#Asignado option:selected").text();
    //contructor de nueva fila en string para la tabla...
    var newrow = "";
    newrow += "<tr id=''>";
    newrow += "<td>";
    newrow += "<input type='hidden' id='AsignadoId" + num + "' value='" + idasignado + "'>";
    newrow += "<input type='text' class='form-control required' id='Asignado" + num + "' value='" + nombreasignado + "' disabled>";
    //newrow += "  <select id='listAsignado" + num +"' class='form-control col-10 selectAsignado"+ num +"'><option></option></select>";
    newrow += "</td>";
    newrow += "<td>";
    newrow += "<textarea class='form-control required' type='text' name='Tarea' id='idTarea'></textarea>";
    newrow += "</td>";
    newrow += "<td>";
    newrow += "<textarea class='form-control ' type='text' name='Acuerdo' id='idAcuerdo'></textarea>";
    newrow += "</td>";
    newrow += "<td>";
    newrow += "<textarea class='form-control ' type='text' name='Recomendacion' id='idRecomendacion'></textarea>";
    newrow += "</td>";
    newrow += "<td>";
    newrow += "<input type='date' class='form-control datepickerhtml col-12 required' id='FechaEntrega' name='FechaEntrega' onkeypress='return pulsar(event)' onblur='validDateTime(this.value, this)' placeholder=''>";
    newrow += "</td>";
    newrow += "<td>";
    newrow += "<button onclick='SomeDeleteRowFunction(this)' class='btnmore btn btn-danger' name='btnmore' id='btnmore' type='button'>X</button>";
    newrow += "</td>";
    newrow += "</tr>";
    //var agregacion = $(newrow);
    $("#maintable").append(newrow);
    $('#Asignado').empty();
    $('#Asignado').val(null).trigger('change');

});
$("#AddAsistenteEdit").click(function () {
    num = num + 1;
    var idasignado = $("#AsignadoEdit option:selected").val();
    var nombreasignado = $("#AsignadoEdit option:selected").text();
    //contructor de nueva fila en string para la tabla...
    var newrow = "";
    newrow += "<tr id=''>";
    newrow += "<td>";
    newrow += "<input type='hidden' id='AsignadoIdEdit" + num + "' value='" + idasignado + "'>";
    newrow += "<input type='text' class='form-control required' id='AsignadoEdit" + num + "' value='" + nombreasignado + "' disabled>";
    //newrow += "  <select id='listAsignado" + num +"' class='form-control col-10 selectAsignado"+ num +"'><option></option></select>";
    newrow += "</td>";
    newrow += "<td>";
    newrow += "<textarea class='form-control required' type='text' name='Tarea' id='idTareaEdit'></textarea>";
    newrow += "</td>";
    newrow += "<td>";
    newrow += "<textarea class='form-control ' type='text' name='Acuerdo' id='idAcuerdoEdit'></textarea>";
    newrow += "</td>";
    newrow += "<td>";
    newrow += "<textarea class='form-control ' type='text' name='Recomendacion' id='idRecomendacionEdit'></textarea>";
    newrow += "</td>";
    newrow += "<td>";
    newrow += "<input type='date' class='form-control datepickerhtml col-12 required' id='FechaEntregaEdit' name='FechaEntrega' onkeypress='return pulsar(event)' onblur='validDateTime(this.value, this)' placeholder=''>";
    newrow += "</td>";
    newrow += "<td>";
    newrow += "<button onclick='SomeDeleteRowFunction(this)' class='btnmore btn btn-danger' name='btnmore' id='btnmoreEdit' type='button'>X</button>";
    newrow += "</td>";
    newrow += "</tr>";
    //var agregacion = $(newrow);
    $("#maintableEdit").append(newrow);


});

function SomeDeleteRowFunction(item) {

    Swal.fire({
        title: 'Seguro que deseas eliminar esta linea?',
        text: "",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar linea!',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.value) {
            var td = item.parentNode;
            var tr = td.parentNode; // the row to be removed
            tr.parentNode.removeChild(tr);
            Swal.fire(
                'Eliminado!',
                '',
                'success'
            );
        }
    });
}

function CreateMinuta() {
    var objectTable = [];
    var filas = document.querySelectorAll("#idTableCreacionMinuta tbody tr");
    var columnas;

    if (filas.length !== 0) {
        filas.forEach(function (e) {
            columnas = e.querySelectorAll("td");

            var obj = {
                IdAsignado: columnas[0].firstElementChild.defaultValue,
                Tarea: columnas[1].firstElementChild.value,
                Acuerdo: columnas[2].firstElementChild.value,
                Recomendacion: columnas[3].firstElementChild.value,
                Fecha_Entrega: columnas[4].firstElementChild.value
            };

            objectTable.push(obj);
            console.log(objectTable);
        });
    }

    var AsistentesExt = $("#AsistenteExt").val().split(",");
    var Asistentes = $("#AsistentesList").val();

    for (var i = 0; i < AsistentesExt.length; i++) {
        Asistentes.push(AsistentesExt[i]);
    }

    var ModelMinuta = new Object();

    ModelMinuta.IdPersona = $("#ClienteM option:selected").val(),
        ModelMinuta.Contacto = $("#Contacto").val(),
        ModelMinuta.Fecha = $("#Fecha").val(),
        ModelMinuta.MotivoReunion = $("#MotivoReunion").val(),
        ModelMinuta.Prioridad = $("#idPrioridad").val(),
        ModelMinuta.Lugar = $("#lugar").val(),
        ModelMinuta.Asistente = Asistentes,
        ModelMinuta.Asignaciones = objectTable,
        ModelMinuta.Resumen = tinymce.get('Resumen').getContent(),
        ModelMinuta.IdMinutaTemporal = $("#MinutaTemporalList option:selected").val();



    $.ajax({
        type: 'POST',
        url: 'SaveMinuta',
        data: JSON.stringify(ModelMinuta),
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            if (data === '1') {
                Swal.fire(
                    'Minuta creada correctamente',
                    'Click aqui↓',
                    'success'
                ).then(function () {
                    location.reload();
                });

            } else if (data === '0') {
                Swal.fire(
                    'Ocurrio un error, favor comunicarse con soporte tecnico',
                    'Click aqui↓',
                    'error'
                );
            }
        }, error: function (exception) {
            alert('Exeption:' + exception);
            $('#submit').val('Save');
        }

    });
}


function EditarMinuta() {
    var objectTable = [];
    var filas = document.querySelectorAll("#idTableCreacionMinutaEdit tbody tr");
    var columnas;

    if (filas.length !== 0) {
        filas.forEach(function (e) {
            columnas = e.querySelectorAll("td");

            var obj = {
                IdAsignado: columnas[0].firstElementChild.defaultValue,
                Tarea: columnas[1].firstElementChild.value,
                Acuerdo: columnas[2].firstElementChild.value,
                Recomendacion: columnas[3].firstElementChild.value,
                Fecha_Entrega: columnas[4].firstElementChild.value
            };

            objectTable.push(obj);
            console.log(objectTable);
        });
    }


    var ModelMinuta = new Object();

   
    ModelMinuta.Asignaciones = objectTable,
        ModelMinuta.Resumen = tinymce.get('ResumenEdit').getContent(),
        ModelMinuta.IdMinuta = $("#MinutaID").val();


    $.ajax({
        type: 'POST',
        url: 'EditarMinuta',
        data: JSON.stringify(ModelMinuta),
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            if (data === '1') {
                Swal.fire(
                    'Minuta actualizada correctamente',
                    'Click aqui↓',
                    'success'
                ).then(function () {
                    location.reload();
                });

            } else if (data === '0') {
                Swal.fire(
                    'Ocurrio un error, favor comunicarse con soporte tecnico',
                    'Click aqui↓',
                    'error'
                );
            }
        }, error: function (exception) {
            alert('Exeption:' + exception);
            $('#submit').val('Save');
        }

    });
}

function SetDetailsMinuta(Id) {

    var param = {
        Id: Id

    };

    $.ajax({
        url: "GetDetailsMinuta", // Url
        type: "GET", // Verbo HTTP
        data: param
    })
        // Se ejecuta si todo fue bien.
        .done(function (result) {

            if (result !== null) {
                fillDetails(result, Id);
            }
        })
        // Se ejecuta si se produjo un error.
        .fail(function (xhr, status, error) {

        })
        // Hacer algo siempre, haya sido exitosa o no.
        .always(function () {

        });
}

function fillDetails(data, Id) {
    $("#CompletarAsignacion").show();
    let verifica = 0;
    var html = "";
    var html2 = "";
    var htmlEdit = "";
    $('#Nombre_Cliente').text(data.Cliente);
    $('#Contacto').text(data.Contacto);
    $('#Fecha').text(formatDate(data.FechaReunion));
    $('#UsuarioCreador').text(data.UsuarioRegistro);
    $('#MotivoReunion').text(data.MotivoReunion);
    $('#Lugar').val(data.Lugar);
    tinymce.get('ResumenDetalle').setContent(data.Resumen);
    tinymce.get('ResumenDetalle').getBody().setAttribute('contenteditable', false);
    $('.tox-editor-header').hide();
    $('#MinutaID').val(Id);

    $("#tableAsistentes  tbody").empty();
    for (var i = 0; i < data.Asistentes.length; i++) {

        $("#tableAsistentes  tbody").append("<tr id=''><td>" + data.Asistentes[i].Asistente + "</td></tr>");
    }

    $("#tableAsignaciones  tbody").empty();
    for (var y = 0; y < data.Asignaciones.length; y++) {

        html += "<tr id=''>";
        html += "<td>" + data.Asignaciones[y].IdAsignacion + "</td>";
        html += "<td><textarea ondblclick='ViewModal(this)' readonly style='resize: none; height: 58px; width:197px' data-toggle='tooltip' data-placement='top' title='Hacer doble click para visualizar el texto completo'>" + data.Asignaciones[y].Tarea + "</textarea></td>";
        html += "<td><textarea ondblclick='ViewModal(this)' readonly style='resize: none; height: 58px; width:197px' data-toggle='tooltip' data-placement='top' title='Hacer doble click para visualizar el texto completo'> " + data.Asignaciones[y].Acuerdo + "</textarea></td>";
        html += "<td><textarea ondblclick='ViewModal(this)' readonly style='resize: none; height: 58px; width:197px' data-toggle='tooltip' data-placement='top' title='Hacer doble click para visualizar el texto completo'>" + data.Asignaciones[y].Recomendacion + "</textarea></td></tr>";


        $("#tableAsignaciones  tbody").append(html);
        ///////       
    }

    for (var r = 0; r < data.Asignaciones.length; r++) {
        $("#tableCompAsignaciones  tbody").empty();

        html2 += "<tr id=''>";
        html2 += "<td>" + data.Asignaciones[r].IdAsignacion + "</td>";
        html2 += "<td><textarea ondblclick='ViewModal(this)' readonly style='resize: none; height: 58px; width:197px' data-toggle='tooltip' data-placement='top' title='Hacer doble click para visualizar el texto completo'>" + data.Asignaciones[r].Tarea + "</textarea></td>";
        html2 += "<td><textarea ondblclick='ViewModal(this)' readonly style='resize: none; height: 58px; width:197px' data-toggle='tooltip' data-placement='top' title='Hacer doble click para visualizar el texto completo'> " + data.Asignaciones[r].Acuerdo + "</textarea></td>";
        html2 += "<td><textarea ondblclick='ViewModal(this)' readonly style='resize: none; height: 58px; width:197px' data-toggle='tooltip' data-placement='top' title='Hacer doble click para visualizar el texto completo'>" + data.Asignaciones[r].Recomendacion + "</textarea></td>";
        html2 += "<td>" + formatDate(data.Asignaciones[r].Fecha_Entrega) + "</td>";
        if (data.Asignaciones[r].Condicion === "Completada") {
            html2 += "<td style='color:green'>" + data.Asignaciones[r].Condicion + "</td>";
            html2 += "<td><i class='fas fa-check'></i></td>";
            verifica = 1;
        } else if (data.Asignaciones[r].Condicion === "En Proceso") {
            verifica = 0;
            html2 += "<td style='color:red'>" + data.Asignaciones[r].Condicion + "</td>";
            html2 += "<td><a href='#' class='btn btn-primary float-right' onclick='confirmCompletarAsignacion(this," + Id + ")' value='" + data.Asignaciones[r].IdAsignacion + "'>Completar</a></td></tr>";
        }


        $("#tableCompAsignaciones  tbody").append(html2);
    }

    //////////////////////////////////////Llenando campos de editar la minuta////////

    //var newOption = new Option(decodeURIComponent(data.NombreCliente), data.IdCliente, true, true);
    //$('#ClienteMEdit').append(newOption).trigger('change');
    //$('#ContactoEdit').val(data.Contacto);
    //$('#idPrioridadEdit').val(data.Prioridad);
    //$('#FechaEdit').val(data.FechaReunion);
    //$('#ContactoEdit').val(data.Contacto);
    //$('#ContactoEdit').val(data.Contacto);
    //$('#ContactoEdit').val(data.Contacto);
    tinymce.get('ResumenEdit').setContent(data.Resumen);

    $("#idTableCreacionMinutaEdit  tbody").empty();
    for (var y = 0; y < data.Asignaciones.length; y++) {
        format = function date2str(x, y) {
            var z = {
                M: x.getMonth() + 1,
                d: x.getDate(),
                h: x.getHours(),
                m: x.getMinutes(),
                s: x.getSeconds()
            };
            y = y.replace(/(M+|d+|h+|m+|s+)/g, function (v) {
                return ((v.length > 1 ? "0" : "") + eval('z.' + v.slice(-1))).slice(-2);
            });

            return y.replace(/(y+)/g, function (v) {
                return x.getFullYear().toString().slice(-v.length)
            });
        };
        date = format(new Date(data.Asignaciones[y].Fecha_Entrega), 'yyyy-MM-dd');

        num = num + 1;

        //contructor de nueva fila en string para la tabla...
        var newrow = "";
        newrow += "<tr id=''>";
        newrow += "<td>";
        newrow += "<input type='hidden' id='AsignadoIdEdit" + num + "' value='" + data.Asignaciones[y].IdAsignado + "'>";
        newrow += "<input type='text' class='form-control required' id='AsignadoEdit" + num + "' value='" + data.Asignaciones[y].Asignado + "' disabled>";
        //newrow += "  <select id='listAsignado" + num +"' class='form-control col-10 selectAsignado"+ num +"'><option></option></select>";
        newrow += "</td>";
        newrow += "<td>";
        newrow += "<textarea class='form-control required' type='text' name='Tarea' id='idTareaEdit' value:''>" + data.Asignaciones[y].Tarea + "</textarea>";
        newrow += "</td>";
        newrow += "<td>";
        newrow += "<textarea class='form-control ' type='text' name='Acuerdo' id='idAcuerdoEdit' value:''>" + data.Asignaciones[y].Tarea + "</textarea>";
        newrow += "</td>";
        newrow += "<td>";
        newrow += "<textarea class='form-control ' type='text' name='Recomendacion' id='idRecomendacionEdit' value:''>" + data.Asignaciones[y].Tarea + "</textarea>";
        newrow += "</td>";
        newrow += "<td>";
        newrow += "<input type='date' class='form-control datepickerhtml col-12 required' id='FechaEntregaEdit' name='FechaEntrega' onkeypress='return pulsar(event)' onblur='validDateTime(this.value, this)' value='" + date + "'>";
        newrow += "</td>";
        newrow += "<td>";
        newrow += "<button onclick='SomeDeleteRowFunction(this)' class='btnmore btn btn-danger' name='btnmore' id='btnmoreEdit' type='button'>X</button>";
        newrow += "</td>";
        newrow += "</tr>";
        //var agregacion = $(newrow);
        $("#maintableEdit").append(newrow);
    }

    ///////////////////

    ////////Funcion de que verifica y oculta los botones

    if (verifica === 1) {
        $("#CompletarAsignacion").hide();
        $("#EditarMinuta").hide();
    }
    $('#ModalDetalleMinuta').modal('show');

}

function ViewModal(param) {
    $("#textView").val('');
    $("#textView").val(param.value);
    $('#ModalView').modal('show');
}

function BotonCompletar() {
    $("#formDetalleMinuta").hide("slow");
    $("#formCompletarAsignacion").show("slow");

}

function volverMinutaAsignaciones() {
    $("#formCompletarAsignacion").hide("slow");
    $("#formDetalleMinuta").show("slow");
    $("#formCompletarAsignacion")[0].reset();
}

function confirmCompletarAsignacion(data, Id) {
    Swal.fire({
        title: 'Seguro que deseas dar por terminada esta asignacion?',
        text: "",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, seguro!',
        cancelButtonText: 'No, cancelar'
    }).then((result) => {
        if (result.value) {
            completarAsignacion(data, Id);
        } else {
            return false;
        }
    });
}

function completarAsignacion(data, Id) {



    var param = {
        Id: parseInt(data.attributes.value.value)
    };

    $.ajax({
        type: "POST", // Verbo HTTP
        url: "conpletarAsignacion", // Url        
        data: param
    })
        // Se ejecuta si todo fue bien.
        .done(function (result) {

            if (result !== null) {
                if (result === '1') {
                    Swal.fire(
                        'Asignacion completada correctamente',
                        'Click aqui↓',
                        'success'
                    ).then(function () {
                        SetDetailsMinuta(Id);
                    });

                } else if (result === '0') {
                    Swal.fire(
                        'Ocurrio un error, favor comunicarse con soporte tecnico',
                        'Click aqui↓',
                        'error'
                    );
                }
            }
        })
        // Se ejecuta si se produjo un error.
        .fail(function (xhr, status, error) {

        })
        // Hacer algo siempre, haya sido exitosa o no.
        .always(function () {

        });

}

function sortTable(n, idtable) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById(idtable);
    switching = true;
    // Set the sorting direction to ascending:
    dir = "asc";
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
        // Start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /* Loop through all table rows (except the
        first, which contains table headers): */
        for (i = 1; i < (rows.length - 1); i++) {
            // Start by saying there should be no switching:
            shouldSwitch = false;
            /* Get the two elements you want to compare,
            one from current row and one from the next: */
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            /* Check if the two rows should switch place,
            based on the direction, asc or desc: */
            if (dir === "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            } else if (dir === "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            /* If a switch has been marked, make the switch
            and mark that a switch has been done: */
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            // Each time a switch is done, increase this count by 1:
            switchcount++;
        } else {
            /* If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again. */
            if (switchcount === 0 && dir === "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}

function formModalControl(boton) {

    switch (boton) {
        case "CompletarAsignacion":
            $("#formCompletarAsignacion").show("slow");
            $("#formDetalleMinuta").hide("slow");
            $("#divBotonesDetalle").attr("hidden", true);
            break;
        case "EditarMinuta":
            $("#formEditarMinuta").show("slow");
            $("#formDetalleMinuta").hide("slow");
            $("#divBotonesDetalle").attr("hidden", true);
            break;
        case "botonVolverCompAsign":
            $("#formDetalleMinuta").show("slow");
            $("#formCompletarAsignacion").hide("slow");
            $("#divBotonesDetalle").attr("hidden", false);
            break;
        case "botonVolverEditar":
            $("#formDetalleMinuta").show("slow");
            $("#formEditarMinuta").hide("slow");
            $("#divBotonesDetalle").attr("hidden", false);
            break;
        case "closeModalDetalle":
            location.reload();
            //$("#formCompletar").hide("slow");
            //$("#formDetalle").show("slow");
            //$("#divBotonesDetalle").attr("hidden", false);
            //$('#formEditar')[0].reset();    
            break;
        //case "botonVolverPosponer":
        //    $("#formPosponer").hide("slow");
        //    $("#formDetalle").show("slow");
        //    $("#divBotonesDetalle").attr("hidden", false);
        //    $('#formPosponer')[0].reset();
        //    $('.required').removeClass('is-invalid')
        //    $('.required').removeClass('is-valid')
        //    break;
        //case "botonVolverCompletar":
        //    $("#formCompletar").hide("slow");
        //    $("#formDetalle").show("slow");
        //    $("#divBotonesDetalle").attr("hidden", false);
        //    $('#formCompletar')[0].reset();
        //    $('.required').removeClass('is-invalid')
        //    $('.required').removeClass('is-valid')
        //    break;
        //case "guardarEditar":
        //    $("#formEditar").hide("slow");
        //    $("#formDetalle").show("slow");
        //    $("#divBotonesDetalle").attr("hidden", false);
        //    $('#formEditar')[0].reset();
        //    $("#tbodyEdit").html('');
        //    $("#searchResponsableEdit").val('');
        //    break;
        //case "guardarCompletar":
        //    $("#formCompletar").hide("slow");
        //    $("#formDetalle").show("slow");
        //    $("#divBotonesDetalle").attr("hidden", false);
        //    $('#formCompletar')[0].reset();
        //    break;
        //case "guardarPosponer":
        //    $("#formPosponer").hide("slow");
        //    $("#formDetalle").show("slow");
        //    $("#divBotonesDetalle").attr("hidden", false);
        //    $('#formPosponer')[0].reset();
        //    break;

    }
}

function GetDataMinutaTemporal(id) {

    var param = {
        Id: id
    };

    $.ajax({
        url: "GetDataMinutaTemporal", // Url
        type: "GET", // Verbo HTTP
        data: param
    })
        // Se ejecuta si todo fue bien.
        .done(function (result) {
            //(result);
            $.each(result, function (index, value) {
                format = function date2str(x, y) {
                    var z = {
                        M: x.getMonth() + 1,
                        d: x.getDate(),
                        h: x.getHours(),
                        m: x.getMinutes(),
                        s: x.getSeconds()
                    };
                    y = y.replace(/(M+|d+|h+|m+|s+)/g, function (v) {
                        return ((v.length > 1 ? "0" : "") + eval('z.' + v.slice(-1))).slice(-2)
                    });

                    return y.replace(/(y+)/g, function (v) {
                        return x.getFullYear().toString().slice(-v.length)
                    });
                };

                var newOption = new Option(decodeURIComponent(value.NombreCliente), value.IdCliente, true, true);
                $('#ClienteM').append(newOption).trigger('change');
                $('#Contacto').val(value.Contacto);
                date = format(new Date(value.FechaReunion), 'yyyy-MM-dd');
                $('#Fecha').val(date);
                $('#MotivoReunion').val(value.Motivo);
                $('#lugar').text(value.Lugar);



                //    retorna += "$('#MotivoReunion').val(" + item.Motivo_Reunion + ");";
                //    retorna += "$('#lugar').text(" + item.Lugar + ");";
            });
        })
        // Se ejecuta si se produjo un error.
        .fail(function (xhr, status, error) {

        })
        // Hacer algo siempre, haya sido exitosa o no.
        .always(function () {

        });
}


function GetDataMinutaDetail(id) {

    var param = {
        Id: id
    };

    $.ajax({
        url: "GetDataMinutaTemporal", // Url
        type: "GET", // Verbo HTTP
        data: param
    })
        // Se ejecuta si todo fue bien.
        .done(function (result) {
            //(result);
            $.each(result, function (index, value) {
                format = function date2str(x, y) {
                    var z = {
                        M: x.getMonth() + 1,
                        d: x.getDate(),
                        h: x.getHours(),
                        m: x.getMinutes(),
                        s: x.getSeconds()
                    };
                    y = y.replace(/(M+|d+|h+|m+|s+)/g, function (v) {
                        return ((v.length > 1 ? "0" : "") + eval('z.' + v.slice(-1))).slice(-2)
                    });

                    return y.replace(/(y+)/g, function (v) {
                        return x.getFullYear().toString().slice(-v.length)
                    });
                };

                var newOption = new Option(decodeURIComponent(value.NombreCliente), value.IdCliente, true, true);
                $('#ClienteM').append(newOption).trigger('change');
                $('#Contacto').val(value.Contacto);
                date = format(new Date(value.FechaReunion), 'yyyy-MM-dd');
                $('#Fecha').val(date);
                $('#MotivoReunion').val(value.Motivo);
                $('#lugar').text(value.Lugar);



                //    retorna += "$('#MotivoReunion').val(" + item.Motivo_Reunion + ");";
                //    retorna += "$('#lugar').text(" + item.Lugar + ");";
            });
        })
        // Se ejecuta si se produjo un error.
        .fail(function (xhr, status, error) {

        })
        // Hacer algo siempre, haya sido exitosa o no.
        .always(function () {

        });
}
