﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aspose.Email;
using Aspose.Email.Calendar;

namespace ApiMailsender
{
    public class mainclass
    {
        public bool AppoimentCreator(string mailbody, string Mailuser,string Mailuserpass, string mailsubjet, List<string> attemptes, DateTime Datefrom, DateTime Dateto,string AppAddress)
            {
            bool result = false;

            try
            {
                MailAddressCollection localattemptes = new MailAddressCollection();
                foreach (string item in attemptes)
                {
                    localattemptes.Add(item);
                }
                Appointment app = new Appointment(AppAddress, Datefrom, Dateto, new MailAddress(Mailuser), localattemptes);
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress("noreply@ros.com.do");

                foreach (string endmailer in attemptes)
                {
                    msg.To.Add(endmailer);
                }
                msg.Subject = mailsubjet;
                msg.Body = mailbody;
                msg.AddAlternateView(app.RequestApointment());           
                Aspose.Email.Clients.Smtp.SmtpClient Mailclient = new Aspose.Email.Clients.Smtp.SmtpClient("accesoros.saas24x7.com", 587, Mailuser, Mailuserpass);
                Mailclient.Send(msg);
                result = true;
                
            }
            catch
            {
                result = false;
            }
                return result;
             

            }       

    }
}
